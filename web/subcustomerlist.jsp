<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%
   String type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp"); 
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
    }
    if(!type.equals("Customer")){
        out.println("<script>window.history.back();</script>");
    }
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
%>
<%try { %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
        <style>
            div.dataTables_wrapper {

                margin: 0 auto;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
        <jsp:include page="customer_menu.jsp" />    

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Sub Customer
                        <!--<small>Control panel</small>--> 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    
                    <button type="button" class="btn btn-primary" style="margin-top:20px;" onClick="location.href = 'subcustomer.jsp';">Create New</button>
                    <!-- /.row --> 
                    <!-- Main row --> 
                    <!-- /.row (main row) -->
                    <div class="container-fluid" style="margin-top:20px;  height: 100%;" >
                         <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!--                            <table class="table table-responsive table-bordered table-striped table-hover">-->
                        <table id="example" class="table table-responsive" cellspacing="0">
                                <thead>
                                    <tr>  
                                        <th>NAME</th>
                                        <th>USER NAME</th>
                                        <th>PASSWORD </th>
                                       <!--  <th>COMPANY NAME</th>
                                        <th>REPRESENTIVE NAME</th> -->
                                        <th>EMAIL</th>
                                        <th>CONTACT NO</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                     
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        try { 
                                            PreparedStatement ps = conn.prepareStatement("SELECT * FROM reg WHERE flag = '1' and cust_master_id = '"+session.getAttribute("uname")+"'");
                                            ResultSet rs = ps.executeQuery();
                                            int i = 1;
                                            while (rs.next()) {
                                    %>
                                    <tr>
                                        
                                        <td><%=rs.getString(2)%></td>
                                        <td><%=rs.getString(1)%></td>
                                        <td><%=rs.getString(5)%></td>
                                       <%--  <td><%=rs.getString(6)%></td>
                                        <td><%=rs.getString(7)%></td> --%>
                                        <td><%=rs.getString(4)%></td>
                                        <td><%=rs.getString(3)%></td>
                                        <td><a href="sub_cust_update.jsp?m=<%=rs.getString(15)%>"><img src="img/edit.png" style="width: 24px; height: 24px;" title="Edit"/></a></td>
                                        <td><a href="code/rest_sub_cust_pswd.jsp?m=<%=rs.getString(15)%>"><img src="img/reset_icon.png" style="width: 24px; height: 24px;" title="Reset Password"/></a></td>
                                        <td><a href="code/delete_sub_cust.jsp?m=<%=rs.getString(15)%>"><img src="img/delete.png" style="width: 24px; height: 24px;" title="Delete"/></a></td>
                                        <td>
                                            <%  if(rs.getString(17).equals("0")){ %>
                                                    <a onclick="return confirm('Are you sure? You want to Block Customer')" href="code/unblock_subcustomer.jsp?uid=<%=rs.getString(15)%>&st=1" style='color:green;'>Block</a>                                                    
                                            <%  } else{ %>
                                                    <a onclick="return confirm('Are you sure? You want to Un-Block Customer')" href="code/unblock_subcustomer.jsp?uid=<%=rs.getString(15)%>&st=0" style='color:red;'>Un-Block</a>
                                            <%  }   %>                                            
                                        </td>
                                        
                                        
                                        
                                    </tr>
                                    <%
                                                i++;
                                            }
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    
                    </div>
                    </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar --> 

        </div>
        <!-- ./wrapper --> 

        <script type="text/javascript" class="init">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": false
                });
            });

        </script>
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
                        $.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
         
        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 
         
        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>
    </body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>