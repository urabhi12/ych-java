<%-- 
    Document   : autodbbackup
    Created on : 5 Apr, 2018, 12:33:14 PM
    Author     : MJ
--%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%
    String Result = "";
    try {
        Date d = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMddHHmmss");
        String curdate = sdf.format(d);
        Runtime runtime = Runtime.getRuntime();
        String DbName = "dbbkup_"+curdate+".sql";
        File backupFile = new File("D:\\Workspace\\Netbeen\\YCH_old\\web\\backup\\"+DbName);
        FileWriter fw = new FileWriter(backupFile);
        Process child = runtime.exec("D:\\Workspace\\xampp\\mysql\\bin\\mysqldump --user=root --password= --lock-all-tables --opt ych_server");
        InputStreamReader irs = new InputStreamReader(child.getInputStream());
        BufferedReader br = new BufferedReader(irs);

        String line;
        while( (line=br.readLine()) != null ) {
            fw.write(line + "\n");
        }
        fw.close();
        irs.close();
        br.close();
        Result = "Done";
    } catch (IOException ex) {
        out.println(ex.toString());
        ex.printStackTrace();
        Result = "Error";
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title></title>
    </head>
    <body>
        <h1><%= Result %></h1>
    </body>
</html>
