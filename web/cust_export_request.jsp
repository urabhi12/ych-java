<%@page import="java.util.Random"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItemFactory"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="pack1.Set_Para_Value"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="pack1.FileUploadManager"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Date"%>
<%@page import="pack1.Send_Email"%>
<%@page pageEncoding="UTF-8"%>
<%
	String type = "", user_id = "", Userid = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp"); 
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		type = session.getAttribute("type").toString();
		user_id = session.getAttribute("user_id").toString();
		Userid = session.getAttribute("uname").toString();
	}
	if (!type.equals("Customer") && !type.equals("Customer-Sub")) {
		out.println("<script>window.history.back();</script>");
	}
	String id = "";
	String expid = "";
	Date d = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	String y = sdf.format(d);

	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	String Req_job_no = "", Req_id = "", Req_EXP_Type = "", Req_FCLLCL = "", Req_DOD = "",
			Req_Destination_Ports = "", Req_Place_of_Delivery = "", Req_Bonded_Truck = "",
			Req_Reference_No = "", Req_Status="",Req_Remark = "", Req_PostType = "New";

	try {
		boolean isPost = "POST".equals(request.getMethod());

		if (isPost) {
			// String Path = "C://Program Files//Apache Software Foundation//Tomcat 8.5//webapps//YCH//uploadproduct";
			String Path = Send_Email.getPath();

			String Old_img_For_Update = "";

			ArrayList<String> arr = new ArrayList<String>();

			ArrayList<String> arrnocont = new ArrayList<String>();
			ArrayList<String> arrsizecont = new ArrayList<String>();
			ArrayList<String> arrtypecont = new ArrayList<String>();
			Set_Para_Value SPV = new Set_Para_Value();
			//String arrnocont[] = request.getParameterValues("nocont");

			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (!isMultipart) {
			} else {
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = null;
				try {
					items = upload.parseRequest(request);
				} catch (FileUploadException e) {
					e.printStackTrace();
				}
				Iterator itr = items.iterator();
				while (itr.hasNext()) {
					FileItem item = (FileItem) itr.next();

					if (item.isFormField()) {
						String Fname = item.getFieldName();
						String Fvalue = item.getString();
						if (Fname.equals("multi_nocont")) {
							arrnocont.add(Fvalue);
						} else if (Fname.equals("multi_sizecont")) {
							arrsizecont.add(Fvalue);
						} else if (Fname.equals("multi_typecont")) {
							arrtypecont.add(Fvalue);
						}

						if (Fname.equals("req_img_path")) {
							if (Old_img_For_Update == "") {
								Old_img_For_Update = "'" + Fvalue + "'";
							} else {
								Old_img_For_Update = Old_img_For_Update + ",'" + Fvalue + "'";
							}
						}

						SPV.Set_Value(item);
					} else {

						try {
							String itemName = item.getName();
							if (!itemName.equals("")) {
								Random randid = new Random();
								int n = randid.nextInt(10000) + 1;
								itemName = "ereq_" + String.valueOf(n) + "_" + itemName.replace("#", "_").replace("%", "_ ").replace("+", "_ ");
								File savedFile = new File(Path + itemName);
								item.write(savedFile);
								arr.add(itemName);
							}

							//out.println("<tr><td><b>Your file has been saved at the loaction:</b></td></tr><tr><td><b>" + config.getServletContext().getRealPath("/") + "uploadedFiles" + "\\" + itemName + "</td></tr>");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

			Req_PostType = SPV.getFormValue("reqtype");
			Req_id = SPV.getFormValue("reqid");
			Req_job_no = SPV.getFormValue("reqjobno");

			String cso_name = "", cso_email = "", UserEmail = "";

			PreparedStatement ps_cso_name = conn.prepareStatement(
					"SELECT id, name,cname,type,cust_master_id,cso,email FROM reg WHERE id ='"
							+ Userid + "' and flag = '1'");

			ResultSet rs_cso_name = ps_cso_name.executeQuery();

			while (rs_cso_name.next()) {
				UserEmail = rs_cso_name.getString(7);
				if (rs_cso_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

					String SQl_main_com = "SELECT * FROM reg where id = '" + rs_cso_name.getString(5) + "'";
					PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
					ResultSet rs_main_com = ps_main_com.executeQuery();
					while (rs_main_com.next()) {
						cso_name = rs_main_com.getString(18);
					}

				} else {
					cso_name = rs_cso_name.getString(6);
				}
				String SQl_main_cso_email = "SELECT email FROM reg where id = '" + cso_name + "'";
				PreparedStatement ps_main_cso_email = conn.prepareStatement(SQl_main_cso_email);
				ResultSet rs_main_cso_email = ps_main_cso_email.executeQuery();
				while (rs_main_cso_email.next()) {
					cso_email = rs_main_cso_email.getString(1);
				}

			}

			if (Req_id == "" && Req_PostType.equals("New")) {

				//if (arr.size() > 0) {

					String Requestid = "";
					String generatedColumns[] = {"tid"};
					PreparedStatement ps = conn.prepareStatement(
							"insert into req values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
							generatedColumns);

					if (!SPV.getFormValue("exptype").equals("Air")) {
						if (SPV.getFormValue("exptype").equals("Sea")) {
							if (SPV.getFormValue("fcl").equals("FCL")) {
								ps.setString(1, "EFS" + SPV.getFormValue("id"));
								Requestid = "EFS";
							} else if (SPV.getFormValue("fcl").equals("LCL")) {
								ps.setString(1, "ELS" + SPV.getFormValue("id"));
								Requestid = "ELS";
							}
						} else {
							//ps.setString(1, "CB"+SPV.getFormValue("id"));
							//Requestid = "CB"+SPV.getFormValue("id");
							if (SPV.getFormValue("fcl").equals("FCL")) {
								ps.setString(1, "EFR" + SPV.getFormValue("id"));
								Requestid = "EFR";
							} else if (SPV.getFormValue("fcl").equals("LCL")) {
								ps.setString(1, "ELR" + SPV.getFormValue("id"));
								Requestid = "ELR";
							}
						}
					} else {
						ps.setString(1, "ELA" + SPV.getFormValue("id"));
						Requestid = "ELA";
					}

					ps.setString(2, SPV.getFormValue("uid"));
					ps.setString(3, "");
					ps.setString(4, SPV.getFormValue("da"));
					ps.setString(5, y);
					ps.setString(6, y);
					ps.setString(7, SPV.getFormValue("exptype"));
					if (!SPV.getFormValue("exptype").equals("Air")) {
						ps.setString(8, SPV.getFormValue("fcl"));
					} else {
						ps.setString(8, "");
					}
					ps.setString(9, SPV.getFormValue("desport"));
					ps.setString(10, SPV.getFormValue("placedel"));

					ps.setString(11, "");
					ps.setString(12, "");
					ps.setString(13, "");

					ps.setString(14, SPV.getFormValue("bondedtruck"));
					ps.setString(15, "");
					ps.setString(16, "Pending");
					ps.setString(17, "New Export");
					ps.setString(18, SPV.getFormValue("cmpname"));
					ps.setString(19, cso_name);
					ps.setString(20, SPV.getFormValue("filename"));
					ps.setString(21, "");
					ps.setString(22, "");
					ps.setString(23, "");
					ps.setString(24, "");
					ps.setObject(25, null);//sequence
					ps.setString(26, "");
					ps.setString(27, "");
					ps.setString(28, "");
					ps.setString(29, "");
					ps.setString(30, "");
					ps.setString(31, "");
					ps.setString(32, "");
					ps.setString(33, "");
					ps.setString(34, "");
					ps.setString(35, "");
					ps.setString(36, SPV.getFormValue("remark"));
					ps.setString(37, "");
					ps.setString(38, SPV.getFormValue("customer_ref_no"));
					ps.setInt(39, 0);
					ps.setString(40, "");
					int n = ps.executeUpdate();
					if (n == 1) {
						ResultSet rs = ps.getGeneratedKeys();

						if (rs.next()) {
							int abc = rs.getInt(1);

							// For Update Request Id
							Requestid = Requestid + abc;
							PreparedStatement ps_update = conn.prepareStatement(
									"update req set id='" + Requestid + "'  where tid='" + abc + "'");
							ps_update.executeUpdate();

							// For Inser Container Details
							if (!SPV.getFormValue("exptype").equals("Air")) {
								if (!SPV.getFormValue("fcl").equals("LCL")) {
									for (int i = 0; i < arrnocont.size(); i++) {
										PreparedStatement ps_container = conn.prepareStatement(
												"insert into request_container values(?,?,?,?,?,?,?,?)",
												generatedColumns);
										ps_container.setInt(1, 0);
										ps_container.setString(2, String.valueOf(abc));
										ps_container.setString(3, arrnocont.get(i).toString());
										ps_container.setString(4, arrsizecont.get(i).toString());
										ps_container.setString(5, arrtypecont.get(i).toString());
										ps_container.setInt(6, 1);
										ps_container.setString(7, y);
										ps_container.setString(8, y);

										int n2 = ps_container.executeUpdate();
										if (n2 == 1) {

										}
									}
								}
							}

							// For Image Upload 
							int ecouny = 0;
							for (int i = 0; i < arr.size(); i++) {
								//                    while (abc > 1) {
								PreparedStatement ps2 = conn.prepareStatement(
										"insert into request_upload values(?,?,?,?,?)");
								ps2.setObject(1, null);//sequence
								ps2.setString(2, String.valueOf(abc));
								ps2.setString(3, arr.get(i).toString());
								ps2.setString(4, "Customer");
								ps2.setString(5, Userid);

								int n2 = ps2.executeUpdate();
								if (n2 == 1) {
									ecouny++;
									//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_new_request.jsp'</script>");
								}
							}

							// For Company and Customer Name
							String company = "", Custname = "";
							PreparedStatement ps_name = conn.prepareStatement(
									"SELECT id, name,cname,type,cust_master_id  FROM reg WHERE id ='"
											+ SPV.getFormValue("uid") + "'");
							ResultSet rs_name = ps_name.executeQuery();
							while (rs_name.next()) {
								Custname = rs_name.getString(2);
								if (rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {
									String SQl_main_com = "SELECT * FROM reg where id = '"
											+ rs_name.getString(5) + "'";
									PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
									ResultSet rs_main_com = ps_main_com.executeQuery();
									while (rs_main_com.next()) {
										company = rs_main_com.getString(6);
									}

								} else {
									company = rs_name.getString(3);
								}
							}

							// For CSO Email 
							if (!cso_email.equals("")) {
								String[] cso_emails = new String[1];
								cso_emails[0] = cso_email;

								String cso_Header = "YCH Freight Portal";
								String cso_Subject = "New job request created-" + company;
								String cso_message = Custname + " of " + company
										+ " created a New Export request. The request id is " + Requestid
										+ ". <br> <br> This is an automated email. Please do not reply to this.";

								boolean cso_mailstatus = Send_Email.send(cso_emails, cso_Header, null, null,
										cso_Subject, cso_message, null);
							}

							// For Mail Sending
							boolean imail = SPV.getFormValue("ismail") != null;
							boolean issent;
							if (imail) {
								try {
									String SQL = "select * from email_master where user_id = '" + user_id
											+ "' and set_type = 'New Export'";
									PreparedStatement psemail = conn.prepareStatement(SQL,
											ResultSet.TYPE_SCROLL_INSENSITIVE,
										    ResultSet.CONCUR_READ_ONLY);
									ResultSet rsemail = psemail.executeQuery();
									rsemail.last();
									int count = rsemail.getRow();
									rsemail.beforeFirst();
									//out.println("<script>alert("+count+")</script>");

									String[] emails = new String[count];

									int cntemaill = 0;
									while (rsemail.next()) {

										emails[cntemaill] = rsemail.getString("emails");
										cntemaill++;
									}

									String Message = SPV.getFormValue("remark");

									/*  String Message  = "Dear Concern,<br/><br/>"
									 + "New Export Request submission is successful."
									 + " Reference ID for this request is " + Requestid + "<br/><br/>Regards,<br/>YCH Team"; */

									String Subject = "New Export-" + SPV.getFormValue("customer_ref_no") + "-"
											+ SPV.getFormValue("da");

									String strheader = Custname + "-" + company;

									ArrayList<String> arr_attachment = new ArrayList<String>();

									for (int attachcnt = 0; attachcnt < arr.size(); attachcnt++) {
										arr_attachment
												.add(Send_Email.getPath() + arr.get(attachcnt).toString());
									}

									String[] usr_emails = new String[1];
									usr_emails[0] = UserEmail;

									boolean mailstatus = Send_Email.sendwithAttachment(emails, strheader,
											usr_emails, null, Subject, Message, arr_attachment);

									// boolean mailstatus =  Send_Email.send(emails,strheader, null, null,Subject, Message,null);
									if (mailstatus) {
										issent = true;
									} else {
										issent = false;
									}

									if (ecouny == arr.size()) {
										out.println(
												"<script>alert('Request submission is successful. Your reference ID for this request is "
														+ Requestid
														+ "');window.location.href='cust_my_request.jsp'</script>");
									} else {
										out.println(
												"<script>alert('Request submission not Successfull');window.location.href='cust_export_request.jsp'</script>");
									}
								} catch (Exception e) {
									System.out.println(e);
								}
							} else {
								if (ecouny == arr.size()) {
									out.println(
											"<script>alert('Request submission is successful. Your reference ID for this request is "
													+ Requestid
													+ "');window.location.href='cust_my_request.jsp'</script>");
								} else {
									out.println(
											"<script>alert('Request submission not Successfull');window.location.href='cust_export_request.jsp'</script>");
								}

							}

						}
						PreparedStatement psi = conn.prepareStatement("update sno set sno=sno+1");
						psi.executeUpdate();
						//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_export_request.jsp'</script>");
					} else {
						out.println(
								"<script>alert('Request submission is not Successfull');window.location.href='cust_export_request.jsp'</script>");
					}
				//}
			} else {
				// For Update Page

				String imp_type = "";
				if (!SPV.getFormValue("exptype").equals("Air")) {

					imp_type = "fcl = '" + SPV.getFormValue("fcl") + "', ";
				} else {
					imp_type = "";
				}
	
				String pRemark = SPV.getFormValue("remark").replaceAll("'","&#039;");
				String status = SPV.getFormValue("status");
				boolean updateStatus = status!=null && status.matches("Draft\\s+Ready|Draft\\s+Ok");
				PreparedStatement ps = conn.prepareStatement(
						"update req set exptype='" + SPV.getFormValue("exptype") + "'," + imp_type + " da='"
								+ SPV.getFormValue("da") + "', desport='" + SPV.getFormValue("desport")
								+ "', placedel='" + SPV.getFormValue("placedel") + "', bondedtruck ='"
								+ SPV.getFormValue("bondedtruck") + "', remarks='" + pRemark
								+ "', cust_ref_no='" + SPV.getFormValue("customer_ref_no")
								+ "', is_send_vcc = '0', "+(updateStatus?("status = '"+status+"',"):"status='Pending',")+"    mdate='" + y + "' where tid='"
								+ Req_id + "'");
				int n = ps.executeUpdate();
				if (n == 1) {
					String generatedColumns[] = {"tid"};
					
					// For Inser Container Details
					PreparedStatement psdelcon = conn
								.prepareStatement("Delete from request_container where request_id = '" + Req_id + "'");
					int ndelcon = psdelcon.executeUpdate();
						
						
					if (!SPV.getFormValue("exptype").equals("Air")) {
						if (!SPV.getFormValue("fcl").equals("LCL")) {
							for (int i = 0; i < arrnocont.size(); i++) {							
															
								PreparedStatement ps_container = conn.prepareStatement(
										"insert into request_container values(?,?,?,?,?,?,?,?)",
										generatedColumns);
								ps_container.setInt(1, 0);
								ps_container.setString(2, String.valueOf(Req_id));
								ps_container.setString(3, arrnocont.get(i).toString());
								ps_container.setString(4, arrsizecont.get(i).toString());
								ps_container.setString(5, arrtypecont.get(i).toString());
								ps_container.setInt(6, 1);
								ps_container.setString(7, y);
								ps_container.setString(8, y);

								int n2 = ps_container.executeUpdate();
								if (n2 == 1) {

								}
							}
						}
					}

					int ecouny = 0;
					

					if (!Old_img_For_Update.equals("")) {
						PreparedStatement psdel = conn
								.prepareStatement("Delete from request_upload where upload_file not in ("
										+ Old_img_For_Update + ") and req_id = '" + Req_id + "'");
						int ndel = psdel.executeUpdate();
					} else {
						PreparedStatement psdel = conn
								.prepareStatement("Delete from request_upload where req_id = '" + Req_id + "'");
						int ndel = psdel.executeUpdate();
					}
					for (int i = 0; i < arr.size(); i++) {

						PreparedStatement ps2 = conn.prepareStatement(
								"insert into request_upload values(?,?,?,?,?)");
						ps2.setObject(1, null);//sequence
						ps2.setString(2, String.valueOf(Req_id));
						ps2.setString(3, arr.get(i).toString());
						ps2.setString(4, "Customer");
						ps2.setString(5, Userid);

						int n2 = ps2.executeUpdate();
						if (n2 == 1) {
							ecouny++;
							//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_new_request.jsp'</script>");
						}
					}

					// For CSO Email 
					if (!cso_email.equals("")) {
						String[] cso_emails = new String[1];
						cso_emails[0] = cso_email;

						String cso_Header = "YCH Freight Portal";
						String cso_Subject = "Job " + Req_job_no + " was edited";
						String cso_message = "";

						boolean cso_mailstatus = Send_Email.send(cso_emails, cso_Header, null, null,
								cso_Subject, cso_message, null);
					}

					if (ecouny == arr.size()) {
						out.println(
								"<script>alert('Update successful!');window.location.href='cust_my_request.jsp'</script>");
					} else {
						out.println(
								"<script>alert('Update not successful!');window.location.href='cust_my_request.jsp'</script>");
					}

				} else {
					out.println(
							"<script>alert('Update not successful!');window.location.href='cust_my_request.jsp'</script>");
				}

			}
		} else {
			if (request.getParameter("id") != null) {
				Req_id = request.getParameter("id");

				PreparedStatement ps = conn.prepareStatement("select * from req where tid='" + Req_id + "'");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Req_job_no = rs.getString(1);
					Req_PostType = "Edit";

					Req_EXP_Type = rs.getString(7);
					Req_FCLLCL = rs.getString(8);
					Req_DOD = rs.getString(4);
					Req_Destination_Ports = rs.getString(9);
					Req_Place_of_Delivery = rs.getString(10);

					Req_Bonded_Truck = rs.getString(14);

					Req_Reference_No = rs.getString(38);
					Req_Remark = rs.getString(36);
					Req_Status = rs.getString("STATUS");
				}

			}
		}

	} catch (Exception e) {
		out.println(e.getMessage());
	}
%>
<%try { %>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>

<!-- Tell the browser to be responsive to screen width -->

<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<!-- Bootstrap 3.3.5 -->

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.min.css">

<!-- Font Awesome -->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Ionicons -->

<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Theme style -->

<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">

<!-- AdminLTE Skins. Choose a skin from the css/skins
        
                 folder instead of downloading all of them to reduce the load. -->

<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">

<!-- iCheck -->

<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>
        
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        
            <![endif]-->


	<link rel="stylesheet" href="upload.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />

		<!-- Left side column. contains the logo and sidebar -->

		<jsp:include page="customer_menu.jsp" />

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->

			<section class="content-header">
				<h1>
					New Export

					<!--<small>Control panel</small>-->

				</h1>
			</section>

			<!-- Main content -->

			<section class="content">

				<!-- Small boxes (Stat box) -->

				<!-- /.row -->

				<!-- Main row -->

				<!-- /.row (main row) -->

				<div class="container" style="margin-top: 50px; width: 100%;">
					<div class="panel panel-default">
						<div class="panel-body">
							<form action="cust_export_request.jsp" method="post"
								enctype="multipart/form-data">
								<%
									if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
										//response.sendRedirect("../index.jsp");
										out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
									}

									try {
										//        Class.forName("com.mysql.jdbc.Driver");
										//        Connection con = DriverManager.getConnection(host, username, password);

										PreparedStatement ps = conn.prepareStatement("select * from sno");
										ResultSet rs = ps.executeQuery();
										while (rs.next()) {
											id = rs.getString(1);
											expid = id;

										}
									} catch (Exception e) {
										System.out.println(e);
									}
								%>
								<div class="panel-heading">
									<u> </u>
								</div>
								<br>


								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Export
										Type :</div>
								</div>
								<div class="col-md-4">
									<select id="ddl_extype" class="form-control" name="exptype">


										<%
											if (Req_EXP_Type == "") {
										%>
										<option>Air</option>
										<option>Sea</option>
										<option>Cross Border</option>
										<%
											} else {
												if (Req_EXP_Type.equals("Air")) {
										%>
										<option selected="true">Air</option>
										<%
											} else {
										%>
										<option>Air</option>
										<%
											}
												if (Req_EXP_Type.equals("Sea")) {
										%>
										<option selected="true">Sea</option>
										<%
											} else {
										%>
										<option>Sea</option>
										<%
											}
												if (Req_EXP_Type.equals("Cross Border")) {
										%>
										<option selected="true">Cross Border</option>
										<%
											} else {
										%>
										<option>Cross Border</option>
										<%
											}
											}
										%>




									</select>
								</div>

								<div class="col-md-2" id="divfcllclhd">
									<div class="form-group" style="margin-top: 5%">FCL/ LCL :
									</div>
								</div>
								<div class="col-md-4" id="divfcllclval">
									<div class="form-group">
										<select id="fcl" class="form-control" name="fcl">


											<%
												if (Req_FCLLCL == "") {
											%>
											<option>FCL</option>
											<option>LCL</option>
											<%
												} else {
													if (Req_FCLLCL!=null && Req_FCLLCL.equals("FCL")) {
											%>
											<option selected="true">FCL</option>
											<%
												} else {
											%>
											<option>FCL</option>
											<%
												}
													if (Req_FCLLCL!=null && Req_FCLLCL.equals("LCL")) {
											%>
											<option selected="true">LCL</option>
											<%
												} else {
											%>
											<option>LCL</option>
											<%
												}

												}
											%>


										</select>

									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Expected
										Date of Departure:</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" id="ed_date" name="da"
											value="<%=Req_DOD%>">
										<!--  <p>Date: <input type="text" id="datepicker"></p>-->

									</div>
								</div>
								<div class="col-md-2" id="desporhd">
									<div class="form-group" style="margin-top: 5%">
										Destination Ports(AIR/SEA) :</div>
								</div>
								<div class="col-md-4" id="desporval">
									<div class="form-group">
										<input class="form-control"
											placeholder="Destination Ports (Air/ Sea)" type="text"
											name="desport" value="<%=Req_Destination_Ports%>">
									</div>
								</div>

								<div class="col-md-2" id="placedelhd">
									<div class="form-group" style="margin-top: 5%">Place of
										Delivery :</div>
								</div>
								<div class="col-md-4" id="placedelval">
									<div class="form-group">
										<input class="form-control" placeholder="Place of Delivery "
											type="text" name="placedel"
											value="<%=Req_Place_of_Delivery%>">
									</div>
								</div>


								<div class="clearfix"></div>
								<div class="col-md-2" id="boundtruckhd">
									<div class="form-group" style="margin-top: 5%">Bonded
										Truck :</div>
								</div>
								<div class="col-md-4" id="boundtruckval">
									<div class="form-group">
										<select class="form-control" name="bondedtruck">


											<%
												if (Req_Bonded_Truck == "") {
											%>
											<option>Yes</option>
											<option>No</option>
											<%
												} else {
													if (Req_Bonded_Truck!=null && Req_Bonded_Truck.equals("Yes")) {
											%>
											<option selected="true">Yes</option>
											<%
												} else {
											%>
											<option>Yes</option>
											<%
												}
													if (Req_Bonded_Truck!=null && Req_Bonded_Truck.equals("No")) {
											%>
											<option selected="true">No</option>
											<%
												} else {
											%>
											<option>No</option>
											<%
												}

												}
											%>



										</select>

									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Customer
										Reference No:</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control"
											placeholder="Customer Reference No" type="text"
											name="customer_ref_no" value="<%=Req_Reference_No%>">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2" id="noofconthd">
									<div class="form-group" style="margin-top: 5%">No. of
										Container :</div>
								</div>
								<div class="col-md-1" id="noofcontval">
									<div class="form-group">
										<select class="form-control" name="nocont" id="nocont"
											style="padding: 0px !important;">
											<option></option>
											<option>1</option>
											<option>2</option>
											<option>3</option>
											<option>4</option>
											<option>5</option>
										</select>
									</div>
								</div>
								<div class="col-md-2" id="sizeofconthd">
									<div class="form-group" style="margin-top: 5%">Size of
										Container :</div>
								</div>
								<div class="col-md-1" id="sizeofcontval">
									<div class="form-group">
										<select class="form-control" name="sizecont" id="sizecont"
											style="padding: 0px !important;">
											<option></option>
											<option>20'</option>
											<option>40'</option>

										</select>
									</div>
								</div>
								<div class="col-md-2" id="typeofconthd">
									<div class="form-group" style="margin-top: 5%">Type of
										Container :</div>
								</div>
								<div class="col-md-2" id="typeofcontval">
									<div class="form-group demo">
										<select class="form-control" name="typecont" id="typecont"
											style="padding: 0px !important;">
											<option>GP</option>
											<option>DG</option>
											<option>RF</option>
											<option>HC</option>
											<option>Other</option>
										</select>

									</div>

								</div>
								<div class="col-md-2" id="typeofconthd_add">
									<div class="form-group">
										<a href="javascript:;" class="btn btn-primary"
											id="btn_add_container">Add</a>
									</div>
								</div>

								<div class="clearfix"></div>
								<div id="typeofconthd_added"
									class="col-md-12 bg-blue container_box"
									style="padding-top: 10px; margin-bottom: 10px;">

									<%-- <%
										try {
											String No_of_Container = "", Size_of_Container = "", Type_of_Container = "";
											PreparedStatement ps = conn
													.prepareStatement("select * from request_container where request_id='" + Req_id + "'");
											ResultSet rs1 = ps.executeQuery();
											int i = 1;
											while (rs1.next()) {
												No_of_Container = rs1.getString(3);
												Size_of_Container = rs1.getString(4);
												Type_of_Container = rs1.getString(5);
									%>
									<div class="col-md-12 container_block">
										<div class="col-md-1">
											<div class="form-group" style="margin-top: 5%">

												<span id="sno"><%=i%></span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group" style="margin-top: 5%">No. of
												Container :</div>
										</div>
										<div class="col-md-1">
											<div class="form-group" style="margin-top: 5%">
												<input class="form-control" type="hidden"
													name="multi_nocont" value="<%=No_of_Container%>"> <span><%=No_of_Container%></span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group" style="margin-top: 5%">Size of
												Container :</div>
										</div>
										<div class="col-md-1">
											<div class="form-group" style="margin-top: 5%">
												<input class="form-control" type="hidden"
													name="multi_sizecont" value="<%=Size_of_Container%>">
												<span><%=Size_of_Container%></span>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group" style="margin-top: 5%">Type of
												Container :</div>
										</div>
										<div class="col-md-2">
											<div class="form-group demo" style="margin-top: 5%">
												<input class="form-control" type="hidden"
													name="multi_typecont" value="<%=Type_of_Container%>">
												<span><%=Type_of_Container%></span>
											</div>
										</div>
										<a href="javascript:;"
											class="btn btn-primary remove_container">Delete</a>
									</div>
									<%
										i++;
											}
										} catch (Exception e) {
											System.out.println(e);
										}
									%> --%>



								</div>


								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Remarks :
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<textarea class="form-control" rows="6" type="text" name="remark"><%=Req_Remark%></textarea>
									</div>
								</div>
								<%
								if (Req_Status.equalsIgnoreCase("accepted")) {
									%>
								
									<div class="clearfix"></div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">Status :
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<select class="form-control" name="status">
											<option value=""></option>
											<option>Draft Ok</option>
											<option>Draft Ready</option>
											</select>
										</div>
									</div>
									<%
								}
								%>
								<div class="clearfix"></div>
								<div class="col-md-2">

									<div class="form-group" style="margin-top: 5%">Upload
										Files :</div>

								</div>

								<div class="col-md-10">

									<div class="form-group optionBox">
										<!-- <input name="file" type="file" id="file" multiple="true">-->
										<div class="col-md-12">
											<%
												try {
													String upfile = "",upfile_name="";
													PreparedStatement ps = conn.prepareStatement(
															"select * from request_upload where req_id='" + Req_id + "'");
													ResultSet rs1 = ps.executeQuery();
													int i = 1;
													while (rs1.next()) {
														upfile = rs1.getString(3);
														upfile_name = rs1.getString("UPLOAD_FILE");
											%>
											<div class="col-md-2" id="prev_<%=i%>">
												<input type="hidden" name="req_img_path" value="<%=upfile%>">
												<a class="upload_<%= rs1.getString("UPLOAD_BY_TYPE") %>" href="uploadproduct/<%=upfile%>"
													download="uploadproduct/<%=upfile%>" target="_blank"><%= upfile_name %></a>&nbsp;
												<a href="javascript:;" class="remove" style="color:red">Remove</a>
											</div>
											<%
												i++;
													}
												} catch (Exception e) {
													System.out.println(e);
												}
											%>
											<br>
										</div>


										<div class="col-md-4 block">
											<input type="file" name="uploadfiles"
												multiple="true">
										</div>
									</div>

								</div>
								<div class="clearfix"></div>
								<div class="col-md-10 pull-right">



									<div class="form-group">
										<a href="javascript:;" class="btn btn-primary add">Add</a>

									</div>

								</div>
								<div class="clearfix"></div>
								<input type="hidden" name="id" value="<%=expid%>" /> <input
									type="hidden" name="uid"
									value="<%=session.getAttribute("uname")%>">
								<%
									String cmpname = "";
									try {
										// Class.forName("com.mysql.jdbc.Driver");
										// Connection con = DriverManager.getConnection(host, username, password);

										PreparedStatement ps8 = conn
												.prepareStatement("select * from reg where id='" + session.getAttribute("uname") + "'");
										ResultSet rs8 = ps8.executeQuery();
										while (rs8.next()) {
											cmpname = rs8.getString(6);
										}
									} catch (Exception e) {
										System.out.println(e);
									}
								%>

								<input type="hidden" name="cmpname" value="<%=cmpname%>" />
								<div class="col-md-12" align="center">
									<br> <input type="hidden" name="reqid" value="<%=Req_id%>">
									<input type="hidden" name="reqtype" value="<%=Req_PostType%>">
									<input type="hidden" name="reqjobno" value="<%=Req_job_no%>">
									<%
										if (Req_id == "") {
									%>
									<span class="btn btn-primary"><input type="checkbox"
										name="ismail"> Mail Send </span>
<!-- 									<button type="submit" class="btn btn-primary">Submit</button> -->
									<button type="button" class="btn btn-primary" onclick="this.form.submit(); this.disabled=true;">Submit</button>
									<%
										} else {
									%>
<!-- 									<button type="submit" class="btn btn-primary">Update</button> -->
									<button type="button" class="btn btn-primary" onclick="this.form.submit(); this.disabled=true;">Update</button>
									<%
										}
									%>


								</div>
							</form>
						</div>
					</div>
				</div>
			</section>

			<!-- /.content -->

		</div>

		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
          
                     immediately after the control sidebar -->

	</div>



	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->

	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

	<!-- jQuery UI 1.11.4 -->

	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>


	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

	<script>
		// $.widget.bridge('uibutton', $.ui.button);
	</script>

	<!-- Bootstrap 3.3.5 -->

	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.min.js"></script>
	<!-- daterangepicker -->

	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<script src="ltmcss/js/app.min.js"></script>

	<!-- AdminLTE dashboard demo (This is only for demo purposes) 

        <script src="ltmcss/js/pages/dashboard.js"></script> -->

	<!-- AdminLTE for demo purposes -->

	<script src="ltmcss/js/demo.js"></script>

	<script>
		$(function() {
			// typecont
			//$("#typecont").dropdownchecklist({ firstItemChecksAll: true, maxDropHeight: 100 });

			$('#ed_date').datepicker({
				format : "dd/mm/yyyy"
			});
			//$('#ed_date').datepicker('setDate', 'today');
	<%if (Req_id == "" || Req_EXP_Type.equals("Air")) {%>
		$("#divfcllclhd").hide();
			$("#divfcllclval").hide();

			/* $("#noofconthd").hide();
			$("#noofcontval").hide();
			$("#sizeofconthd").hide();
			$("#sizeofcontval").hide();
			$("#typeofconthd").hide();
			$("#typeofcontval").hide();
			$('#typeofconthd_add').hide();
			$('#typeofconthd_added').hide(); */
	<%} else if (Req_FCLLCL.equals("LCL")) {%>
			/* $("#noofconthd").hide();
			$("#noofcontval").hide();
			$("#sizeofconthd").hide();
			$("#sizeofcontval").hide();
			$("#typeofconthd").hide();
			$("#typeofcontval").hide();
			$('#typeofconthd_add').hide();
			$('#typeofconthd_added').hide(); */
	<%}%>
		
	<%if (Req_id == "" || !Req_EXP_Type.equals("Cross Border")) {%>
		$("#placedelhd").hide();
			$("#placedelval").hide();

			$("#boundtruckhd").hide();
			$("#boundtruckval").hide();
	<%}%>
		$("#ddl_extype").change(function() {
				var selddl_export = $('option:selected', this).text();
				$("#fcl").val('FCL');
				if (selddl_export == "Air") {
					$("#divfcllclhd").hide();
					$("#divfcllclval").hide();
					
					
					/* $("#noofconthd").hide();
					$("#noofcontval").hide();
					$("#sizeofconthd").hide();
					$("#sizeofcontval").hide();
					$("#typeofconthd").hide();
					$("#typeofcontval").hide();
					$('#typeofconthd_add').hide();
					$('#typeofconthd_added').hide(); */
					
					
					
				} else {
					$("#divfcllclhd").show();
					$("#divfcllclval").show();
					
					/* $("#noofconthd").show();
					$("#noofcontval").show();
					$("#sizeofconthd").show();
					$("#sizeofcontval").show();
					$("#typeofconthd").show();
					$("#typeofcontval").show();
					$('#typeofconthd_add').show(); */
					
					
					var numItems = $('.container_block').length;
					if (numItems > 0) {
						$('#typeofconthd_added').show();
					}

				}
				if (selddl_export == "Cross Border") {
					$("#desporhd").hide();
					$("#desporval").hide();
					$("#placedelhd").show();
					$("#placedelval").show();
					$("#boundtruckhd").show();
					$("#boundtruckval").show();
				} else {
					$("#desporhd").show();
					$("#desporval").show();
					$("#placedelhd").hide();
					$("#placedelval").hide();
					$("#boundtruckhd").hide();
					$("#boundtruckval").hide();
				}
			});
		// Start Container Hide
		$("#noofconthd").hide();
		$("#noofcontval").hide();
		$("#sizeofconthd").hide();
		$("#sizeofcontval").hide();
		$("#typeofconthd").hide();
		$("#typeofcontval").hide();
		$('#typeofconthd_add').hide();
		$('#typeofconthd_added').hide();
		// END Container Hide
		
			$("#fcl").change(function() {
				var fcl = $('option:selected', this).text();
				if (fcl == "LCL") {
					/* $("#noofconthd").hide();
					$("#noofcontval").hide();
					$("#sizeofconthd").hide();
					$("#sizeofcontval").hide();
					$("#typeofconthd").hide();
					$("#typeofcontval").hide();
					$('#typeofconthd_add').hide();
					$('#typeofconthd_added').hide(); */
					
				} else {
					/* $("#noofconthd").show();
					$("#noofcontval").show();
					$("#sizeofconthd").show();
					$("#sizeofcontval").show();
					$("#typeofconthd").show();
					$("#typeofcontval").show();
					$('#typeofconthd_add').show(); */
					
					var numItems = $('.container_block').length;
					if (numItems > 0) {
						$('#typeofconthd_added').show();
					}
				}
			});
			$('.add')
					.click(
							function() {
								$('.block:last')
										.after(
												'<div class="col-md-4 block"><input type="file" name="uploadfiles" required="required" multiple="true"><a href="javascript:;" class="remove">Remove</a></div>');
							});
			$('.optionBox').on('click', '.remove', function() {
				$(this).parent().remove();
			});

			$('#btn_add_container')
					.click(
							function() {

								var typecont = $("#typecont option:selected")
										.text();

								var nocont = $("#nocont option:selected")
										.text();
								var sizecont = $("#sizecont option:selected")
										.text();

								if (typecont != "" && nocont != ""
										&& sizecont != "") {
									$('.container_box')
											.append(
													'<div class="col-md-12 container_block"><div class="col-md-1" ><div class="form-group" style="margin-top:5%"><span id="sno">1</span></div></div><div class="col-md-2"><div class="form-group" style="margin-top:5%"> No. of Container : </div></div><div class="col-md-1"><div class="form-group" style="margin-top:5%"><input class="form-control" type="hidden" name="multi_nocont"   value="'+nocont+'" ><span>'
															+ nocont
															+ '</span></div></div><div class="col-md-2"><div class="form-group" style="margin-top:5%"> Size of Container : </div></div><div class="col-md-1"><div class="form-group" style="margin-top:5%"><input class="form-control" type="hidden" name="multi_sizecont"  value="'+sizecont+'"  ><span>'
															+ sizecont
															+ '</span></div></div><div class="col-md-2"><div class="form-group" style="margin-top:5%"> Type of Container : </div></div><div class="col-md-2"><div class="form-group demo" style="margin-top:5%"><input class="form-control" type="hidden" name="multi_typecont"  value="'+typecont+'"><span>'
															+ typecont
															+ '</span></div></div><a href="javascript:;" class="btn btn-primary remove_container">Delete</a></div>');
									set_Container_Sno();
									$('#typeofconthd_added').show();
								} else {
									alert("Please Select All Details");
								}
							});
			$('.container_box').on('click', '.remove_container', function() {
				$(this).parent().remove();
				var numItems = $('.container_block').length;
				if (numItems > 0) {
					set_Container_Sno();
				} else {
					$('#typeofconthd_added').hide();
				}

			});
			
			

		});
		function set_Container_Sno() {
			$('.container_block').each(function(i, obj) {
				console.log(i + " " + obj);
				$(this).find('#sno').text((i + 1));
			});
		}
	</script>








</body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>