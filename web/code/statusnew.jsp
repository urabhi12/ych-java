<%@page import="java.time.LocalTime"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = null;

    try {
        String status = request.getParameter("status");
        String statuss = "";

        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement pss = conn.prepareStatement("select * from status");
        ResultSet rs = pss.executeQuery();
        while (rs.next()) {
            statuss = rs.getString(1);

            if (statuss.equals(status)) {
                out.print("<script>alert('Status is already add');window.location.href='../status_new.jsp'</script>");
            }
        }
        PreparedStatement ps = conn.prepareStatement("insert into status values(?,?,?,?)");
        ps.setString(1, status);
        ps.setString(2, y);
        ps.setString(3, y);
        ps.setObject(4, null);//set by sequence
        
        int n = ps.executeUpdate();
        if (n == 1) {
            out.println("<script>alert('Status add successfully');window.location.href='../status_view.jsp'</script>");
        } else {
            out.println("<script>alert('Status is not add');window.location.href='../status_view.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>