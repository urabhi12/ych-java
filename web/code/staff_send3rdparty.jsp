<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="pack1.GenerateXML"%>
<%@page import="pack1.Send_Email" %>
<%@page import="java.util.ArrayList"%>

<%
 
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
    
    Date d = new Date(); 
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
    String datetime = sdf.format(d);
    try {
        String tid = request.getParameter("tid");
        String handby = request.getParameter("handby");
        String vcctype = request.getParameter("u");
        
        PreparedStatement ps_req_details = conn.prepareStatement("select * from req where tid='" + tid + "'");
        ResultSet rs_req_details = ps_req_details.executeQuery();
        
        PreparedStatement ps_req_upload = conn.prepareStatement("SELECT * FROM request_upload where req_id='" + tid + "'");
        ResultSet rs_req_upload = ps_req_upload.executeQuery();
        
        PreparedStatement ps_req_staff_det = conn.prepareStatement("SELECT * FROM reg where id='" + handby + "'");
        ResultSet rs_req_staff_det = ps_req_staff_det.executeQuery();
        
        String RequestType = "0"; 
        String Requestid = "0"; 
        String Subject = "";
        
        String StaffEmail = "";
        
        String[] cc = new String[1];
        
        
        String vcc_instru = "";
        while (rs_req_staff_det.next()) {
        	StaffEmail = rs_req_staff_det.getString(4);
        	cc[0] =  rs_req_staff_det.getString(4); 
        }
        
        ArrayList<String> arr = new ArrayList<String>();
                
        String XMLContent = "";
        while (rs_req_details.next()) {
            
                String ImportType  = ""; 
                String DateofArrival  = "";                 
                String FCLLCL  = ""; 
               
                
                String ExportType = ""; 
                String ExpectedDateofDeparture = ""; 
                String DestinationPorts = "";  
                String PlaceofDelivery = "";                 
                String Container = ""; 
                String BondedTruck = "";     
                
                
                
                String PermitType = "";
                String CCDFB = "";                 
                
                String DATEOFDELIVERY = "";  
                String TOTALWEIGHT = "";    
                String TOTALNOOFPALLETS = "";   
                String TOTALM3 = ""; 
                String PACKAGEDIMENSION = "";     
                String SPECIALREQUEST = "";   
                String PICKUPADDRESS = ""; 
                String DELIVERYADDRESS = ""; 
                
                 
                Requestid = rs_req_details.getString(1);
                String CustomerReferenceNo = rs_req_details.getString(38);
                String Remarks = rs_req_details.getString(36);                
                Subject = rs_req_details.getString(37);
                vcc_instru = rs_req_details.getString(40);
                
                String company ="";
                PreparedStatement ps_name = conn.prepareStatement("SELECT id, name,cname,type,cust_master_id  FROM reg WHERE id ='"+rs_req_details.getString(2)+"'");
                ResultSet rs_name = ps_name.executeQuery();
                while (rs_name.next()) {
                	if (rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

						String SQl_main_com = "SELECT * FROM reg where id = '" + rs_name.getString(5) + "'";
						PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
						ResultSet rs_main_com = ps_main_com.executeQuery();
						while (rs_main_com.next()) {
							company = rs_main_com.getString(6);
						}

					} else {
						company = rs_name.getString(3);
					}
                	
                }
                
                if(rs_req_details.getString(17).equals("Local Delivery")){     
                    RequestType = "Local Delivery";
                    DATEOFDELIVERY = rs_req_details.getString(4);    
                    TOTALWEIGHT = rs_req_details.getString(29);    
                    TOTALNOOFPALLETS = rs_req_details.getString(30);    
                    TOTALM3 = rs_req_details.getString(31);    
                    PACKAGEDIMENSION = rs_req_details.getString(32);    
                    SPECIALREQUEST = rs_req_details.getString(33);  
                    PICKUPADDRESS = rs_req_details.getString(26);
                    DELIVERYADDRESS = rs_req_details.getString(27);
                    
                    Subject = Subject +"-"+ DATEOFDELIVERY +"-"+ company;
                    
                } else if(rs_req_details.getString(17).equals("New Import")){
                    RequestType = "New Import";
                    
                    ImportType = rs_req_details.getString(3);
                    DateofArrival = rs_req_details.getString(4);
                    FCLLCL = rs_req_details.getString(8);
                    
                    Subject = Subject +"-"+ DateofArrival +"-"+ company;
                    
                } else if(rs_req_details.getString(17).equals("New Export")){
                    RequestType = "New Export";
                    FCLLCL = rs_req_details.getString(8);
                    ExportType = rs_req_details.getString(7);
                    ExpectedDateofDeparture = rs_req_details.getString(4);
                    DestinationPorts = rs_req_details.getString(9);
                    PlaceofDelivery = rs_req_details.getString(10);                     
                    
                    try {
                        String NoOfContainer="",SizeOfContainer="",TypeOfContainer="";
                        PreparedStatement ps_container = conn.prepareStatement("select * from request_container where request_id='" + tid + "'");
                        ResultSet rs_container = ps_container.executeQuery();
                        int ci = 1;
                        while (rs_container.next()) {                          
                        	NoOfContainer = rs_container.getString(3);
                        	SizeOfContainer = rs_container.getString(4);
                        	TypeOfContainer = rs_container.getString(5);    
                        	Container = Container                                           
                        	  + "        <NoofContainer> " + NoOfContainer + "</NoofContainer>\n"                        
                              + "        <SizeofContainer> " + SizeOfContainer + "</SizeofContainer>\n"
                              + "        <TypeofContainer> " + TypeOfContainer + "</TypeofContainer>\n";
                        	
							ci++;
                        }
                        if(ci >0){  
                        	 
                        }
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    
                    
                    
                    
                    BondedTruck = rs_req_details.getString(14);    
                    Subject = Subject +"-"+ ExpectedDateofDeparture +"-"+ company;
                    
                } else if(rs_req_details.getString(17).equals("Request of Permit")){
                    RequestType = "Request of Permit";
                    PermitType = rs_req_details.getString(15);    
                    CCDFB = rs_req_details.getString(4);  
                    
                    Subject = Subject +"-"+ CCDFB +"-"+ company;
                }
                XMLContent = "    <RequestType> " + RequestType + " </RequestType>\n"
                        + "    <Requestid> " +  Requestid + " </Requestid>\n"
                        + "    <ImportType> " +  ImportType + " </ImportType>\n"
                        + "    <DateofArrival> " +  DateofArrival + " </DateofArrival>\n"
                        + "    <FCLLCL> " +  FCLLCL + " </FCLLCL>\n"                      
                        + "    <CustomerReferenceNo> " +  CustomerReferenceNo + " </CustomerReferenceNo>\n"                         
                        + "    <Remarks> " +  Remarks + " </Remarks>\n"
                        + "    <ExportType> " +  ExportType + " </ExportType>\n"
                        + "    <ExpectedDateofDeparture> " +  ExpectedDateofDeparture + " </ExpectedDateofDeparture>\n"
                        + "    <DestinationPorts> " +  DestinationPorts + " </DestinationPorts>\n"
                        + "    <PlaceofDelivery> " +  PlaceofDelivery + " </PlaceofDelivery>\n"
                        + "    <Container> " +  Container  + " </Container>\n"
                        + "    <BondedTruck> " +  BondedTruck + " </BondedTruck>\n"
                        + "    <PermitType> " +  PermitType + " </PermitType>\n"
                        + "    <CCDFB> " +  CCDFB + " </CCDFB>\n"
                        + "    <DATEOFDELIVERY> " +  DATEOFDELIVERY + " </DATEOFDELIVERY>\n"
                        + "    <TOTALWEIGHT> " +  TOTALWEIGHT + " </TOTALWEIGHT>\n"
                        + "    <TOTALNOOFPALLETS> " +  TOTALNOOFPALLETS + " </TOTALNOOFPALLETS>\n"
                        + "    <TOTALM3> " +  TOTALM3 + " </TOTALM3>\n"
                        + "    <PACKAGEDIMENSION> " +  PACKAGEDIMENSION + " </PACKAGEDIMENSION>\n"
                        + "    <SPECIALREQUEST> " +  SPECIALREQUEST + " </SPECIALREQUEST>\n"
                        + "    <PICKUPADDRESS> " +  PICKUPADDRESS + " </PICKUPADDRESS>\n"
                        + "    <DELIVERYADDRESS> " +  DELIVERYADDRESS + " </DELIVERYADDRESS>\n";
        }
        String File="";
        while (rs_req_upload.next()) {
             File =File.equals("")? rs_req_upload.getString(3): File+","+rs_req_upload.getString(3);
             arr.add(Send_Email.getPath()+rs_req_upload.getString(3));
        }
        XMLContent += "    <UploadFiles> " + File + "</UploadFiles>\n";
        
        if(!Requestid.equals("0")){
            
            
            
            XMLContent = "<?xml version='1.0'?>\n"
           				+"<YCH>\n"
           				+"  <Request>\n"+XMLContent; 
             
            XMLContent += "  </Request>\n"
            			+"</YCH>";
            
            /* String Filepath =  GenerateXML.XMLGenerate(XMLContent,Requestid+".xml");
            arr.add(Filepath); */
            
            
            String Message = vcc_instru;  //XMLContent;
            
            
           
          /*   String[] emails = new String[2];
            emails[0] =  "mohit.r@softgentechnologies.com";
            emails[1] =  "jensen@aide-technologies.com"; */
            
            
            String[] emails = new String[1];
            emails[0] = "YCH.All@vccportal.net";
            
            
          
            /* 
            Main Email ID 
            
            String[] emails = new String[1];
            emails[0] =  "YCH.All@vccportal.net"; */
            
            /* String[] emails = new String[1];
            emails[0] =  "VCCFWD.UAT@vcargocloud.com"; */
            
            /* String[] emails = new String[1];
            emails[0] =  "mohit.r@softgentechnologies.com";  */
            
            Subject = Subject +"-";
            if(vcctype.equals("1")){
            	 Subject = Subject +"URGENT";
            }
            
           
           
                    
            boolean mailstatus =  Send_Email.sendwithAttachment(emails, StaffEmail,cc, null,Subject, Message,arr);
            boolean issent;
            if(mailstatus){
                issent=true; 

               
                // For Log Maintain
                String StrSQL = "INSERT INTO request_update_log"
                + "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
                + "VALUES "
                + "('"+tid+"','Sent to VCC','"+handby+"','"+datetime+"','"+datetime+"','"+datetime+"')";
        
                PreparedStatement req_log = conn.prepareStatement(StrSQL);
                int n2 = req_log.executeUpdate(); 
                
                
                // For XML Generation Status Maintain
                String StrSQL_IsSend = "Update req set is_send_vcc = '1' where tid  = '"+tid+"'";        
                PreparedStatement ps_IsSend = conn.prepareStatement(StrSQL_IsSend);
                int exc_IsSend = ps_IsSend.executeUpdate(); 
                
                
                out.println("<script>alert('Details Send');window.location.href='../staff_request_detail.jsp'</script>");
            }else{
                issent=false;
                out.println("<script>alert('Details Not Send');window.location.href='../staff_request_detail.jsp'</script>");
            }
            
            
        }else{
            out.println("<script>alert('Error On XML Generation');window.location.href='../staff_request_detail.jsp'</script>");
        }
        
        
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>