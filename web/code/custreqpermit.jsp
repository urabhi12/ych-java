<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    } 
%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
 
    
    try {
        String id = request.getParameter("id");
        String uid = request.getParameter("uid");
        String da = request.getParameter("da");
        String pertype = request.getParameter("pertype");

 
        PreparedStatement ps = conn.prepareStatement("insert into req values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, id);
        ps.setString(2, uid);
        ps.setString(3, "");
        ps.setString(4, da);
        ps.setString(5, y);
        ps.setString(6, y);
        ps.setString(7, "");
        ps.setString(8, "");
        ps.setString(9, "");
        ps.setString(10, "");
        ps.setString(11, "");
        ps.setString(12, "");
        ps.setString(13, "");
        ps.setString(14, "");
        ps.setString(15, pertype);

        int n = ps.executeUpdate();
        if (n == 1) 
        {   
            PreparedStatement psi = conn.prepareStatement("update sno set sno=sno+1");
            psi.executeUpdate();
            out.println("<script>alert('Request submission is Successfull');window.location.href='../cust_request_of_permit.jsp'</script>");
        } else {
            out.println("<script>alert('Request submission is not Successfull');window.location.href='../cust_request_of_permit.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>