<%-- 
    Document   : delete_email
    Created on : Jun 15, 2018, 1:42:26 PM
    Author     : Mohit
--%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    } 
%>
<%     
	ConnectionManager cm = null; 
    try {
        String id = request.getParameter("id");
        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement ps = conn.prepareStatement("Delete from email_master where id = ?");
        ps.setObject(1,id);
        int n = ps.executeUpdate();
        if (n == 1) 
        {
            //out.println("<script>alert('Email Deletion is Successfully'); window.location.href='../email_master.jsp'</script>");
            response.sendRedirect("../email_master.jsp?error=00");
        } else {
            //out.println("<script>alert('Email Deletion is not Successfull'); window.location.href='../email_master.jsp'</script>");
            response.sendRedirect("../email_master.jsp?error=11");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }

%>