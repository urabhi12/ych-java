<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = null;
    
    try {
        String s = request.getParameter("m");
        
        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement ps = conn.prepareStatement("delete from location where location=?");
        ps.setObject(1,s);
        int n = ps.executeUpdate();
        if (n == 1) 
        {
            out.println("<script>alert('Location delete successfully');window.location.href='../location_view.jsp'</script>");
        } else {
            out.println("<script>alert('Location is not delete');window.location.href='../location_view.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>