<%@page import="pack1.ConnectionManager"%>
<%@page import="java.time.temporal.ChronoUnit"%>
<%@page import="java.time.LocalTime"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.io.File"%>
<%@page import="java.io.FileWriter"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="pack1.Send_Email"%>
<%@page import="java.util.Random"%>

<%
	String uname = request.getParameter("id");
String pwd = request.getParameter("pwd");

String count = "";
String userid = "";
String dt = "";
String type = "";
String pwdd = "";
String flag = "1";
String name = "";
String company = "";
String isblocked = "";

LocalTime time = LocalTime.now();
String tt = time.toString();

Date cur_date = new Date();
SimpleDateFormat sdf_cur_date = new SimpleDateFormat("dd/MMM/yyyy");
String str_cur_date = sdf_cur_date.format(cur_date);

ConnectionManager cm = null;

try {
	cm = new ConnectionManager();
	Connection conn = cm.getCon();
	PreparedStatement ps = conn.prepareStatement("select * from reg where id=? and flag=?");
	ps.setObject(1, uname);
	ps.setObject(2, flag);
	ResultSet rs = ps.executeQuery();

	if (rs.next()) {
		userid = rs.getString(15);
		name = rs.getString(2);
		company = rs.getString(6);
		pwdd = rs.getString(5);
		count = rs.getString(9);
		dt = rs.getString(11);
		type = rs.getString(8);
		isblocked = rs.getString(17);

		LocalTime localDate = LocalTime.parse(dt);
		long minutes = ChronoUnit.MINUTES.between(time, localDate);

		int UserCount = 5;
		if (type.equals("Customer") || type.equals("Customer-Sub")) {
	UserCount = 3;
		}
		if (Integer.parseInt(count) < UserCount) {
	if (pwd.equals(pwdd)) {
		if (isblocked.equals("1")) {
			out.println(
					"<script>alert('Your Account has Blocked, Please Contact to Admin');window.location.href='../index.jsp';</script>");
		} else {

			PreparedStatement ps_login_log = conn.prepareStatement("SELECT * FROM login_log WHERE date_ = ? ");
			ps_login_log.setObject(1, str_cur_date);
			ResultSet rs_login_log = ps_login_log.executeQuery();
			int login_found = 0;
			while (rs_login_log.next()) {
				login_found = 1;

			}
			if (login_found == 0) {
				String StrSQL_login_log = "INSERT INTO login_log(date_,time, user_id) VALUES (? ,?,?)";
				PreparedStatement req_login_log = conn.prepareStatement(StrSQL_login_log);
				req_login_log.setObject(1, str_cur_date);
				req_login_log.setObject(2, tt);
				req_login_log.setObject(3, userid);
				int n2 = req_login_log.executeUpdate();
				//DON"T BACKUP MYSQL DB. WE ARE MOVING TO ORACLE
				//LINUX ENV DIFFERENT PATH
				// 							try {
				// 						        Date d = new Date();
				// 						        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMddHHmmss");
				// 						        String curdate = sdf.format(d);
				// 						        Runtime runtime = Runtime.getRuntime();
				// 						        String DbName = "dbbkup_"+curdate+".sql";
				// 						        File backupFile = new File(Send_Email.getPath()+"backup//"+DbName);
				// 						        FileWriter fw = new FileWriter(backupFile);
				// 						        //Process child = runtime.exec("D:\\Workspace\\xampp\\mysql\\bin\\mysqldump --user=root --password= --lock-all-tables --opt ych_server");
				// 						       // Process child = runtime.exec("C://Program Files (x86)//MySQL//MySQL Server 5.0//bin//mysqldump --user=root --password=AIDE@123 --lock-all-tables --opt ych-dev");
				// 						        Process child = runtime.exec("C://Program Files (x86)//MySQL//MySQL Server 5.0//bin//mysqldump --user=root --password=AIDE@123 --lock-all-tables --opt ych");
				// 						        InputStreamReader irs = new InputStreamReader(child.getInputStream());
				// 						        BufferedReader br = new BufferedReader(irs);

				// 						        String line;
				// 						        while( (line=br.readLine()) != null ) {
				// 						            fw.write(line + "\n");
				// 						        }
				// 						        fw.close();
				// 						        irs.close();
				// 						        br.close();

				// 						    } catch (IOException ex) {
				// 						        out.println(ex.toString());
				// 						        ex.printStackTrace();

				// 						    }

			}

			// For Login Check
			Date logcurdate = new Date();
			String y = logcurdate.toGMTString();

			Random randid = new Random();
			int token = randid.nextInt(90000) + 10000;

			PreparedStatement pslogin_acc_del = conn
					.prepareStatement("delete from login_access where   user_id = ?");
			pslogin_acc_del.setObject(1, userid);
			int nacc_del = pslogin_acc_del.executeUpdate();

			String StrSQL_login_acc = "INSERT INTO login_access(user_id,token, created_date) VALUES ( ? , ? , ?)";
			PreparedStatement req_login_acc = conn.prepareStatement(StrSQL_login_acc);
			req_login_acc.setObject(1,userid);
			req_login_acc.setObject(2, token);
			req_login_acc.setObject(3,y);
			int nlogin_acc = req_login_acc.executeUpdate();

			session.setAttribute("logtoken", token);

			if (type.equals("Admin")) {
				PreparedStatement ps1 = conn
						.prepareStatement("update reg set count=0, dt=? where id=?");
				ps1.setObject(1,tt);
				ps1.setObject(2,uname);
				ps1.executeUpdate();
				session.setAttribute("uname", uname);
				session.setAttribute("name", name);
				session.setAttribute("company", company);
				session.setAttribute("user_id", userid);
				session.setAttribute("type", type);
				response.sendRedirect("../staff_profile_details.jsp");
			}
			if (type.equals("Staff")) {
				PreparedStatement ps2 = conn
						.prepareStatement("update reg set count=0, dt=? where id=?");
				ps2.setObject(1,tt);
				ps2.setObject(2,uname);
				ps2.executeUpdate();
				session.setAttribute("uname", uname);
				session.setAttribute("name", name);
				session.setAttribute("company", company);
				session.setAttribute("user_id", userid);
				session.setAttribute("type", type);
				response.sendRedirect("../staff_request_detail.jsp");
			}
			if (type.equals("Customer") || type.equals("Customer-Sub")) {
				PreparedStatement ps3 = conn
						.prepareStatement("update reg set count=0, dt=? where id=?");
				ps3.setObject(1,tt);
				ps3.setObject(2,uname);
				ps3.executeUpdate();
				session.setAttribute("uname", uname);
				session.setAttribute("name", name);
				session.setAttribute("company", company);
				session.setAttribute("user_id", userid);
				session.setAttribute("type", type);
				response.sendRedirect("../cust_my_request.jsp");
			}
		}
	} else {
		if (minutes > 60) {
			PreparedStatement psi_before_hr = conn
					.prepareStatement("update reg set count = 1, dt = ? where id = ?");
			psi_before_hr.setObject(1,tt);
			psi_before_hr.setObject(2,uname);
			psi_before_hr.executeUpdate();
			out.println("<script>alert('Invalid Password...');window.location.href='../index.jsp';</script>");
		} else {
			if (Integer.parseInt(count) == (UserCount - 1)) {
				PreparedStatement psi_block = conn.prepareStatement("update reg set count = count+1, dt = ?, isblocked = '1' where id = ?");
				psi_block.setObject(1,tt);
				psi_block.setObject(2, uname);
				psi_block.executeUpdate();
				out.println(
						"<script>alert('Your account is blocked. Please contact your admin.');window.location.href='../index.jsp';</script>");
			} else {
				PreparedStatement psi_update = conn.prepareStatement(
						"update reg set count = count+1, dt = ? where id = ?");
				psi_update.setObject(1,tt);
				psi_update.setObject(2, uname);
				psi_update.executeUpdate();
				out.println(
						"<script>alert('Invalid Password...');window.location.href='../index.jsp';</script>");
			}
		}

	}
		} else {
	/*if (minutes > 60) 
	{
	    if(isblocked.equals("1")){
	out.println("<script>alert('User Blocked Please Contact to Admin');window.location.href='../index.jsp';</script>");
	    } else {
	if (pwd.equals(pwdd)) 
	{
	    if (type.equals("Admin")) {
	PreparedStatement ps1 = conn.prepareStatement("update reg set count=0, dt='" + tt + "' where email='" + uname + "'");
	ps1.executeUpdate();
	session.setAttribute("uname", uname);
	session.setAttribute("name", name);
	session.setAttribute("company", company);
	session.setAttribute("user_id", userid);
	session.setAttribute("type", type);
	response.sendRedirect("../staff_profile_details.jsp");
	    }
	    if (type.equals("Staff")) {
	PreparedStatement ps2 = conn.prepareStatement("update reg set count=0, dt='" + tt + "' where email='" + uname + "'");
	ps2.executeUpdate();
	session.setAttribute("uname", uname);
	session.setAttribute("name", name);
	session.setAttribute("company", company);
	session.setAttribute("user_id", userid);
	session.setAttribute("type", type);
	response.sendRedirect("../staff_request_detail.jsp");
	    }
	    if (type.equals("Customer")) {
	PreparedStatement ps3 = conn.prepareStatement("update reg set count=0, dt='" + tt + "' where email='" + uname + "'");
	ps3.executeUpdate();
	session.setAttribute("uname", uname);
	session.setAttribute("name", name);
	session.setAttribute("company", company);
	session.setAttribute("user_id", userid);
	session.setAttribute("type", type);
	response.sendRedirect("../cust_my_request.jsp");
	    }
	
	} else {
	   PreparedStatement psi = conn.prepareStatement("update reg set count=1, dt='" + tt + "' where email='" + uname + "'");
	   psi.executeUpdate();
	   out.println("<script>alert('Invalid Password...');window.location.href='../index.jsp';</script>");
	}
	    }
	
	}
	else
	{
	    
	    out.println("<script>alert('Your Account has Blocked, Please Contact to Admin');window.location.href='../index.jsp';</script>");
	}*/
	out.println(
			"<script>alert('Your account is blocked. Please contact your admin.');window.location.href='../index.jsp';</script>");
		}
	} else {
		out.println("<script>alert('Invalid User Name and Password');window.location.href='../index.jsp';</script>");
	}
} catch (Exception e) {
	out.println(e.getMessage());
} finally {
	if(cm!=null) {
		cm.close();
	}
}
%>

