<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    } 
    ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();
%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    
   
 
    try {
        String id = request.getParameter("id");
        String uid = request.getParameter("uid");
        String da = request.getParameter("da");
        String exptype = request.getParameter("exptype");
        String fcl = request.getParameter("fcl");
        String desport = request.getParameter("desport");
        String placedel = request.getParameter("placedel");
        String nocont = request.getParameter("nocont");
        String sizecont = request.getParameter("sizecont");
        String typecont = request.getParameter("typecont");
        String bondedtruck = request.getParameter("bondedtruck");

 
        PreparedStatement ps = conn.prepareStatement("insert into req values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        ps.setString(1, id);
        ps.setString(2, uid);
        ps.setString(3, "");
        ps.setString(4, da);
        ps.setString(5, y);
        ps.setString(6, y);
        ps.setString(7, exptype);
        ps.setString(8, fcl);
        ps.setString(9, desport);
        ps.setString(10, placedel);
        ps.setString(11, nocont);
        ps.setString(12, sizecont);
        ps.setString(13, typecont);
        ps.setString(14, bondedtruck);
        ps.setString(15, "");

        int n = ps.executeUpdate();
        if (n == 1) 
        {   
            PreparedStatement psi = conn.prepareStatement("update sno set sno=sno+1");
            psi.executeUpdate();
            out.println("<script>alert('Request submission is Successfull');window.location.href='../cust_export_request.jsp'</script>");
        } else {
            out.println("<script>alert('Request submission is not Successfull');window.location.href='../cust_export_request.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>