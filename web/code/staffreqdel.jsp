<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%
	Date d = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	String y = sdf.format(d);

	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	String uname = session.getAttribute("uname").toString();

	try {
		String id = request.getParameter("id");
		String expcmp = request.getParameter("expcmp");
		String cmpname = request.getParameter("cmpname");
		String tx3id = request.getParameter("tx3id");
		String reqtype = request.getParameter("reqtype");
		String pdateto = request.getParameter("pdateto");
		String pdatefrom = request.getParameter("pdatefrom");
		String pudateto = request.getParameter("pudateto");
		String pudatefrom = request.getParameter("pudatefrom");
		String remark = request.getParameter("remark");
		String additional_comment = request.getParameter("additional_comment");
		
		String dateofarival = request.getParameter("dateofarival");
		String vcc_instru = request.getParameter("vcc_instru");
		
		String local_delivery = "";
		if (reqtype.equals("Local Delivery")) {
			String ttl_nf_pallets = request.getParameter("ttl_nf_pallets");
			String ttl_m = request.getParameter("ttl_m");
			String pckg_dimension = request.getParameter("pckg_dimension");
			local_delivery = " tnop = '" + ttl_nf_pallets + "',pkgd = '" + pckg_dimension + "',tm3 = '" + ttl_m
					+ "', ";//, ";            
		}

		// For Date of Arival
		PreparedStatement ps_dateofarival = conn
				.prepareStatement("SELECT * FROM req WHERE da ='" + dateofarival + "' and tid='" + id + "'");
		ResultSet rs_dateofarival = ps_dateofarival.executeQuery();
		int dateofarivalrow = 0;
		while (rs_dateofarival.next()) {
			dateofarivalrow = 1;
		}
		if (dateofarivalrow < 1) {
			String StrSQL_dateofarival = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Date of Arival:- " + dateofarival + "','" + uname + "','" + y + "','" + y
					+ "','" + y + "')";
			PreparedStatement req_dateofarival = conn.prepareStatement(StrSQL_dateofarival);
			int n2 = req_dateofarival.executeUpdate();
		}
		
		
		
		// For Tx3id
		PreparedStatement ps_tx3id = conn
				.prepareStatement("SELECT * FROM req WHERE tx3id ='" + tx3id + "' and tid='" + id + "'");
		ResultSet rs_tx3id = ps_tx3id.executeQuery();
		int tx3idrow = 0;
		while (rs_tx3id.next()) {
			tx3idrow = 1;
		}
		if (tx3idrow < 1) {
			String StrSQL_tx3id = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','New Tx3ID:- " + tx3id + "','" + uname + "','" + y + "','" + y
					+ "','" + y + "')";
			PreparedStatement req_tx3id = conn.prepareStatement(StrSQL_tx3id);
			int n2 = req_tx3id.executeUpdate();
		}

		// For Remark
		PreparedStatement ps_remark = conn
				.prepareStatement("SELECT * FROM req WHERE remarks LIKE '" + remark + "' and tid='" + id + "'");
		ResultSet rs_remark = ps_remark.executeQuery();
		int remarkrow = 0;
		while (rs_remark.next()) {
			remarkrow = 1;
		}
		if (remarkrow < 1) {
			String StrSQL_remark = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','New Remark:- " + remark + "','" + uname + "','" + y + "','"
					+ y + "','" + y + "')";
			PreparedStatement req_remark = conn.prepareStatement(StrSQL_remark);
			int n3 = req_remark.executeUpdate();
		}

		// Expected Completion        
		PreparedStatement ps_expcmp = conn.prepareStatement(
				"SELECT * FROM req WHERE Expected_Comp ='" + expcmp + "' and tid='" + id + "'");
		ResultSet rs_expcmp = ps_expcmp.executeQuery();
		int expcmp_cnt = 0;
		while (rs_expcmp.next()) {
			expcmp_cnt = 1;
		}
		if (expcmp_cnt < 1) {
			String StrSQL_expcmp = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Expected Completion :- " + expcmp + "','" + uname + "','" + y
					+ "','" + y + "','" + y + "')";
			PreparedStatement req_expcmp = conn.prepareStatement(StrSQL_expcmp);
			int n4 = req_expcmp.executeUpdate();
		}

		// Permit Date To        
		PreparedStatement ps_per_dt_to = conn
				.prepareStatement("SELECT * FROM req WHERE pdateto ='" + pdateto + "' and tid='" + id + "'");
		ResultSet rs_per_dt_to = ps_per_dt_to.executeQuery();
		int per_dt_to_cnt = 0;
		while (rs_per_dt_to.next()) {
			per_dt_to_cnt = 1;
		}
		if (per_dt_to_cnt < 1) {
			String StrSQL_per_dt_to = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Permit Date To :- " + pdateto + "','" + uname + "','" + y
					+ "','" + y + "','" + y + "')";
			PreparedStatement req_per_dt_to = conn.prepareStatement(StrSQL_per_dt_to);
			int n4 = req_per_dt_to.executeUpdate();
		}

		// Permit Date From        
		PreparedStatement ps_pdatefrom = conn.prepareStatement(
				"SELECT * FROM req WHERE pdatefrom ='" + pdatefrom + "' and tid='" + id + "'");
		ResultSet rs_pdatefrom = ps_pdatefrom.executeQuery();
		int pdatefrom_cnt = 0;
		while (rs_pdatefrom.next()) {
			pdatefrom_cnt = 1;
		}
		if (pdatefrom_cnt < 1) {
			String StrSQL_pdatefrom = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Permit Date From :- " + pdatefrom + "','" + uname + "','" + y
					+ "','" + y + "','" + y + "')";
			PreparedStatement req_pdatefrom = conn.prepareStatement(StrSQL_pdatefrom);
			int n4 = req_pdatefrom.executeUpdate();
		}

		// Pickup Date To        
		PreparedStatement ps_pudateto = conn.prepareStatement(
				"SELECT * FROM req WHERE pudateto ='" + pudateto + "' and tid='" + id + "'");
		ResultSet rs_pudateto = ps_pudateto.executeQuery();
		int pudateto_cnt = 0;
		while (rs_pudateto.next()) {
			pudateto_cnt = 1;
		}
		if (pudateto_cnt < 1) {
			String StrSQL_pudateto = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Pickup Date To :- " + pudateto + "','" + uname + "','" + y
					+ "','" + y + "','" + y + "')";
			PreparedStatement req_pudateto = conn.prepareStatement(StrSQL_pudateto);
			int n4 = req_pudateto.executeUpdate();
		}

		// Pickup Date From        
		PreparedStatement ps_pudatefrom = conn.prepareStatement(
				"SELECT * FROM req WHERE pudatefrom ='" + pudatefrom + "' and tid='" + id + "'");
		ResultSet rs_pudatefrom = ps_pudatefrom.executeQuery();
		int pudatefrom_cnt = 0;
		while (rs_pudatefrom.next()) {
			pudatefrom_cnt = 1;
		}
		if (pudatefrom_cnt < 1) {
			String StrSQL_pudatefrom = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','Pickup Date From :- " + pudatefrom + "','" + uname + "','" + y
					+ "','" + y + "','" + y + "')";
			PreparedStatement req_pudatefrom = conn.prepareStatement(StrSQL_pudatefrom);
			int n4 = req_pudatefrom.executeUpdate();
		}

		// Special Instruction        
		PreparedStatement ps_special_ins = conn
				.prepareStatement("SELECT * FROM req WHERE additional_comment LIKE '" + additional_comment
						+ "' and tid='" + id + "'");
		ResultSet rs_special_ins = ps_special_ins.executeQuery();
		int special_ins = 0;
		while (rs_special_ins.next()) {
			special_ins = 1;
		}
		if (special_ins < 1) {
			String StrSQL_special_ins = "INSERT INTO request_update_log"
					+ "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
					+ "VALUES " + "('" + id + "','New Status :- " + additional_comment + "','"
					+ uname + "','" + y + "','" + y + "','" + y + "')";
			PreparedStatement req_special_ins = conn.prepareStatement(StrSQL_special_ins);
			int n4 = req_special_ins.executeUpdate();
		}

		
		
		// For Update
		String SQL = "update req set da ='" + dateofarival + "',Expected_Comp ='" + expcmp + "', tx3id ='" + tx3id + "',remarks ='"
				+ remark + "',cmpname='" + cmpname + "',pdateto = '" + pdateto + "'" + ",pdatefrom = '"
				+ pdatefrom + "',pudateto = '" + pudateto + "',pudatefrom='" + pudatefrom
				+ "',  additional_comment = '" + additional_comment + "', vcc_instru = '" + vcc_instru + "'," + local_delivery + " mdate='" + y
				+ "', is_send_vcc = '0' where tid='" + id + "'";

		PreparedStatement ps = conn.prepareStatement(SQL);

		int n = ps.executeUpdate();
		if (n == 1) {
			out.println(
					"<script>alert('Update Successful!');window.location.href='../staff_request_detail.jsp'</script>");
		} else {
			out.println(
					"<script>alert('Update UnSuccessful!');window.location.href='../staff_request_detail.jsp'</script>");
		}
	} catch (Exception e) {
		out.println(e);
	} finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>