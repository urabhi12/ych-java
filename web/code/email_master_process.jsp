<%-- 
    Document   : email_master_process
    Created on : Jun 15, 2018, 12:54:15 PM
    Author     : Mohit
--%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	}

	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	Date d = new Date();
	String y = d.toGMTString();

	try {
		String user_id = request.getParameter("userid");
		String user_type = request.getParameter("usertype");
		String settype = request.getParameter("settype");
		String txt_name = request.getParameter("txt_name");
		String txt_emailid = request.getParameter("txt_emailid");
		String cont = "0";

		PreparedStatement pss_email = conn.prepareStatement(
				"select * from email_master where emails = '" + txt_emailid + "' and set_type = '" + settype
						+ "' and user_id = '" + user_id + "' and  status = '1' ");

		ResultSet rs_email = pss_email.executeQuery();

		while (rs_email.next()) {
			cont = "1";
		}

		if (cont.equals("0")) {

			PreparedStatement ps = conn.prepareStatement("insert into email_master "
					+ "(user_id,user_type,set_type,name,emails,status,createdate,modifydate)" + " values"
					+ "(?,?,?,?,?,?,?,?)");

			ps.setString(1, user_id);
			ps.setString(2, user_type);
			ps.setString(3, settype);
			ps.setString(4, txt_name);
			ps.setString(5, txt_emailid);
			ps.setString(6, "1");
			ps.setString(7, y);
			ps.setString(8, y);

			int n = ps.executeUpdate();
			if (n == 1) {
				response.sendRedirect("../email_master.jsp?error=0");
			} else {
				response.sendRedirect("../email_master.jsp?error=2");
			}
		} else if (cont.equals("1")) {
			response.sendRedirect("../email_master.jsp?error=1");
		} else {
			response.sendRedirect("../email_master.jsp?error=2");
		}
	} catch (Exception e) {
		out.println(e);
	} finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>