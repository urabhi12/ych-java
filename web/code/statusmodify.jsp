<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	Date d = new Date();
String y = d.toGMTString();
ConnectionManager cm = null;

try {
	String status = request.getParameter("status");
	String snew = request.getParameter("snew");

	cm = new ConnectionManager();
	Connection conn = cm.getCon();
	PreparedStatement ps = conn.prepareStatement(
	"update status set status=?,mdate=? where status=?");
	ps.setObject(1, status);
	ps.setObject(2, y);
	ps.setObject(3, snew);
	int n = ps.executeUpdate();
	if (n == 1) {
		out.println("<script>alert('Status update successfully');window.location.href='../status_view.jsp'</script>");
	} else {
		out.println("<script>alert('Status is not update');window.location.href='../status_view.jsp'</script>");
	}
} catch (Exception e) {
	out.println(e);
} finally {
	if(cm!=null) {
		cm.close();
	}
}
%>