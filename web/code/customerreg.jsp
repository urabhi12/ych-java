<%@page import="java.time.LocalTime"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    } 
%>
<%
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
    Date d = new Date();
    String y = d.toGMTString();
    
    LocalTime time = LocalTime.now();
    String tt = time.toString();
    
    try {
        String id = request.getParameter("id");
        String cno = request.getParameter("cno");
        String uname = request.getParameter("uname");
        String email = request.getParameter("email");
        String pwd = request.getParameter("pwd");
        String location = request.getParameter("location");
        String cname = request.getParameter("cname");
        //String rname = request.getParameter("rname");    
        String ddl_cso = request.getParameter("ddl_cso");    
        String rname = "";
        String idd = "";
        String emaill = "";
        String cont = "0";
        
        
         
        PreparedStatement pss_email = conn.prepareStatement("select * from reg where email = ? and flag = '1'");
        pss_email.setObject(1, email);
        ResultSet rs_email = pss_email.executeQuery();
        
        while (rs_email.next()) {            
            
                cont = "1";
            
        }
        PreparedStatement pss_id = conn.prepareStatement("select * from reg where id = ? and flag = '1'");
        pss_id.setObject(1, id);
        ResultSet rs_id = pss_id.executeQuery();
        
        while (rs_id.next()) {
            
             
                cont = "2";
            
        }
        if(cont.equals("0")){
            PreparedStatement ps = conn.prepareStatement("insert into reg values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            ps.setString(1, id);
            ps.setString(2, uname);
            ps.setString(3, cno);
            ps.setString(4, email);
            ps.setString(5, pwd);
            ps.setString(6, cname);
            ps.setString(7, rname);
            ps.setString(8, "Customer");
            ps.setString(9, "0");
            ps.setString(10, "1");
            ps.setString(11, tt);
            ps.setString(12, location);
            ps.setString(13, y);
            ps.setString(14, y);
            ps.setObject(15, null);//value populated by sequence
            ps.setString(16, "");
            ps.setInt(17, 0);
            ps.setString(18, ddl_cso);
            int n = ps.executeUpdate();
            if (n == 1) 
            {
                out.println("<script>alert('Registration is Successfull');window.location.href='../customer_profile_details.jsp'</script>");
            }else {
                out.println("<script>alert('Registration is not Successfull');window.location.href='../customer_profile_details.jsp'</script>");
            }
        }else if(cont.equals("1")){
             out.println("<script>alert('Email Id Already in Use');window.location.href='../customer_profile_details.jsp'</script>");
        }else if(cont.equals("2")){
             out.println("<script>alert('User Id Already in Use');window.location.href='../customer_profile_details.jsp'</script>");
        }else{
            
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>