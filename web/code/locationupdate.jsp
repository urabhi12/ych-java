<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = null;
    
    try {
        String lname = request.getParameter("lname");
        String lnew = request.getParameter("lnew");
        
        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement ps = conn.prepareStatement("update location set location=?,mdate=? where location=?");
        ps.setObject(1,lname);
        ps.setObject(2,y);
        ps.setObject(3,lnew);
        int n = ps.executeUpdate();
        if (n == 1) 
        {
            out.println("<script>alert('Updation is Successfull');window.location.href='../location_view.jsp'</script>");
        } else {
            out.println("<script>alert('Updation is not Successfull');window.location.href='../location_view.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>