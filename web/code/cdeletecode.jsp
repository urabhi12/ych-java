<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    } 
%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = null;
  
    
    try {
        String s = request.getParameter("m");

//        Class.forName("com.mysql.jdbc.Driver");
//        Connection con = DriverManager.getConnection(host, username, password);
        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement ps = conn.prepareStatement("update reg set flag='0',mdate=? where id=?");
        ps.setObject(1,y);
        ps.setObject(2,s);
        int n = ps.executeUpdate();
        if (n == 1) 
        {
            out.println("<script>alert('Deletion is Successfull');window.location.href='../customer_profile_details.jsp'</script>");
        } else {
            out.println("<script>alert('Deletion is not Successfull');window.location.href='../customer_profile_details.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }

%>