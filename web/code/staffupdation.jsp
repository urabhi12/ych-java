<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	Date d = new Date();
String y = d.toGMTString();
ConnectionManager cm = null;

try {
	String id = request.getParameter("id");
	String cno = request.getParameter("cno");
	String uname = request.getParameter("uname");
	String email = request.getParameter("email");

	String acrole = request.getParameter("acrole");

	cm = new ConnectionManager();
	Connection conn = cm.getCon();
	PreparedStatement ps = conn.prepareStatement("update reg set cno=?,name=?,email=?,type=?,mdate=? where tid=?");
	ps.setObject(1, cno);
	ps.setObject(2, uname);
	ps.setObject(3, email);
	ps.setObject(4, acrole);
	ps.setObject(5, y);
	ps.setObject(6, id);

	int n = ps.executeUpdate();
	if (n == 1) {
		out.println(
		"<script>alert('Updation is Successfull');window.location.href='../staff_profile_details.jsp'</script>");
	} else {
		out.println(
		"<script>alert('Updation is not Successfull');window.location.href='../staff_profile_details.jsp'</script>");
	}
} catch (Exception e) {
	out.println(e);
} finally {
	if(cm!=null) {
		cm.close();
	}
}
%>