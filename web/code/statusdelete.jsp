<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	Date d = new Date();
String y = d.toGMTString();
ConnectionManager cm = null;

try {
	String s = request.getParameter("m");

	cm = new ConnectionManager();
	Connection conn = cm.getCon();
	PreparedStatement ps = conn.prepareStatement("delete from status where status=?");
	ps.setObject(1, s);
	int n = ps.executeUpdate();
	if (n == 1) {
		out.println("<script>alert('Status delete successfully');window.location.href='../status_view.jsp'</script>");
	} else {
		out.println("<script>alert('Status is not delete');window.location.href='../status_view.jsp'</script>");
	}
} catch (Exception e) {
	out.println(e);
} finally {
	if(cm!=null) {
		cm.close();
	}
}
%>