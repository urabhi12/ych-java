<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
	//response.sendRedirect("../index.jsp");
	out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
}
%>
<%
	Date d = new Date();
String y = d.toGMTString();
ConnectionManager cm = null;

try {
	String uid = request.getParameter("uid");
	String status = request.getParameter("st");

	//        Class.forName("com.mysql.jdbc.Driver");
	//        Connection con = DriverManager.getConnection(host, username, password);
	cm = new ConnectionManager();
	Connection conn = cm.getCon();
	PreparedStatement ps = conn.prepareStatement(
	"update reg set isblocked=?,count = 0 ,mdate=? where tid=?");
	ps.setObject(1, status);
	ps.setObject(2, y);
	ps.setObject(3, uid);

	int n = ps.executeUpdate();
	if (n == 1) {
		if (status.equals("0")) {
	out.println(
			"<script>alert('Staff Un-Blocked Successfull');window.location.href='../staff_profile_details.jsp'</script>");
		} else {
	out.println(
			"<script>alert('Staff Blocked Successfull');window.location.href='../staff_profile_details.jsp'</script>");
		}
	} else {
		out.println("<script>alert('Not Successfull');window.location.href='../staff_profile_details.jsp'</script>");
	}
} catch (Exception e) {
	out.println(e);
} finally {
	if(cm!=null) {
		cm.close();
	}
}
%>