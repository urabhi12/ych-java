<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp");
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	}
%>
<%
	Date d = new Date();
	String y = d.toGMTString();
	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	try {
		String id = request.getParameter("id");
		String cno = request.getParameter("cno");
		String uname = request.getParameter("uname");
		String email = request.getParameter("email");
		String location = request.getParameter("location");
		String cname = request.getParameter("cname");
		String ddl_cso = request.getParameter("ddl_cso");
		//String rname = request.getParameter("rname"); 
		String rname = "";

		PreparedStatement ps = conn.prepareStatement("update reg set cno=?,name=?,email=?,location=?,cname=?,repname=?,mdate=?,cso=? where id=?");

		ps.setObject(1,cno);
		ps.setObject(2,uname);
		ps.setObject(3,email);
		ps.setObject(4,location);
		ps.setObject(5,cname);
		ps.setObject(6,rname);
		ps.setObject(7,y);
		ps.setObject(8,ddl_cso);
		ps.setObject(9,id);
		int n = ps.executeUpdate();
		if (n == 1) {
			out.println(
					"<script>alert('Updation is Successfull');window.location.href='../customer_profile_details.jsp'</script>");
		} else {
			out.println(
					"<script>alert('Updation is not Successfull');window.location.href='../customer_profile_details.jsp'</script>");
		}
	} catch (Exception e) {
		out.println(e);
	} finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>