<%-- 
    Document   : request_upload_more_image
    Created on : Jan 31, 2018, 3:01:13 PM
    Author     : Mohit
--%>
<%@page import="pack1.Send_Email"%>
<%@page import="java.util.Random"%>
<%@page import="java.util.ArrayList"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@ page
	import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@ page import="org.apache.commons.fileupload.*"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Iterator"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
	String Userid = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp");
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		Userid = session.getAttribute("uname").toString();
	}
	Date d = new Date();
    String y = d.toGMTString();
    
    
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
    String datetime = sdf.format(d);
    
    
    
	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();
	try {
		String tid = request.getParameter("tid");

		//String Path = "C://Program Files//Apache Software Foundation//Tomcat 8.5//webapps//YCH//uploadproduct";
		String Path = Send_Email.getPath();
		ArrayList<String> arr = new ArrayList<String>();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);
		if (!isMultipart) {
		} else {
			FileItemFactory factory = new DiskFileItemFactory();
			ServletFileUpload upload = new ServletFileUpload(factory);
			List items = null;
			try {
				items = upload.parseRequest(request);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
			Iterator itr = items.iterator();
			while (itr.hasNext()) {
				FileItem item = (FileItem) itr.next();

				if (item.isFormField()) {

				} else {

					try {
						String itemName = item.getName();
						Random randid = new Random();
						int n = randid.nextInt(10000) + 1;
						itemName = "nreq_" + String.valueOf(n) + "_" + itemName;
						File savedFile = new File(Path + itemName);
						item.write(savedFile);
						arr.add(itemName);

						//out.println("<tr><td><b>Your file has been saved at the loaction:</b></td></tr><tr><td><b>" + config.getServletContext().getRealPath("/") + "uploadedFiles" + "\\" + itemName + "</td></tr>");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (arr.size() > 0) {

			int ecouny = 0;
			for (int i = 0; i < arr.size(); i++) {
				PreparedStatement ps2 = conn.prepareStatement("insert into request_upload values(?,?,?,?,?)");
				ps2.setObject(1, null);//sequence
				ps2.setString(2, tid);
				ps2.setString(3, arr.get(i).toString());
				ps2.setString(4, "Customer");
				ps2.setString(5, Userid);
				int n2 = ps2.executeUpdate();
				if (n2 == 1) {
					ecouny++;
					PreparedStatement ps_update_req = conn.prepareStatement(
							"update req set status='Pending' , is_send_vcc = '0', mdate = '"+y+"' where tid='" + tid + "'");
					ps_update_req.executeUpdate();
					
					
					// For Log Maintain
	                String StrSQL = "INSERT INTO request_update_log"
	                + "(req_id, Status, Updated_By, update_datetime, create_date, modify_date) "
	                + "VALUES "
	                + "('"+tid+"','New documents added by customer','"+Userid+"','"+datetime+"','"+datetime+"','"+datetime+"')";
	        
	                PreparedStatement req_log = conn.prepareStatement(StrSQL);
	                int update_log = req_log.executeUpdate();

				}
			}
			if (ecouny == arr.size()) {
				out.println("<script>alert('File Uploaded Successfull');window.location.href='../cview.jsp?m="
						+ tid + "'</script>");
			} else {
				out.println(
						"<script>alert('File Uploaded not Successfull');window.location.href='../cview.jsp?m="
								+ tid + "'</script>");
			}
		}
	} catch (Exception e) {
		out.println(e.getMessage());
	} finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>