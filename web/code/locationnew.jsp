<%@page import="java.time.LocalTime"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
    Date d = new Date();
    String y = d.toGMTString();
    ConnectionManager cm = null;

    try {
        String lname = request.getParameter("lname");
        String location = "";

        cm = new ConnectionManager();
        Connection conn = cm.getCon();
        PreparedStatement pss = conn.prepareStatement("select * from location");
        ResultSet rs = pss.executeQuery();
        while (rs.next()) {
            location = rs.getString(1);

            if (location.equals(lname)) {
                out.print("<script>alert('Location is already add');window.location.href='../location_new.jsp'</script>");
            }
        }
        PreparedStatement ps = conn.prepareStatement("insert into location values(?,?,?,?)");
        ps.setString(1, lname);
        ps.setString(2, y);
        ps.setString(3, y);
        ps.setObject(4, null);//set by sequence

        int n = ps.executeUpdate();
        if (n == 1) {
            out.println("<script>alert('Location add successfully');window.location.href='../location_view.jsp'</script>");
        } else {
            out.println("<script>alert('Location is not add');window.location.href='../location_view.jsp'</script>");
        }
    } catch (Exception e) {
        out.println(e);
    } finally {
    	if(cm!=null) {
    		cm.close();
    	}
    }
%>