<%@page import="java.text.SimpleDateFormat"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	String type = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp"); 
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		type = session.getAttribute("type").toString();
	}
	if (!type.equals("Customer") && !type.equals("Customer-Sub")) {
		out.println("<script>window.history.back();</script>");
	}
	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();
%>
<%try { %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">


<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript"
	src="//code.jquery.com/jquery-1.12.4.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript"
	src="bootstrap/js/datatable_dateformat.js"></script>
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
<style>
div.dataTables_wrapper {
	margin: 0 auto;
}

.PreClass {
	overflow-x: auto;
	white-space: pre-wrap;
	white-space: -moz-pre-wrap;
	white-space: -pre-wrap;
	white-space: -o-pre-wrap;
	word-wrap: break-word;
}
</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<jsp:include page="header_include.jsp" />

		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="customer_menu.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>Manage Request</h1>

			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Small boxes (Stat box) -->
				<form action="cust_my_request.jsp" method="post">
					<div class="row">
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<!-- small box -->
							<div>Request Type</div>
							<select class="form-control" name="reqtype">
								<option>Select</option>
								<%
									if (request.getParameter("reqtype") == null) {
								%>
								<option>New Import</option>
								<option>New Export</option>
								<option>Request of Permit</option>
								<option>Local Delivery</option>
								<%
									} else {
										if (request.getParameter("reqtype").equals("New Import")) {
								%>
								<option selected="true">New Import</option>
								<%
									} else {
								%>
								<option>New Import</option>
								<%
									}
										if (request.getParameter("reqtype").equals("New Export")) {
								%>
								<option selected="true">New Export</option>
								<%
									} else {
								%>
								<option>New Export</option>
								<%
									}
										if (request.getParameter("reqtype").equals("Request of Permit")) {
								%>
								<option selected="true">Request of Permit</option>
								<%
									} else {
								%>
								<option>Request of Permit</option>
								<%
									}
										if (request.getParameter("reqtype").equals("Local Delivery")) {
								%>
								<option selected="true">Local Delivery</option>
								<%
									} else {
								%>
								<option>Local Delivery</option>
								<%
									}
				
								}
								%>
							</select>
						</div>
						<!-- ./col -->
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<!-- small box -->
							<div>Status</div>
							<select class="form-control" name="status">
								<option>Select</option>
								<%
									try {
										PreparedStatement pst = conn.prepareStatement(
												"select * from status where status in ('Accepted','Pending','Rejected','Draft Ready','Draft Ok') order by status asc");
										ResultSet rst = pst.executeQuery();
										while (rst.next()) {
											if (request.getParameter("status") == null) {
								%>
								<option><%=rst.getString(1)%></option>
								<%
									} else {
												if (request.getParameter("status").equals(rst.getString(1))) {
								%>
								<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
								<%
									} else {
								%>
								<option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>
								<%
									}
											}
										}
									} catch (Exception e) {
										System.out.println(e);
									}
								%>
							</select>
						</div>
						<%
							if (type.equals("Customer")) {
						%>
						<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
							<%
								} else {
							%>
							<div style="display: none;"
								class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
								<%
									}
								%>
								<!-- small box -->
								<div>Sub Customers</div>
								<select class="form-control" name="sub_cust">
									<option>Select</option>
									<%
										try {
											PreparedStatement pst = conn.prepareStatement("select * from reg where cust_master_id = ? and flag = '1' order by name asc");
											pst.setObject(1,session.getAttribute("uname"));
											ResultSet rst = pst.executeQuery();
											while (rst.next()) {
												if (request.getParameter("sub_cust") == null) {
									%>
									<option value="<%=rst.getString(1)%>"><%=rst.getString(2)%></option>
									<%
										} else {
													if (request.getParameter("sub_cust").equals(rst.getString(1))) {
									%>
									<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(2)%></option>
									<%
										} else {
									%>
									<option value="<%=rst.getString(1)%>"><%=rst.getString(2)%></option>
									<%
										}
												}
											}
										} catch (Exception e) {
											System.out.println(e);
										}
									%>
								</select>
							</div>

							<button type="submit" class="btn btn-primary"
								style="margin-top: 20px;">Search</button>
							<!-- ./col -->
							<!---->
							<!-- ./col -->
						</div>
				</form>
				<!-- /.row -->
				<!-- Main row -->
				<!-- /.row (main row) -->

				<div class="container-fluid" style="margin-top: 20px; height: 100%;">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<!--                            <table class="table table-responsive table-bordered table-striped table-hover">-->
							<table id="example" class="table table-responsive"
								cellspacing="0">
								<thead>
									<tr>
										<th>REQUEST ID</th>
										<th>REQUESTED ON</th>

										<%
											if (type.equals("Customer")) {
										%>
										<th>BY</th>
										<%
											} else {
										%>

										<%
											}
										%>

										<!-- <th>REQUEST TYPE</th> -->
										<th>CUSTOMER REF NO</th>
										<th>SHPT Date</th>
										<th>STATUS</th>
										<th>REMARKS</th>
										<!--  <th>HANDLING BY</th> -->


										<th></th>
									</tr>
								</thead>
								<tbody>

									<%
										String reqtype = request.getParameter("reqtype");
										String status = request.getParameter("status");
										String cust_type = "Select";
										String requested_on_time = "";
									if (type.equals("Customer")) {
										cust_type = request.getParameter("sub_cust");
									} else {
										cust_type = request.getParameter("sub_cust");
									}
									//out.println("<script>alert('"+customer+"');</script>");
									//                                    String where1 = " and reqtype='" + reqtype + "' ";
									//                                    String where2 = " and status='" + status + "' ";
									String SQL = "";

									if (status == null && reqtype == null && cust_type == null) {
										//out.println("<script>alert(nl'"+customer+"');</script>");
										if (type.equals("Customer")) {
											SQL = "select * from req where uid_ in (SELECT id FROM reg where id='" + session.getAttribute("uname")
											+ "' or cust_master_id = '" + session.getAttribute("uname") + "') ORDER BY tid DESC";
										} else {
											SQL = "select * from req where uid_='" + session.getAttribute("uname") + "' ORDER BY tid DESC";
										}

									} else {
										String Where = "";
										if (!status.equals("Select")) {
											if (status.equals("Pending") || status.equals("Rejected")) {
										Where = Where + " and status='" + status + "' ";
											} else {

										Where = Where + " and status not in('Pending','Rejected')";
											}

										}
										if (!reqtype.equals("Select")) {
											Where = Where + " and reqtype='" + reqtype + "'";
										}

										if (!cust_type.equals("Select")) {

											Where = Where + " and uid_='" + cust_type + "'";
										}
										// SQL = "select * from req where uid_='" + session.getAttribute("uname") + "'" + Where + "  ORDER BY tid DESC";
										if (type.equals("Customer")) {
											SQL = "select * from req where uid_ in (SELECT id FROM reg where id='" + session.getAttribute("uname")
											+ "' or cust_master_id = '" + session.getAttribute("uname") + "') " + Where + "  ORDER BY cdate DESC";
										} else {
											SQL = "select * from req where uid_ ='" + session.getAttribute("uname") + "' " + Where
											+ "  ORDER BY cdate DESC";
										}
									}
									String dddd = "";
									String st = "";

									String expected_com = "";
									String requested_on = "";
									try {
										//                                        Class.forName("com.mysql.jdbc.Driver");
										//                                        Connection con = DriverManager.getConnection(host, username, password);
										PreparedStatement ps = conn.prepareStatement(SQL);
										ResultSet rs = ps.executeQuery();
										int cnt = 1;
										while (rs.next()) {
											expected_com = "";
											requested_on = "";
											if (rs.getString(34) != null && !rs.getString(34).equals("")) {
										dddd = rs.getString(34);
										Date d = new SimpleDateFormat("dd/MM/yyyy").parse(dddd);

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
										expected_com = sdf.format(d);
											}
											if (rs.getString(5) != null && !rs.getString(5).equals("")) {
										Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy").parse(rs.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

										requested_on = sdf.format(temp_requested_Date);
											}
											if (rs.getString(5) != null && !rs.getString(5).equals("")) {
										Date temp_requested_Date_time = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

										requested_on_time = sdf.format(temp_requested_Date_time);
											}

											st = rs.getString(16);
							
									%>
									<tr>
										<td><%=rs.getString(1)%></td>
										<td><%=requested_on + ", " + requested_on_time%></td>

										<%
											if (type.equals("Customer")) {
										%>
										<td>
											<%
												String name = "";
											PreparedStatement ps_name = conn.prepareStatement("SELECT id, name FROM reg WHERE id =? and flag = '1'");
											ps_name.setObject(1, rs.getString(2));
											ResultSet rs_name = ps_name.executeQuery();
											while (rs_name.next()) {
												name = rs_name.getString(2);
											}
											%> <%=name%></td>
										<%
											} else {
										%>

										<%
											}
										%>

										<td>
											<%-- <% String reType="";
                                            if(rs.getString(17).equals("New Export")) {
                                                 reType = "Export ("+rs.getString(7)+")";
                                            }else if(rs.getString(17).equals("New Import")) {

                                                 reType = "Import ("+rs.getString(3)+")";
                                            }else{
                                                reType=rs.getString(17);
                                            }
                                        %>
                                        <%=reType%> --%> <%=rs.getString(38)%>



										</td>
										<td><%=rs.getString(4)%></td>
										<td><%=st%></td>
										<td><pre class="PreClass"><%=rs.getString(36)%></pre></td>
										<%-- <td><%=rs.getString(19)%></td> --%>


										<td><a href="cview.jsp?m=<%=rs.getString(25)%>"><img
												src="img/view.png" style="width: 24px; height: 24px;"
												title="View" /></a> <%
 	String editurl = "";
 			if (rs.getString(17).equals("New Import")) {
 				editurl = "cust_new_request.jsp?id=" + rs.getString(25);

 			} else if (rs.getString(17).equals("New Export")) {
 				editurl = "cust_export_request.jsp?id=" + rs.getString(25);

 			} else if (rs.getString(17).equals("Request of Permit")) {
 				editurl = "cust_request_of_permit.jsp?id=" + rs.getString(25);

 			} else if (rs.getString(17).equals("Local Delivery")) {
 				editurl = "local_delivery.jsp?id=" + rs.getString(25);

 			} else {
 				editurl = "";
 			}
 %> <a href="<%=editurl%>"><img src="img/edit.png"
												style="width: 24px; height: 24px;" title="Edit" /></a></td>
									</tr>
									<%
										dddd = "";
												cnt++;
											}
										} catch (Exception e) {
											System.out.println(e);
										}
									%>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->

	</div>
	<!-- ./wrapper -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script
		src="https://cdn.datatables.net/plug-ins/1.10.15/sorting/datetime-moment.js"></script>
	<script type="text/javascript" class="init">
		$(document).ready(function() {
			$.fn.dataTable.moment('DD/MM/YYYY, hh:mm a');
			$('#example').DataTable({
				"scrollX" : false,
				order : [],
				columnDefs : [ {
					orderable : false,
					targets : [ 0 ]
				} ]
			});

		});
	</script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<!-- AdminLTE App -->
	<script src="ltmcss/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="ltmcss/js/demo.js"></script>




</body>
</html>
<%
} finally {
	if(cm != null) {
		cm.close();
	}
}
%>