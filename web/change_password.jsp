<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%
	String type = "", user_id = "", log_User_id = "", log_token = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp"); 
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		type = session.getAttribute("type").toString();
		user_id = session.getAttribute("user_id").toString();
		log_User_id = session.getAttribute("user_id").toString();
		log_token = session.getAttribute("logtoken").toString();
	}
	if (!type.equals("Customer") && !type.equals("Customer-Sub")) {
		out.println("<script>window.history.back();</script>");
	}
	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	PreparedStatement ps_login_acc_chk = conn
			.prepareStatement("select * from login_access where user_id='" + log_User_id + "'");
	ResultSet rs_login_acc_chk = ps_login_acc_chk.executeQuery();

	if (rs_login_acc_chk.next()) {
		if (!log_token.equals(rs_login_acc_chk.getString(3))) {
			out.println(
					"<script>alert('Logged in by other System...');window.location.href='index.jsp';</script>");
		}
	} else {
		out.println("<script>alert('Logged in by other System...');window.location.href='index.jsp';</script>");
	}
%>


<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%try { %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type"
	content="text/html; charset=windows-1252">
<title>YCH | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">


<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript"
	src="//code.jquery.com/jquery-1.12.4.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" language="javascript"
	src="bootstrap/js/datatable_dateformat.js"></script>
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />
		<!-- Left side column. contains the logo and sidebar -->
		<jsp:include page="customer_menu.jsp" />

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
			<h1>Change Password</h1>
			</section>

			<!-- Main content -->
			<section class="content">
			<div class="panel panel-default">

				<div class="panel-body">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<p>
							<%
								if (request.getParameter("r") != null) {
									if (request.getParameter("r").equals("0")) {
										out.println(
												"<div class='alert alert-success'><strong>Success!</strong> Password Change Successfull.</div>");
									} else if (request.getParameter("r").equals("1")) {
										out.println(
												"<div class='alert alert-danger'><strong>Error!</strong>  Old Password Not matched.</div>");
									} else if (request.getParameter("r").equals("2")) {
										out.println("<div class='alert alert-danger'><strong>Error!</strong> Password Not matched..</div>");
									} else {
										out.println("<div class='alert alert-danger'><strong>Error!</strong> Password Not Changed..</div>");
									}
								}
							%>
						</p>
					</div>

					<form action="code/change_password_process.jsp" method="post"
						onsubmit="return validate()">
						<div class="row">
							<!-- ./col -->
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<!-- small box -->
								<div>Old Password</div>
								<input type="text" class="form-control"
									placeholder="Please Enter Old Password" name="txt_old_password"
									required>
							</div>
							<br>
						</div>

						<div class="row">
							<!-- ./col -->
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<!-- small box -->
								<div>New Password</div>
								<input type="text" class="form-control"
									placeholder="Please Enter New Password" name="txt_new_password"
									id="txt_new_password" required pattern="(.){8,20}"
									title="Password should be at least 8 characters.">
							</div>
							<br>
						</div>

						<div class="row">

							<!-- ./col -->
							<div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
								<!-- small box -->
								<div>Confirm Password</div>
								<input type="text" class="form-control"
									placeholder="Please Enter Confirm Password"
									name="txt_Confirm_password" id="txt_Confirm_password" required
									required pattern="(.){8,20}"
									title="Password should be at least 8 characters.">
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<br> <input type="hidden" name="userid"
									value="<%=user_id%>"> <input type="hidden"
									name="usertype" value="<%=type%>">
								<button type="submit" class="btn btn-primary">Submit</button>

							</div>
						</div>



					</form>
				</div>
			</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
		<div class="pull-right hidden-xs"></div>
		<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
		</strong> All rights reserved. </footer>

	</div>
	<!-- ./wrapper -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script
		src="https://cdn.datatables.net/plug-ins/1.10.15/sorting/datetime-moment.js"></script>
	<script type="text/javascript" class="init">
		$(document).ready(function() {
			$('#example').DataTable({});
		});
		function validate() {
			var newpassword = $('#txt_new_password').val();
			var confirmpassword = $('#txt_Confirm_password').val();

			if (newpassword == confirmpassword) {
				return true;
			} else {
				alert("Password and Confirm Password Not Matched.");
				return false;
			}

		}
	</script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<!-- AdminLTE App -->
	<script src="ltmcss/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="ltmcss/js/demo.js"></script>




</body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>