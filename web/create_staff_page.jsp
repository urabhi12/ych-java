
<!DOCTYPE html>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>YCH | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <jsp:include page="header_include.jsp" />
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar"> 
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar"> 
          
          <!-- /.search form --> 
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Menus</li>
            <li class="active treeview"> </li>
            <li> <a href="staff_request_detail.jsp"> <i class="fa fa-reply" aria-hidden="true"></i> <span>Request</span> </a> </li>
            <li> <a href="staff_permit_dec_list.jsp"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Permit Declaration</span> </a> </li>
            <li> <a href="staff_pickup_date_list.jsp"> <i class="fa fa-gift" aria-hidden="true"></i> <span>Pickup</span> </a> </li>
            <li> <a href="staff_account.jsp"> <i class="fa fa-user" aria-hidden="true"></i> <span>Account</span> </a> </li>
            <li style="position:absolute; bottom:0px; width:100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);"><a href="index.jsp"> <i class="fa fa-power-off" aria-hidden="true"></i> <span>Sign out</span> </a> </li>
          </ul>
        </section>
        <!-- /.sidebar --> 
      </aside>
      
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper"> 
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1> Manage Request 
            <!--<small>Control panel</small>--> 
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
        
        <!-- Main content -->
        <section class="content"> 
          <!-- Small boxes (Stat box) -->
          <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"> 
              <!-- small box -->
              <div> Request Type</div>
              <select class="form-control">
                <option>All</option>
                <option>ID</option>
                <option>All</option>
              </select>
            </div>
            <!-- ./col -->
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"> 
              <!-- small box -->
              <div> Status</div>
              <select class="form-control">
                <option>All</option>
                <option>ID</option>
              </select>
            </div>
            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"> 
              <!-- small box -->
              <div>Search</div>
              <input class="form-control" placeholder="Search" type="text">
            </div>
            
            <!-- ./col --> 
            <!----><!-- ./col --> 
          </div>
          <button type="button" class="btn btn-primary" style="margin-top:20px;">Search </button>
          <!-- /.row --> 
          <!-- Main row --> 
          <!-- /.row (main row) -->
          <div class="container" style="margin-top:50px; width:100%;">
            <table class="table table-responsive table-bordered table-striped table-hover">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>REQUESTED ON </th>
                  <th>EXPECTED COMPLETION</th>
                  <th>REQUEST TYPE</th>
                  <th>STATUS</th>
                  <th>HANDLING BY</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>YFT001</td>
                  <td>02-Oct-201<br>
                    01:32PM</td>
                  <td>20-Oct-2017</td>
                  <td>Import, Sea</td>
                  <td>Pending</td>
                  <td>-</td>
                  <td><button class="btn btn-warning">View</button></td>
                </tr>
                <tr>
                  <td>YFT002</td>
                  <td>02-Oct-201<br>
                    01:32PM</td>
                  <td>20-Oct-2017</td>
                  <td>Import, Sea</td>
                  <td>Accepted</td>
                  <td>Mary Tank</td>
                  <td><button class="btn btn-warning">View</button></td>
                </tr>
                <tr>
                  <td>YFT003</td>
                  <td>02-Oct-201<br>
                    01:32PM</td>
                  <td>20-Oct-2017</td>
                  <td>Import, Sea</td>
                  <td>Pending</td>
                  <td>-</td>
                  <td><button class="btn btn-warning">View</button></td>
                </tr>
              </tbody>
            </table>
          </div>
        </section>
        <!-- /.content --> 
      </div>
      <!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs"> </div>
        <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>
      
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar --> 
      
    </div>
    <!-- ./wrapper --> 

    <!-- jQuery 2.1.4 --> 
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> 
    <!-- jQuery UI 1.11.4 --> 
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script> 
    <!-- Bootstrap 3.3.5 --> 
    <script src="bootstrap/js/bootstrap.min.js"></script> 
    <!-- Morris.js charts --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
    <script src="plugins/morris/morris.min.js"></script> 
    <!-- Sparkline --> 
    <script src="plugins/sparkline/jquery.sparkline.min.js"></script> 
    <!-- jvectormap --> 
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> 
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script> 
    <!-- jQuery Knob Chart --> 
    <script src="plugins/knob/jquery.knob.js"></script> 
    <!-- daterangepicker --> 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
    <script src="plugins/daterangepicker/daterangepicker.js"></script> 
    <!-- datepicker --> 
    <script src="plugins/datepicker/bootstrap-datepicker.js"></script> 
    <!-- Bootstrap WYSIHTML5 --> 
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script> 
    <!-- Slimscroll --> 
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script> 
    <!-- FastClick --> 
    <script src="plugins/fastclick/fastclick.min.js"></script> 
    <!-- AdminLTE App --> 
    <script src="ltmcss/js/app.min.js"></script> 
    <!-- AdminLTE dashboard demo (This is only for demo purposes) --> 
    <script src="ltmcss/js/pages/dashboard.js"></script> 
    <!-- AdminLTE for demo purposes --> 
    <script src="ltmcss/js/demo.js"></script>
</body>
</html>
