<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%-- <%@page import="javafx.util.converter.LocalDateStringConverter"%> --%>
<%@page import="java.time.LocalDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>


<%
	String type = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp");
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		type = session.getAttribute("type").toString();
	}
	if (!type.equals("Staff")) {
		out.println("<script>window.history.back();</script>");
	}

	SimpleDateFormat dateformat_sdf = new SimpleDateFormat("yyyy-MM-dd");
	Date current_date = new Date();
	LocalDate addmonth_date = LocalDate.now().plusMonths(1);

	String s = request.getParameter("m");
	String request_id = "";
	String cmpname = "";
	String reqon = "";
	String expcmp = "";
	String reqtype = "";
	String status = "";
	String pickup_dateto = "";
	String pickup_datefrom = "";
	String permit_dateto = "";
	String permit_datefrom = "";
	String additional_comment = "";
	String Remarks = "";
	String TX3ID = "";
	String TOTAL_NO_OF_PALLETS = "";
	String TOTAL_M3 = "";
	String Package_Dimension = "";
	String dateofarival = "";
	String vcc_instru = "";
	//DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");  
	Date d = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	String pdateto = sdf.format(d);

	//dateformat_sdf.format(current_date);     
	// pickup_datefrom = formatter.format(addmonth_date);

	//permit_dateto = pdateto;//dateformat_sdf.format(current_date);    
	//  permit_datefrom = formatter.format(addmonth_date);

	ConnectionManager cm = null;
	try {
		//        Class.forName("com.mysql.jdbc.Driver");
		//        Connection con = DriverManager.getConnection(host, username, password);
		cm = new ConnectionManager();
		Connection conn = cm.getCon();
		PreparedStatement ps = conn.prepareStatement("select * from req where tid='" + s + "'");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			request_id = rs.getString(1);
			cmpname = rs.getString(18);
			if (rs.getString(5)!=null && !rs.getString(5).equals("")) {
				Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs.getString(5));

				SimpleDateFormat sdf_req = new SimpleDateFormat("dd/MM/yyyy HH:mm a");

				reqon = sdf_req.format(temp_requested_Date);
			}

			additional_comment = rs.getString(35);
			dateofarival = rs.getString(4);
			Remarks = rs.getString(36);
			TX3ID = rs.getString(37);
			if (rs.getString(34)!=null && rs.getString(34).equals("")) {
				//expcmp = dateformat_sdf.format(current_date); 
			} else {
				expcmp = rs.getString(34);
			}

			reqtype = rs.getString(17);
			status = rs.getString(16);
			TOTAL_NO_OF_PALLETS = rs.getString(30);
			TOTAL_M3 = rs.getString(31);
			Package_Dimension = rs.getString(32);

			if (rs.getString(23)!=null && rs.getString(23).equals("")) {
				//pickup_dateto = dateformat_sdf.format(current_date); 
			} else {
				pickup_dateto = rs.getString(23);
			}
			if (rs.getString(24)!=null && rs.getString(24).equals("")) {
				//pickup_datefrom = addmonth_date.toString(); 
			} else {
				pickup_datefrom = rs.getString(24);
			}

			if (rs.getString(24)!=null && rs.getString(21).equals("")) {
				//permit_dateto = dateformat_sdf.format(current_date);
			} else {
				permit_dateto = rs.getString(21);
			}
			if (rs.getString(24)!=null && rs.getString(22).equals("")) {
				// permit_datefrom = addmonth_date.toString();  
			} else {
				permit_datefrom = rs.getString(22);

			}
			vcc_instru = rs.getString(40);

		}
	} catch (Exception e) {
		System.out.println(e);
	} finally {
		if(cm != null) {
			cm.close();
		}
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.min.css">

<!-- Font Awesome -->
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->

</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Menus</li>
					<li class="active treeview"></li>
					<li><a href="staff_request_detail.jsp"> <i
							class="fa fa-reply" aria-hidden="true"></i> <span>Request</span>
					</a></li>
					<li><a href="staff_permit_dec_list.jsp"> <i
							class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Permit
								Declaration</span>
					</a></li>
					<li><a href="staff_pickup_date_list.jsp"> <i
							class="fa fa-gift" aria-hidden="true"></i> <span>Pickup</span>
					</a></li>
					<li
						style="position: absolute; bottom: 0px; width: 100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);">
						<a href="logout.jsp"> <i class="fa fa-power-off"
							aria-hidden="true"></i> <span>Sign out</span>
					</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Update Customer Request
					<!--<small>Control panel</small>-->
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Small boxes (Stat box) -->

				<!-- Main row -->
				<!-- /.row (main row) -->
				<div class="container" style="margin-top: 10px; width: 100%;">
					<div class="panel panel-default">
						<div class="panel-body">
							<form method="post" action="code/staffreqdel.jsp" name="forget">
								<div class="panel-heading">
									<u> </u>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%;">Request
										ID :</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" type="text" name="reqid"
										value="<%=request_id%>" readonly="readonly"> <input
										class="form-control" type="text" name="id" value="<%=s%>"
										readonly="readonly" style="display: none;"> <input
										class="form-control" type="text" name="reqtype"
										value="<%=reqtype%>" readonly="readonly"
										style="display: none;">
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Requested
										On :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="reqon"
											value="<%=reqon%>" readonly="readonly">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Remark :</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<textarea class="form-control" type="text" name="remark"
											id="remark"><%=Remarks%></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">TX3ID:</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" type="text" name="tx3id"
										value="<%=TX3ID%>">
								</div>

								<%
									if (reqtype.equals("New Import")) {
								%>

								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Date of
										Arrival :</div>
								</div>
								<%
									} else if (reqtype.equals("New Export")) {
								%>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Expected
										Date of Departure:</div>
								</div>
								<%
									} else if (reqtype.equals("Request of Permit")) {
								%>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Cargo
										Collection Date From Bonded/ Licence Warehouse:</div>
								</div>
								<%
									} else if (reqtype.equals("Local Delivery")) {
								%>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Date of
										Delivery :</div>
								</div>

								<%
									}
								%>
								<div class="col-md-4">
									<input class="form-control" type="text" name="dateofarival"
										id="dateofarival" value="<%=dateofarival%>">
								</div>



								<div class="clearfix"></div>

								<%
									if (reqtype.equals("Local Delivery")) {
								%>


								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Total no.
										of pallets / packages :</div>
								</div>


								<div class="col-md-4">
									<input class="form-control" type="text" name="ttl_nf_pallets"
										value="<%=TOTAL_NO_OF_PALLETS%>">
								</div>


								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Total M3 :
									</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" type="text" name="ttl_m"
										value="<%=TOTAL_M3%>">
								</div>
								<div class="clearfix"></div>


								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Package
										Dimension :</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" type="text" name="pckg_dimension"
										value="<%=Package_Dimension%>">
								</div>
								<%
									}
								%>


								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Expected
										Completion :</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" type="text" name="expcmp"
										id="expcmp" placeholder="dd/mm/yyyy" value="<%=expcmp%>">
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Company
										Name :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="cmpname"
											value="<%=cmpname%>">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Permit
										Date To :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="pdateto"
											id="pdateto" placeholder="dd/mm/yyyy"
											value="<%=permit_dateto%>">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Permit
										Date From :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="pdatefrom"
											id="pdatefrom" placeholder="dd/mm/yyyy"
											value="<%=permit_datefrom%>">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Pickup
										Date To :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="pudateto"
											id="pudateto" placeholder="dd/mm/yyyy"
											value="<%=pickup_dateto%>">
									</div>
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Pickup
										Date From :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="pudatefrom"
											id="pudatefrom" placeholder="dd/mm/yyyy"
											value="<%=pickup_datefrom%>">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Status :</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<textarea class="form-control" type="text"
											name="additional_comment" id="additional_comment"><%=additional_comment%></textarea>
									</div>
								</div>

								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Instruction
										to VCC :</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<textarea class="form-control" type="text" name="vcc_instru"><%=vcc_instru%></textarea>
									</div>
								</div>




								<div class="clearfix"></div>

								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Handling
										By :</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="handby"
											value="<%=session.getAttribute("uname")%>"
											readonly="readonly">
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-12" align="center">
									<br>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
                     immediately after the control sidebar -->

	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<!-- AdminLTE App -->
	<script src="ltmcss/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="ltmcss/js/demo.js"></script>

	<script src="https://cdn.ckeditor.com/4.10.0/standard-all/ckeditor.js"></script>


	<script>
		$(document).ready(function() {

			$('#expcmp').datepicker({
				format : "dd/mm/yyyy"
			});

			$('#pdateto').datepicker({
				format : "dd/mm/yyyy"
			});
			$('#pdatefrom').datepicker({
				format : "dd/mm/yyyy"
			});

			$('#pudateto').datepicker({
				format : "dd/mm/yyyy"
			});
			$('#pudatefrom').datepicker({
				format : "dd/mm/yyyy"
			});

			$('#dateofarival').datepicker({
				format : "dd/mm/yyyy"
			});

		});
	</script>
	<script>
		CKEDITOR.replace('additional_comment', {
			fullPage : false,
			extraPlugins : 'docprops',
			// Disable content filtering because if you use full page mode, you probably
			// want to  freely enter any HTML content in source mode without any limitations.
			allowedContent : true,
			height : 100
		});
		CKEDITOR.replace('remark', {
			fullPage : false,
			extraPlugins : 'docprops',
			// Disable content filtering because if you use full page mode, you probably
			// want to  freely enter any HTML content in source mode without any limitations.
			allowedContent : true,
			height : 100
		});
	</script>
</body>
</html>
