<%@page import="pack1.ConnectionManager"%>
<%
     String type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
    }
    if(!type.equals("Admin")){
        out.println("<script>window.history.back();</script>");
    } 
%>
<%@page import="java.sql.*"%>
<%
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
    String s = request.getParameter("m");
    String name="";
    String cname="";
    String rname="";
    String cno="";
    String location="";
    String email="";
    String pwd="";
    String Cso="";
        
    try {     
        
        
        PreparedStatement ps = conn.prepareStatement("select * from reg where id=?");
        ps.setObject(1,s);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            name = rs.getString(2);
            cname = rs.getString(6);
            rname = rs.getString(7);
            cno = rs.getString(3);
            location = rs.getString(12);
            email = rs.getString(4);
            pwd = rs.getString(5);
            Cso = rs.getString(18);
            
        }
    } catch (Exception e) {
        System.out.println(e);
    }
%>
<%try { %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
     

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        
        <script>
            function validateForm() 
            {
                var x = document.forms["forget"]["pwd"].value;
                var y = document.forms["forget"]["pwd1"].value;
                if (x!=y) {
                    alert("Password not match..!");
                    return false;
                }
            }
        </script>
        
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar"> 
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar"> 

                    <!-- /.search form --> 
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menus</li>
                        <li class="active treeview"> </li>
                        <li> <a href="staff_profile_details.jsp"> <i class="fa fa-user" aria-hidden="true"></i> <span>Staff</span> </a> </li>
                        <li> <a href="customer_profile_details.jsp"> <i class="fa fa-users" aria-hidden="true"></i> <span>Customer</span> </a> </li>
                        <li> <a href="location_view.jsp"> <i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Location</span> </a> </li>
                        <li> <a href="status_view.jsp"> <i class="fa fa-star" aria-hidden="true"></i> <span>Status</span> </a> </li>
                        <li> <a href="pie_chart.jsp"> <i class="fa fa-pie-chart" aria-hidden="true"></i><span>Pie Chart</span> </a> </li>
                        <li> <a href="bar_chart.jsp"> <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span>Bar Chart</span> </a> </li>
                        <li style="position:absolute; bottom:0px; width:100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);"> <a href="index.jsp"> <i class="fa fa-power-off" aria-hidden="true"></i> <span>Sign out</span> </a> </li>
                    </ul>
                </section>
                <!-- /.sidebar --> 
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Customer Details Updation 
                        <!--<small>Control panel</small>--> 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    <!-- Small boxes (Stat box) --> 

                    <!-- Main row --> 
                    <!-- /.row (main row) -->
                    <div class="container" style="margin-top:10px; width:100%;">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form method="post" action="code/customerupdation.jsp" name="forget" onsubmit="return validateForm()">
                                    <div class="panel-heading"> <u> </u> </div>
                                    <br>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%;"> User Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" placeholder="User Name" type="text" name="id" value="<%=s%>" readonly="readonly">
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Name  : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Name" type="text" name="uname" value="<%=name%>" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Contact No : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" placeholder="Contact No" type="text" name="cno" value="<%=cno%>" required="required">
                                    </div>


                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Company Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Company Name" type="text" name="cname" value="<%=cname%>" required="required">
                                        </div>
                                    </div>

                                   <%--  <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Representative Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Representative Name" type="text" name="rname" value="<%=rname%>">
                                        </div>
                                    </div> --%>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Email : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Email" type="email" name="email" value="<%=email%>" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Location : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control" name="location">
                                                
                                                 <% 
                                                    try {
                                                         
                                                        PreparedStatement ps = conn.prepareStatement("select * from location order by location asc");
                                                        ResultSet rst = ps.executeQuery();
                                                        while (rst.next()) {                                                 
                                                            if(location.equals(rst.getString(1))){ %>
                                                                <option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
                                                            <%} else {%>
                                                            <option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>   
                                                            <%}  
                                                        }
                                                    } catch (Exception e) {
                                                        System.out.println(e);
                                                    }%>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> CSO : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <select class="form-control" name="ddl_cso">
                                                <option value="">--Select--</option>
                                                <%                                                    
                                                    try {
                                                        PreparedStatement ps_cso = conn.prepareStatement("select * from reg where type = 'Staff' and flag = '1' order by name asc");
                                                        ResultSet rs_cso = ps_cso.executeQuery();
                                                        while (rs_cso.next()) {
                                                        	String isSelected = "";
                                                        	if(Cso!=null && Cso.equals(rs_cso.getString(1))){
                                                        		isSelected = "selected='selected'";
                                                        	}
                                                %>
                                                <option value="<%=rs_cso.getString(1)%>"  <%=isSelected %>><%=rs_cso.getString(2)%></option>
                                                <%}
                                                    } catch (Exception e) {
                                                        System.out.println(e);
                                                    }%>
                                            </select>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-md-12" align="center"> <br>
                                        <button type="submit" class="btn btn-primary">Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar --> 

        </div>
        <!-- ./wrapper --> 

        <!-- jQuery 2.1.4 --> 
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> 
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
        
        <!-- daterangepicker --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
         
        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 
        
        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>
    </body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>
