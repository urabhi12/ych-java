<%@page import="java.text.SimpleDateFormat"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%
	String type = "";
if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
	//response.sendRedirect("../index.jsp");
	out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
} else {
	type = session.getAttribute("type").toString();
}
if (!type.equals("Staff")) {
	out.println("<script>window.history.back();</script>");
}
ConnectionManager cm = new ConnectionManager();
Connection conn = cm.getCon();
%>
<%try { %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">


<link rel="stylesheet" type="text/css"
	href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<script type="text/javascript" language="javascript"
	src="//code.jquery.com/jquery-1.12.4.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
	
</script>
<script type="text/javascript" language="javascript"
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<!--        <script type="text/javascript" language="javascript" src="bootstrap/js/datatable_dateformat.js"></script>-->


<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
<style>
div.dataTables_wrapper {
	margin: 0 auto;
}

.modal-dialog {
	width: 200px;
}
</style>




</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Menus</li>
					<li class="active treeview"></li>
					<li><a href="staff_request_detail.jsp"> <i
							class="fa fa-reply" aria-hidden="true"></i> <span>Request</span>
					</a></li>
					<li><a href="staff_permit_dec_list.jsp"> <i
							class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Permit
								Declaration</span>
					</a></li>
					<li><a href="staff_pickup_date_list.jsp"> <i
							class="fa fa-gift" aria-hidden="true"></i> <span>Pickup</span>
					</a></li>
					<li
						style="position: absolute; bottom: 0px; width: 100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);">
						<a href="logout.jsp"> <i class="fa fa-power-off"
							aria-hidden="true"></i> <span>Sign out</span>
					</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					Request
					<!--<small>Control panel</small>-->
				</h1>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Small boxes (Stat box) -->
				<form action="staff_request_detail.jsp" method="post">
					<div class="row">
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
							<!-- small box -->
							<div>Request Type</div>
							<select class="form-control" name="reqtype">
								<option>Select</option>
								<%
									if (request.getParameter("reqtype") == null) {
								%>
								<option value="IFS">IFS</option>
								<option value="ILS">ILS</option>
								<option value="IFR">IFR</option>
								<option value="ILR">ILR</option>

								<option value="ILA">ILA</option>

								<option value="EFS">EFS</option>

								<option value="ELS">ELS</option>

								<option value="EFR">EFR</option>

								<option value="ELR">ELR</option>

								<option value="ELA">ELA</option>

								<option value="REQPER">REQPER</option>

								<option value="LLR">LLR</option>


								<%
									} else {
								if (request.getParameter("reqtype").equals("IFS")) {
								%>
								<option selected="true" value="IFS">IFS</option>
								<%
									} else {
								%>
								<option value="IFS">IFS</option>
								<%
									}
								if (request.getParameter("reqtype").equals("ILS")) {
								%>
								<option selected="true" value="ILS">ILS</option>
								<%
									} else {
								%>
								<option value="ILS">ILS</option>
								<%
									}
								if (request.getParameter("reqtype").equals("IFR")) {
								%>
								<option selected="true" value="IFR">IFR</option>
								<%
									} else {
								%>
								<option value="IFR">IFR</option>
								<%
									}
								if (request.getParameter("reqtype").equals("ILR")) {
								%>
								<option selected="true" value="ILR">ILR</option>
								<%
									} else {
								%>
								<option value="ILR">ILR</option>
								<%
									}

								if (request.getParameter("reqtype").equals("ILA")) {
								%>
								<option selected="true" value="ILA">ILA</option>
								<%
									} else {
								%>
								<option value="ILA">ILA</option>
								<%
									}

								if (request.getParameter("reqtype").equals("EFS")) {
								%>
								<option selected="true" value="EFS">EFS</option>
								<%
									} else {
								%>
								<option value="EFS">EFS</option>
								<%
									}

								if (request.getParameter("reqtype").equals("ELS")) {
								%>
								<option selected="true" value="ELS">ELS</option>
								<%
									} else {
								%>
								<option value="ELS">ELS</option>
								<%
									}

								if (request.getParameter("reqtype").equals("EFR")) {
								%>
								<option selected="true" value="EFR">EFR</option>
								<%
									} else {
								%>
								<option value="EFR">EFR</option>
								<%
									}

								if (request.getParameter("reqtype").equals("ELR")) {
								%>
								<option selected="true" value="ELR">ELR</option>
								<%
									} else {
								%>
								<option value="ELR">ELR</option>
								<%
									}

								if (request.getParameter("reqtype").equals("ELA")) {
								%>
								<option selected="true" value="ELA">ELA</option>
								<%
									} else {
								%>
								<option value="ELA">ELA</option>
								<%
									}

								if (request.getParameter("reqtype").equals("REQPER")) {
								%>
								<option selected="true" value="REQPER">REQPER</option>
								<%
									} else {
								%>
								<option value="REQPER">REQPER</option>
								<%
									}

								if (request.getParameter("reqtype").equals("LLR")) {
								%>
								<option selected="true" value="LLR">LLR</option>
								<%
									} else {
								%>
								<option value="LLR">LLR</option>
								<%
									}

								}
								%>
							</select>
						</div>
						<!-- ./col -->
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
							<!-- small box -->
							<div>Request Status</div>
							<select class="form-control" name="status">
								<option>Select</option>
								<%
									try {
									PreparedStatement pst = conn.prepareStatement("select * from status order by status asc");
									ResultSet rst = pst.executeQuery();
									while (rst.next()) {
										if (request.getParameter("status") == null) {
								%>
								<option><%=rst.getString(1)%></option>
								<%
									} else {
								if (request.getParameter("status").equals(rst.getString(1))) {
								%>
								<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
								<%
									} else {
								%>
								<option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>
								<%
									}
								}
								}
									rst.close();
									pst.close();
								} catch (Exception e) {
								System.out.println(e);
								}
								%>
							</select>
						</div>
						<div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
							<div>ETA</div>
							<%
								String ETA_val = "";
							if (request.getParameter("eta_filter") != null) {
								if (!request.getParameter("eta_filter").equals("")) {
									ETA_val = request.getParameter("eta_filter").toString();
								}

							}
							%>
							<input type="text" name="eta_filter" class="form-control"
								value="<%=ETA_val%>" />
						</div>
						<button type="submit" class="btn btn-primary"
							style="margin-top: 20px;">Search</button>
						<!-- ./col -->
						<!---->
						<!-- ./col -->
					</div>
				</form>
				<!-- /.row -->
				<!-- Main row -->
				<!-- /.row (main row) -->

				<div class="container-fluid" style="margin-top: 20px; height: 100%;">
					<div class="row">
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<table id="example" class="table table-responsive cell-border"
								cellspacing="0" style="overflow-x: auto;">
								<thead>
									<tr>

										<th>Request ID</th>
										<th>TX3ID</th>
										<th>Requested Date & Time</th>
										<th>By</th>

										<th>ETA Date</th>
										<th>Request Status</th>
										<th></th>
										<th>Status</th>
										<th>Handling By</th>
										<th></th>
										<th></th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<%
										String reqtype = request.getParameter("reqtype");
									String status = request.getParameter("status");
									String ETA_Srch = request.getParameter("eta_filter");

									String SQLpen = "";
									String SQLacpt = "";
									String SQLrjct = "";
									// For Pending
									if (status == null && reqtype == null && ETA_Srch == null) {
										SQLpen = "select * from req where status = 'Pending' ORDER BY TO_DATE(cdate, 'DD MON YYYY HH24:MI:SS') DESC";
									} else {
										String Where = "";
										if (!status.equals("Select")) {
											Where = Where + " and status='" + status + "' ";
										}
										if (!reqtype.equals("Select")) {
											Where = Where + " and id LIKE '%" + reqtype + "%'";
										}
										if (!ETA_Srch.equals("")) {
											String[] ETA_Arr = ETA_Srch.split("-");
											Where = Where + " and TO_DATE(da,'DD/MM/YYYY') BETWEEN TO_DATE('" + ETA_Arr[0] + "','DD/MM/YYYY') AND TO_DATE('"
											+ ETA_Arr[1] + "','DD/MM/YYYY')";
										}

										SQLpen = "select * from req where 1=1 " + Where
										+ " and status = 'Pending' ORDER BY TO_DATE(cdate, 'DD MM YYYY HH24:MI:SS') DESC";
									}
									// For Accept
									if (status == null && reqtype == null && ETA_Srch == null) {
										SQLacpt = "select * from req where (status != 'Pending' and status != 'Rejected') ORDER BY TO_DATE(cdate, 'DD MON YYYY HH24:MI:SS') DESC";
									} else {
										String Where = "";
										if (!status.equals("Select")) {
											Where = Where + " and status='" + status + "' ";
										}
										if (!reqtype.equals("Select")) {
											Where = Where + " and id LIKE '%" + reqtype + "%'";
										}
										if (!ETA_Srch.equals("")) {
											String[] ETA_Arr = ETA_Srch.split("-");
											Where = Where + " and TO_DATE(da,'DD/MM/YYYY') BETWEEN TO_DATE('" + ETA_Arr[0] + "','DD/MM/YYYY') AND TO_DATE('"
											+ ETA_Arr[1] + "','DD/MM/YYYY')";
										}
										SQLacpt = "select * from req where 1=1 " + Where
										+ " and (status != 'Pending' and status != 'Rejected') ORDER BY TO_DATE(cdate, 'DD MON YYYY HH24:MI:SS') DESC";
									}
									// For Rejected
									if (status == null && reqtype == null && ETA_Srch == null) {
										SQLrjct = "select * from req where (status = 'Rejected') ORDER BY TO_DATE(cdate, 'DD MON YYYY HH24:MI:SS') DESC";
									} else {
										String Where = "";
										if (!status.equals("Select")) {
											Where = Where + " and status='" + status + "' ";
										}
										if (!reqtype.equals("Select")) {
											Where = Where + " and id LIKE '%" + reqtype + "%'";
										}
										if (!ETA_Srch.equals("")) {
											String[] ETA_Arr = ETA_Srch.split("-");
											Where = Where + " and TO_DATE(da,'DD/MM/YYYY') BETWEEN TO_DATE('" + ETA_Arr[0] + "','DD/MM/YYYY') AND TO_DATE('"
											+ ETA_Arr[1] + "','DD/MM/YYYY')";
										}
										SQLrjct = "select * from req where 1=1 " + Where
										+ " and (status = 'Rejected') ORDER BY TO_DATE(cdate, 'DD MON YYYY HH24:MI:SS') DESC";
									}
									String dddd = "";
									String expected_com = "";
									String requested_on = "";
									String requested_on_time = "";

									try {

										PreparedStatement ps_pen = conn.prepareStatement(SQLpen);
										ResultSet rs_pen = ps_pen.executeQuery();

										PreparedStatement ps_acpt = conn.prepareStatement(SQLacpt);
										ResultSet rs_acpt = ps_acpt.executeQuery();

										PreparedStatement ps_rjct = conn.prepareStatement(SQLrjct);
										ResultSet rs_rjct = ps_rjct.executeQuery();

										// For Pending Request
										int cnt = 1;
										while (rs_pen.next()) {
											expected_com = "";
											requested_on = "";
											if (rs_pen.getString(34) != null && !(rs_pen.getString(34).equals("") || rs_pen.getString(34).equals("null"))) {
										dddd = rs_pen.getString(34);
										Date d = new SimpleDateFormat("dd/MM/yyyy").parse(dddd);

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
										expected_com = sdf.format(d);
											}
											if (rs_pen.getString(5) != null && !(rs_pen.getString(5).equals("") || rs_pen.getString(5).equals("null"))) {
										Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy").parse(rs_pen.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

										requested_on = sdf.format(temp_requested_Date);
											}
											if (rs_pen.getString(5) != null && !(rs_pen.getString(5).equals("") || rs_pen.getString(5).equals("null"))) {
										Date temp_requested_Date_time = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs_pen.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

										requested_on_time = sdf.format(temp_requested_Date_time);
											}
											Date cur_date = new Date();
											SimpleDateFormat sdFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
											String dateIS = sdFormat.format(cur_date);
											String startDate = requested_on + " 00:00:00";
											String endDate = dateIS;

											Date startDateObj = sdFormat.parse(startDate);
											Date endDateObj = sdFormat.parse(endDate);
											long timeDiff = endDateObj.getTime() - startDateObj.getTime();

											long daysDiff = timeDiff / (1000 * 60 * 60 * 24);

											if ((daysDiff >= 3) && (rs_pen.getString(16).equals("Pending"))) {
									%>
									<tr style="background-color: #FFB6C1;">
										<%
											} else {
										%>
									
									<tr>
										<%
											}
										%>
										<td><%=rs_pen.getString(1)%></td>
										<td><%=((rs_pen.getString(37) != null)?rs_pen.getString(37) : "")%></td>
										<td><%=requested_on + ", " + requested_on_time%></td>
										<td>
											<%
												String name = "";
											PreparedStatement ps_name = conn.prepareStatement(
													"SELECT id, name,cname,type,cust_master_id FROM reg WHERE id ='" + rs_pen.getString(2) + "' and flag = '1'");
											ResultSet rs_name = ps_name.executeQuery();
											while (rs_name.next()) {
												if (rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

													String SQl_main_com = "SELECT * FROM reg where id = '" + rs_name.getString(5) + "'";
													PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
													ResultSet rs_main_com = ps_main_com.executeQuery();
													while (rs_main_com.next()) {
												name = rs_main_com.getString(6) + " (" + rs_name.getString(2) + ")";
													}

												} else {
													name = rs_name.getString(3) + " (" + rs_name.getString(2) + ")";
												}

											}
											rs_name.close();
											ps_name.close();
											%> <%=name%>
										</td>

										<td>
											<%
											String reType = "";
											String etadate = "";
											if (rs_pen.getString(17).equals("New Export")) {
												reType = "Export (" + rs_pen.getString(7) + ")";
												etadate = rs_pen.getString(4);
											} else if (rs_pen.getString(17).equals("New Import")) {

												reType = "Import (" + rs_pen.getString(3) + ")";
												etadate = rs_pen.getString(4);
											} else if (rs_pen.getString(17).equals("Request of Permit")) {

												etadate = rs_pen.getString(4);
											} else if (rs_pen.getString(17).equals("Local Delivery")) {

												etadate = rs_pen.getString(4);
											} else {
												reType = rs_pen.getString(17);
											}
											%> <%=etadate%>
										</td>
										<form action="code/reqstatusupdate.jsp" method="post">
											<td><select class="form-control" name="status"
												style="width: 80px; padding: 0 !important;">

													<%
														try {
														PreparedStatement pst = conn.prepareStatement("select * from status where status <> 'Accepted (Revised)' order by status asc");
														ResultSet rst = pst.executeQuery();
														while (rst.next()) {
															if (rs_pen.getString(16).equals(rst.getString(1))) {
													%>
													<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
													<%
														} else {
													%>
													<option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>
													<%
														}
													}
													rst.close();
													pst.close();
													} catch (Exception e) {
													System.out.println(e);
													}
													%>
											</select> <input type="hidden" name="tid"
												value="<%=rs_pen.getString(25)%>"> <input
												type="hidden" name="handby"
												value="<%=session.getAttribute("uname")%>"></td>
											<td><button type="submit"
													style="background-color: transparent; border: none; padding: 0 !important;"
													title="Update Status">
													<img src="img/update.png"
														style="width: 24px; height: 24px;" />
												</button></td>

										</form>
										<td><%=((rs_pen.getString(35) != null)?rs_pen.getString(35):"")%></td>
										<td><%=rs_pen.getString(19)%></td>
										<td>
											<%
												if (rs_pen.getString(37) != null && !rs_pen.getString(37).equals("")) {
												if (rs_pen.getString(39) != null && rs_pen.getString(39).equals("0")) {
											%> <a class="btn btn-primary" href="javascript:;"
											data-toggle="modal"
											data-target="#myModal<%=rs_pen.getString(25)%>">Send to
												VCC</a>

											<div id="myModal<%=rs_pen.getString(25)%>" class="modal fade"
												role="dialog">
												<div class="modal-dialog">
													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Send to VCC</h4>
														</div>
														<div class="modal-body">


															<div class="row">
																<div class="col-md-6">
																	<a class="btn btn-primary form-group"
																		href="code/staff_send3rdparty.jsp?tid=<%=rs_pen.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=0">NORMAL</a>
																</div>
																<div class="col-md-6">
																	<div class="pull-right">
																		<a class="btn btn-primary form-group"
																			href="code/staff_send3rdparty.jsp?tid=<%=rs_pen.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=1">URGENT</a>
																	</div>
																</div>
															</div>
														</div>

														<div class="modal-footer">
															<button type="button" class="btn btn-default"
																data-dismiss="modal">Close</button>
														</div>
													</div>

												</div>
											</div> <%
 	} else {
 %> <span> Sent</span> <%
 	}

 }
 %>
										</td>
										<td><a href="sview.jsp?m=<%=rs_pen.getString(25)%>"><img
												src="img/view.png" style="width: 16px; height: 16px;"
												title="View" /></a></td>
										<td><a
											href="staff_req_del_edit.jsp?m=<%=rs_pen.getString(25)%>"><img
												src="img/edit.png" style="width: 16px; height: 16px;"
												title="Edit" /></a></td>
									</tr>
									<%
										cnt++;
									}
									// For Accept Request 
									cnt = 1;
									while (rs_acpt.next()) {
									expected_com = "";
									requested_on = "";
									if (rs_acpt.getString(34) != null && !(rs_acpt.getString(34).equals("") || rs_acpt.getString(34).equals("null"))) {
										dddd = rs_acpt.getString(34);

										Date d = new SimpleDateFormat("dd/MM/yyyy").parse(dddd);

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
										expected_com = sdf.format(d);
									}
									if (rs_acpt.getString(5) != null && !(rs_acpt.getString(5).equals("") || rs_acpt.getString(5).equals("null"))) {
										Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs_acpt.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

										requested_on = sdf.format(temp_requested_Date);
									}
									if (rs_acpt.getString(5) != null && !(rs_acpt.getString(5).equals("") || rs_acpt.getString(5).equals("null"))) {
										Date temp_requested_Date_time = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs_acpt.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

										requested_on_time = sdf.format(temp_requested_Date_time);
									}
									Date cur_date = new Date();
									SimpleDateFormat sdFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
									String dateIS = sdFormat.format(cur_date);
									String startDate = requested_on + " 00:00:00";
									String endDate = dateIS;

									Date startDateObj = sdFormat.parse(startDate);
									Date endDateObj = sdFormat.parse(endDate);
									long timeDiff = endDateObj.getTime() - startDateObj.getTime();

									long daysDiff = timeDiff / (1000 * 60 * 60 * 24);

									if ((daysDiff >= 3) && (rs_acpt.getString(16).equals("Pending"))) {
									%>
									<tr style="background-color: #FFB6C1;">
										<%
											} else {
										%>
									
									<tr>
										<%
											}
										%>
										<td><%=rs_acpt.getString(1)%></td>
										<td><%=((rs_acpt.getString(37) != null)?rs_acpt.getString(37):"")%></td>
										<td><%=requested_on + ", " + requested_on_time%></td>

										<td>
											<%
												String name = "";

											PreparedStatement ps_name = conn.prepareStatement(
													"SELECT id, name,cname,type,cust_master_id FROM reg WHERE id ='" + rs_acpt.getString(2) + "' and flag = '1'");
											ResultSet rs_name = ps_name.executeQuery();
											while (rs_name.next()) {
												if (rs_name.getString(4) != null && rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

													String SQl_main_com = "SELECT * FROM reg where id = '" + rs_name.getString(5) + "'";
													PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
													ResultSet rs_main_com = ps_main_com.executeQuery();
													while (rs_main_com.next()) {
												name = rs_main_com.getString(6) + " (" + rs_name.getString(2) + ")";
													}
													rs_main_com.close();
													ps_main_com.close();

												} else {
													name = rs_name.getString(3) + " (" + rs_name.getString(2) + ")";
												}

											}
											rs_name.close();
											ps_name.close();
											%> <%=name%>
										</td>

										<td>
											<%
												String reType = "";
											String etadate = "";
											if (rs_acpt.getString(17) != null && rs_acpt.getString(17).equals("New Export")) {
												reType = "Export (" + rs_acpt.getString(7) + ")";
												etadate = rs_acpt.getString(4);
											} else if (rs_acpt.getString(17) != null && rs_acpt.getString(17).equals("New Import")) {

												reType = "Import (" + rs_acpt.getString(3) + ")";
												etadate = rs_acpt.getString(4);
											} else if (rs_acpt.getString(17) != null && rs_acpt.getString(17).equals("Request of Permit")) {

												etadate = rs_acpt.getString(4);
											} else if (rs_acpt.getString(17) != null && rs_acpt.getString(17).equals("Local Delivery")) {

												etadate = rs_acpt.getString(4);
											} else {
												reType = rs_acpt.getString(17);
											}
											%> <%=etadate%>
										</td>
										<form action="code/reqstatusupdate.jsp" method="post">
											<td><select class="form-control" name="status"
												style="width: 80px; padding: 0 !important;">

													<%
														try {
														PreparedStatement pst = conn.prepareStatement("select * from status  where status IN('Accepted','Accepted (Revised)')order by status asc");
														ResultSet rst = pst.executeQuery();
														while (rst.next()) {
															if (rs_acpt.getString(16) != null && rs_acpt.getString(16).equals(rst.getString(1))) {
													%>
													<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
													<%
														} else {
													%>
													<option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>
													<%
														}
													}
														rst.close();
														pst.close();
													} catch (Exception e) {
													System.out.println(e);
													}
													%>
											</select> <input type="hidden" name="tid"
												value="<%=rs_acpt.getString(25)%>"> <input
												type="hidden" name="handby"
												value="<%=session.getAttribute("uname")%>"></td>
											<td><button type="submit"
													style="background-color: transparent; border: none; padding: 0 !important;"
													title="Update Status">
													<img src="img/update.png"
														style="width: 24px; height: 24px;" />
												</button></td>

										</form>
										<td><%=((rs_acpt.getString(35) != null)?rs_acpt.getString(35):"")%></td>
										<td><%=rs_acpt.getString(19)%></td>
										<td>
											<%
												if (rs_acpt.getString(37) != null && !rs_acpt.getString(37).equals("")) {
												if (rs_acpt.getString(39) != null && rs_acpt.getString(39).equals("0")) {
											%> <a class="btn btn-primary" href="javascript:;"
											data-toggle="modal"
											data-target="#myModal<%=rs_acpt.getString(25)%>">Send to
												VCC</a>
											<div id="myModal<%=rs_acpt.getString(25)%>"
												class="modal fade" role="dialog">
												<div class="modal-dialog">
													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Send to VCC</h4>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-md-6">
																	<a class="btn btn-primary form-group"
																		href="code/staff_send3rdparty.jsp?tid=<%=rs_acpt.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=0">NORMAL</a>
																</div>
																<div class="col-md-6">
																	<div class="pull-right">
																		<a class="btn btn-primary form-group"
																			href="code/staff_send3rdparty.jsp?tid=<%=rs_acpt.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=1">URGENT</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default"
																data-dismiss="modal">Close</button>
														</div>
													</div>

												</div>
											</div> <%
 	} else {
 %> <span> Sent</span> <%
 	}
 }
 %>



										</td>
										<td><a href="sview.jsp?m=<%=rs_acpt.getString(25)%>"><img
												src="img/view.png" style="width: 16px; height: 16px;"
												title="View" /></a></td>
										<td><a
											href="staff_req_del_edit.jsp?m=<%=rs_acpt.getString(25)%>"><img
												src="img/edit.png" style="width: 16px; height: 16px;"
												title="Edit" /></a></td>
									</tr>
									<%
										cnt++;
									}

									// FOR Reject Request 
									cnt = 1;
									while (rs_rjct.next()) {
									expected_com = "";
									requested_on = "";
									if (!rs_rjct.getString(34).equals("")) {
										dddd = rs_rjct.getString(34);

										Date d = new SimpleDateFormat("dd/MM/yyyy").parse(dddd);

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
										expected_com = sdf.format(d);
									}
									if (!rs_rjct.getString(5).equals("")) {
										Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy").parse(rs_rjct.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

										requested_on = sdf.format(temp_requested_Date);
									}
									if (!rs_rjct.getString(5).equals("")) {
										Date temp_requested_Date_time = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs_rjct.getString(5));

										SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");

										requested_on_time = sdf.format(temp_requested_Date_time);
									}
									Date cur_date = new Date();
									SimpleDateFormat sdFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
									String dateIS = sdFormat.format(cur_date);
									String startDate = requested_on + " 00:00:00";
									String endDate = dateIS;

									Date startDateObj = sdFormat.parse(startDate);
									Date endDateObj = sdFormat.parse(endDate);
									long timeDiff = endDateObj.getTime() - startDateObj.getTime();

									long daysDiff = timeDiff / (1000 * 60 * 60 * 24);

									if ((daysDiff >= 3) && (rs_rjct.getString(16).equals("Pending"))) {
									%>
									<tr style="background-color: #FFB6C1;">
										<%
											} else {
										%>
									
									<tr>
										<%
											}
										%>
										<td><%=rs_rjct.getString(1)%></td>
										<td><%=rs_rjct.getString(37)%></td>
										<td><%=requested_on + ", " + requested_on_time%></td>
										<td>
											<%
												String name = "";

											PreparedStatement ps_name = conn.prepareStatement(
													"SELECT id, name,cname,type,cust_master_id FROM reg WHERE id ='" + rs_rjct.getString(2) + "' and flag = '1'");
											ResultSet rs_name = ps_name.executeQuery();
											while (rs_name.next()) {
												if (rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

													String SQl_main_com = "SELECT * FROM reg where id = '" + rs_name.getString(5) + "'";
													PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
													ResultSet rs_main_com = ps_main_com.executeQuery();
													while (rs_main_com.next()) {
												name = rs_main_com.getString(6) + " (" + rs_name.getString(2) + ")";
													}

												} else {
													name = rs_name.getString(3) + " (" + rs_name.getString(2) + ")";
												}

											}
											rs_name.close();
											ps_name.close();
											%> <%=name%>
										</td>

										<td>
											<%
												String reType = "";
											String etadate = "";
											if (rs_rjct.getString(17) != null && rs_rjct.getString(17).equals("New Export")) {
												reType = "Export (" + rs_rjct.getString(7) + ")";
												etadate = rs_rjct.getString(4);
											} else if (rs_rjct.getString(17) != null && rs_rjct.getString(17).equals("New Import")) {

												reType = "Import (" + rs_rjct.getString(3) + ")";
												etadate = rs_rjct.getString(4);
											} else if (rs_rjct.getString(17) != null && rs_rjct.getString(17).equals("Request of Permit")) {

												etadate = rs_rjct.getString(4);
											} else if (rs_rjct.getString(17) != null && rs_rjct.getString(17).equals("Local Delivery")) {

												etadate = rs_rjct.getString(4);
											} else {
												reType = rs_rjct.getString(17);
											}
											%> <%=etadate%>
										</td>
										<form action="code/reqstatusupdate.jsp" method="post">
											<td><select class="form-control" name="status"
												style="width: 80px; padding: 0 !important;">

													<%
														try {
														PreparedStatement pst = conn.prepareStatement("select * from status where status <> 'Accepted (Revised)' order by status asc");
														ResultSet rst = pst.executeQuery();
														while (rst.next()) {
															if (rs_rjct.getString(16).equals(rst.getString(1))) {
													%>
													<option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
													<%
														} else {
													%>
													<option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>
													<%
														}
													}
													rst.close();
													pst.close();
													} catch (Exception e) {
													System.out.println(e);
													}
													%>
											</select> <input type="hidden" name="tid"
												value="<%=rs_rjct.getString(25)%>"> <input
												type="hidden" name="handby"
												value="<%=session.getAttribute("uname")%>"></td>
											<td><button type="submit"
													style="background-color: transparent; border: none; padding: 0 !important;"
													title="Update Status">
													<img src="img/update.png"
														style="width: 24px; height: 24px;" />
												</button></td>

										</form>
										<td><%=rs_rjct.getString(35)%></td>
										<td><%=rs_rjct.getString(19)%></td>
										<td>
											<%
												if (!rs_rjct.getString(37).equals("")) {
												if (rs_rjct.getString(39).equals("0")) {
											%> <a class="btn btn-primary" href="javascript:;"
											data-toggle="modal"
											data-target="#myModal<%=rs_rjct.getString(25)%>">Send to
												VCC</a>
											<div id="myModal<%=rs_rjct.getString(25)%>"
												class="modal fade" role="dialog">
												<div class="modal-dialog">
													<!-- Modal content-->
													<div class="modal-content">
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal">&times;</button>
															<h4 class="modal-title">Send to VCC</h4>
														</div>
														<div class="modal-body">
															<div class="row">
																<div class="col-md-6">
																	<a class="btn btn-primary form-group"
																		href="code/staff_send3rdparty.jsp?tid=<%=rs_rjct.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=0">NORMAL</a>
																</div>
																<div class="col-md-6">
																	<div class="pull-right">
																		<a class="btn btn-primary form-group"
																			href="code/staff_send3rdparty.jsp?tid=<%=rs_rjct.getString(25)%>&handby=<%=session.getAttribute("uname")%>&u=1">URGENT</a>
																	</div>
																</div>
															</div>
														</div>
														<div class="modal-footer">
															<button type="button" class="btn btn-default"
																data-dismiss="modal">Close</button>
														</div>
													</div>

												</div>
											</div> <%
 	} else {
 %> <span> Sent</span> <%
 	}
 }
 %>


										</td>
										<td><a href="sview.jsp?m=<%=rs_rjct.getString(25)%>"><img
												src="img/view.png" style="width: 16px; height: 16px;"
												title="View" /></a></td>
										<td><a
											href="staff_req_del_edit.jsp?m=<%=rs_rjct.getString(25)%>"><img
												src="img/edit.png" style="width: 16px; height: 16px;"
												title="Edit" /></a></td>
									</tr>
									<%
										cnt++;
									}
									} catch (Exception e) {
									System.out.println(e);
									}
									%>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
                     immediately after the control sidebar -->

	</div>
	<!-- ./wrapper -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
	<script
		src="https://cdn.datatables.net/plug-ins/1.10.15/sorting/datetime-moment.js"></script>



	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<!-- AdminLTE App -->


	<!-- AdminLTE for demo purposes -->
	<script src="ltmcss/js/demo.js"></script>

	<script type="text/javascript"
		src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
	<link rel="stylesheet" type="text/css"
		href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
	<script type="text/javascript" class="init">
		$(document).ready(
				function() {
					//                $.fn.dataTable.moment('DD/MM/YY');

					var ticketTable;
					j$ = jQuery.noConflict();
					j$.fn.dataTable.moment('DD/MM/YYYY, hh:mm a');
					ticketTable = j$('[id$="example"]').DataTable({
						"scrollX" : false,
						order : [],
						columnDefs : [ {
							orderable : false,
							targets : [ 0 ]
						} ]
					});

					j$('input[name="eta_filter"]').daterangepicker(
							{
								"autoApply" : true,
								"autoUpdateInput" : false,
								"maxDate" : moment().format('MM/D/YYYY')
							},
							function(start, end, label) {
								console.log("A new date selection was made: "
										+ start.format('YYYY-MM-DD') + ' to '
										+ end.format('YYYY-MM-DD'));

								j$('input[name="eta_filter"]').val(
										start.format('DD/MM/YYYY') + " - "
												+ end.format('DD/MM/YYYY'));

							});

					//                $('#example').DataTable({
					//                    "scrollX": false,
					//                    order: [],
					//                    columnDefs: [ { orderable: false, targets: [0] },{ type: 'date-dd/mm/yyyy', targets: 2 }]
					//                     
					//                });

				});
	</script>
	</body>
	
</html>
<%
} finally {
	if(cm != null) {
		cm.close();
	}
}
%>