<%@page import="java.sql.*"%>
<%@page import="pack1.ConnectionManager"%>
<%
   String type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp"); 
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
    }
    if(!type.equals("Customer")){
        out.println("<script>window.history.back();</script>");
    }
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
%>
<%try { %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
       

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script>
            function validateForm()
            {
                var x = document.forms["forget"]["pwd"].value;
                var y = document.forms["forget"]["pwd1"].value;
                if (x != y) {
                    alert("Password not match..!");
                    return false;
                }
            }
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
         <jsp:include page="customer_menu.jsp" />

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Create New Sub Customer
                        <!--<small>Control panel</small>--> 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    <!-- Small boxes (Stat box) --> 

                    <!-- Main row --> 
                    <!-- /.row (main row) -->
                    <div class="container" style="margin-top:10px; width:100%;">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <form method="post" action="code/customersubreg.jsp" name="forget" onsubmit="return validateForm()">
                                    <div class="panel-heading"> <u> </u> </div>
                                    <br>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%;"> User Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" placeholder="User Name" type=text   name="id" id="id"  
                                               required="required" onkeyup="remove_space();">
                                         
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Name  : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Name" type="text" name="uname" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Contact No : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <input class="form-control" placeholder="Contact No" type="text" name="cno" required="required">
                                    </div>


                                    <!-- <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Company Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Company Name" type="text" name="cname" required="required">
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Representative Name : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Representative Name" type="text" name="rname">
                                        </div>
                                    </div> -->
                                    
                                    
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Email : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Email" type="email" name="email" required="required">
                                        </div>
                                    </div>
                                    <div class="col-md-2" style="display: none;">
                                        <div class="form-group" style="margin-top:5%"> Location : </div>
                                    </div>
                                    <div class="col-md-4" style="display: none;">
                                        <div class="form-group">
                                            
                                            <% 
                                                 String str_location="";
                                                try { 
                                                    PreparedStatement ps = conn.prepareStatement("SELECT id, name, cno, email, pwd, cname, repname, type, count, flag, dt, location, cdate, mdate, tid, cust_master_id FROM reg WHERE id= '"+session.getAttribute("uname")+"'");
                                                    ResultSet rst = ps.executeQuery();
                                                    while (rst.next()) {     
                                                        str_location = rst.getString(12); 
                                                    }
                                                } catch (Exception e) {
                                                    System.out.println(e);
                                                }
                                            %>
                                            <input class="form-control" name="location" value="<%=str_location%>">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Password : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Password" type="password" name="pwd" required="required" pattern="(.){8,20}" title="Password should be at least 8 characters.">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group" style="margin-top:5%"> Confirm Password : </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <input class="form-control" placeholder="Enter Confirm Password" type="password" name="pwd1" required="required" pattern="(.){8,20}" title="Password should be at least 8 characters.">
                                        </div>
                                    </div>
                                    <div class="col-md-12" align="center"> <br>
                                        <input type="hidden" name="uid" value="<%=session.getAttribute("uname")%>">
                                        <button type="submit" class="btn btn-primary">Submit </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar --> 

        </div>
        <!-- ./wrapper --> 

        <!-- jQuery 2.1.4 --> 
        <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script> 
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
            $.widget.bridge('uibutton', $.ui.button);
            function remove_space(){
                var data = 
                $('#id').val($('#id').val().trim());
            }
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
       
        <!-- daterangepicker --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
      
        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 
       
        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>
    </body>
</html>

<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>