<%
    String menu_type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp"); 
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        menu_type = session.getAttribute("type").toString();
    }
     
    
%> 
<aside class="main-sidebar"> 
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar"> 

                    <!-- /.search form --> 
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menus</li>
                        <li class="active treeview"> </li>
                        <li> <a href="cust_my_request.jsp"> <i class="fa fa-user"></i> <span>My Request</span> </a> </li>
                        <li class="treeview">
                            <a href="#">
                                <i class="fa fa-edit"></i> <span>New Request</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" style="display: none;">
                                <li><a href="cust_new_request.jsp"><i class="fa fa-hand-o-left"></i> New Import</a></li>
                                <li><a href="cust_export_request.jsp"><i class="fa fa-hand-o-right"></i> New Export</a></li>
                                <li><a href="cust_request_of_permit.jsp"><i class="fa fa-thumbs-o-up"></i> Request of Permit</a></li>
                                <li><a href="local_delivery.jsp"><i class="fa fa-dedent"></i> Local Delivery</a></li>
                            </ul>
                        </li>
                        <% if(menu_type.equals("Customer")){ %>  
                            <li class="treeview">
                                <a href="#">
                                    <i class="fa fa-edit"></i> <span>Customer Management</span> <i class="fa fa-angle-left pull-right"></i>
                                </a>
                                <ul class="treeview-menu" style="display: none;">
                                    <li><a href="subcustomer.jsp"><i class="fa fa-hand-o-left"></i> New Sub Customer</a></li>
                                    <li><a href="subcustomerlist.jsp"><i class="fa fa-hand-o-right"></i> Sub Customer</a></li>
                                </ul>
                            </li>
                        <% } %>     
              
                        <li> <a href="email_master.jsp"> <i class="fa fa-envelope"></i> <span>Email Master</span> </a> </li>
                        <li> <a href="change_password.jsp"> <i class="fa fa-key"></i> <span>Change Password</span> </a> </li>
                        
                        
                        <li style="position:absolute; bottom:0px; width:100%; background:rgba(255,255,255,.2);"> <a href="logout.jsp"> <i class="fa fa-power-off" aria-hidden="true"></i>
                                <span>Sign out</span> </a> </li>
                    </ul>
                </section>
                <!-- /.sidebar --> 
            </aside>