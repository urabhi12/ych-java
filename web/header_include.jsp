<%-- 
    Document   : header_include
    Created on : Jun 20, 2018, 12:13:23 PM
    Author     : dell
--%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%
	String menu_type = "", UserType = "", name = "", customer = "", log_User_id = "", log_token = "";
if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
	//response.sendRedirect("../index.jsp"); 
	out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
} else {
	menu_type = session.getAttribute("type").toString();
	name = session.getAttribute("name").toString();
	Object cname = session.getAttribute("company");
	customer = cname != null ? cname.toString() : "";
	log_User_id = session.getAttribute("user_id").toString();
	log_token = session.getAttribute("logtoken").toString();
}
if (menu_type.equals("Customer")) {
	UserType = customer + " - " + name + "</p><p>Main Customer Account";
} else if (menu_type.equals("Customer-Sub")) {
	UserType = customer + " - " + name + "</p><p>Sub Customer Account";
} else if (menu_type.equals("Admin")) {
	UserType = name + "</p><p>Admin Account";
} else if (menu_type.equals("Staff")) {
	UserType = name + "</p><p>Staff Account";
}

ConnectionManager cm = new ConnectionManager();
Connection conn = cm.getCon();
PreparedStatement ps_login_acc_chk = conn
		.prepareStatement("select * from login_access where user_id='" + log_User_id + "'");
ResultSet rs_login_acc_chk = ps_login_acc_chk.executeQuery();

if (rs_login_acc_chk.next()) {
	if (!log_token.equals(rs_login_acc_chk.getString(3))) {
		out.println("<script>alert('Logged in by other System...');window.location.href='index.jsp';</script>");
	}
} else {
	out.println("<script>alert('Logged in by other System...');window.location.href='index.jsp';</script>");
}
%>
<%try { %>
<!--<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">-->
<header class="main-header">
	<!-- Logo -->
	<a href="#" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
		<!-- <span class="logo-mini"><b>A</b>LT</span>--> <!-- logo for regular state and mobile devices -->
		<span class="logo-lg"><b><img src="img/ychlogo.png"
				style="width: 90px;" /></b></span>
	</a>



	<!-- Header Navbar: style can be found in header.less -->
	<nav class="navbar navbar-static-top" role="navigation">
		<!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="Top_heading">
			<p>
				Welcome
				<%=UserType%></p>
		</div>
		<div class="navbar-custom-menu">
			<!--<ul class="nav navbar-nav">
              
             
              <li class="dropdown user user-menu"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="ltmcss/img/user2-160x160.jpg" class="user-image" alt="User Image"> <span class="hidden-xs">Admin Name</span> </a>
                <ul class="dropdown-menu">
                  
           
                  <li class="user-footer">
                    <div class="pull-right"> <a href="index.jsp" class="btn btn-default btn-flat">Sign out</a> </div>
                  </li>
                </ul>
              </li>
            </ul>-->
		</div>
	</nav>
</header>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>