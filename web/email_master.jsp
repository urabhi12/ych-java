<%-- 
    Document   : email_master
    Created on : Jun 15, 2018, 11:14:07 AM
    Author     : Mohit Rai
--%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%
    String type="",user_id="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp"); 
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
        user_id = session.getAttribute("user_id").toString();
    }
    if(!type.equals("Customer") && !type.equals("Customer-Sub")){
        out.println("<script>window.history.back();</script>");
    }
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
%>
<%@page contentType="text/html" pageEncoding="windows-1252"%>
<%try { %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">


        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript" language="javascript" src="bootstrap/js/datatable_dateformat.js"></script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
            <jsp:include page="customer_menu.jsp" /> 

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Email Master </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    <form  action="code/email_master_process.jsp" method="post">
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
                                <!-- small box -->
                                <div> Set Type</div>
                                <select class="form-control" name="settype" required>
                                    <option value="">Select</option>
                                    <option value="New Import">New Import</option>
                                    <option value="New Export">New Export</option>
                                    <option value="Request of Permit">Request of Permit</option>
                                    <option value="Local Delivery">Local Delivery</option>
                                </select>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2"> 
                                <!-- small box -->
                                <div> Name</div>
                                <input type="text" class="form-control" placeholder="Please Enter Name" name="txt_name" required>
                            </div>

                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3"> 
                                <!-- small box -->
                                <div> Email Id</div>
                                <input type="email" class="form-control" placeholder="Please Enter Email Id" name="txt_emailid" 
                                       id="txt_emailid"  
                                       required>
                            </div>

                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"> 
                                <input type="hidden" name="userid" value="<%= user_id %>">
                                <input type="hidden" name="usertype" value="<%= type %>">
                                <button type="submit" class="btn btn-primary" style="margin-top:20px;">Add Email</button>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4"> 
                                <p><% if(request.getParameter("error")!=null){
                                    if(request.getParameter("error").equals("00")){
                                        out.println("<div class='alert alert-success'><strong>Success!</strong> Email Deletion is Successfully.</div>");
                                    } else if(request.getParameter("error").equals("11")){
                                        out.println("<div class='alert alert-danger'><strong>Error!</strong> Email Deletion is not Successfull.</div>");
                                    
                                    } else if(request.getParameter("error").equals("0")){
                                        out.println("<div class='alert alert-success'><strong>Success!</strong> Email Added Successfull.</div>");
                                    }  else if(request.getParameter("error").equals("1")){
                                        out.println("<div class='alert alert-danger'><strong>Error!</strong>  Email Id Already in Use.</div>");
                                    }else if(request.getParameter("error").equals("2")){
                                        out.println("<div class='alert alert-danger'><strong>Error!</strong> Error on Adding Email.</div>");                                     
                                    }
                                } %></p>
                            </div>


                        </div>
                    </form>
                    <div class="container-fluid" style="margin-top:20px;  height: 100%;" >
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id="example" class="table table-responsive" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Set Type</th>
                                            <th>Name</th> 
                                            <th>Email</th>

                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>    
                                        <%
                                            try {
                                            String SQL = "select * from email_master where user_id = '" + user_id + "'";                                               
                                            PreparedStatement ps = conn.prepareStatement(SQL);
                                            ResultSet rs = ps.executeQuery();
                                            int cnt = 1;                                            
                                            while (rs.next()) { 
                                        %>
                                        <tr>
                                            <td><%= rs.getString("set_type") %></td>
                                            <td><%= rs.getString("name") %></td>
                                            <td><%= rs.getString("emails") %></td>

                                            <td><a onclick="return confirm('Are you sure?')" href="code/delete_email.jsp?id=<%= rs.getString("id") %>">
                                                    <img src="img/delete.png" style="width: 24px; height: 24px;" title="Delete"/>
                                                </a>
                                            </td>

                                        </tr>
                                        <%  } 
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }
                                        %>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. 
            </footer>

        </div>
        <!-- ./wrapper --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.8.4/moment.min.js"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.15/sorting/datetime-moment.js"></script>
        <script type="text/javascript" class="init">

            $(document).ready(function () {
                $('#example').DataTable({});
            });
             
        </script>
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 

        <!-- daterangepicker --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 

        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 

        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>




    </body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>