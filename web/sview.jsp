<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%
	String cust_type = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp");
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		cust_type = session.getAttribute("type").toString();
	}
	if (!cust_type.equals("Staff")) {
		out.println("<script>window.history.back();</script>");
	}

	String s = request.getParameter("m");

	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	String request_id = "";
	String userid = "";
	String type = "";
	String DArival = "";

	String Create_Date = "";
	String Modify_Date = "";

	String fcl = "";
	String EDDeparture = "";
	String Desti_Ports = "";
	String No_of_Container = "";
	String Size_of_Container = "";
	String Type_of_Container = "";
	String Place_Delivery = "";
	String Bonded_Truck = "";

	String Permit_Type = "";
	String Collection_Date = "";

	String Date_Of_Delivery = "";
	String Pickup_Address = "";
	String Delivery_Address = "";
	String Total_No_Package = "";
	String Total_Weight = "";
	String TOTAL_NO_OF_PALLETS = "";
	String TOTAL_M3 = "";
	String Package_Dimension = "";
	String Special_Request = "";

	String Request_Type = "";
	String Request_Status = "";
	String Expected_Comple = "";
	String Handle_by = "";
	String Additional_Comment = "";
	String Permit_to = "";
	String Permit_from = "";
	String Pickup_to = "";
	String Pickup_from = "";
	String Remarks = "";
	String tx3id = "";
	String By = "";
	String cust_ref_no = "";
	String vcc_instru = "";
	int tid = 0;

	try {
		PreparedStatement ps = conn.prepareStatement("select * from req where tid='" + s + "'");
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Request_Type = rs.getString(17);
			tid = Integer.parseInt(rs.getString(25).toString());
			request_id = rs.getString(1);
			userid = rs.getString(2);
			if (Request_Type.equals("New Import")) {
				type = rs.getString(3);
				fcl = rs.getString(8);
				if (rs.getString(4)!=null && !(rs.getString(4).equals("") || rs.getString(4).equals("null"))) {
					Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(4));

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					DArival = sdf.format(temp_requested_Date);
				}

			} else if (Request_Type.equals("New Export")) {
				type = rs.getString(7);
				fcl = rs.getString(8);
				if (rs.getString(4)!=null && !(rs.getString(4).equals("") || rs.getString(4).equals("null"))) {
					Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(4));

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					EDDeparture = sdf.format(temp_requested_Date);
				}

				Desti_Ports = rs.getString(9);
				No_of_Container = rs.getString(11);
				Size_of_Container = rs.getString(12);
				Type_of_Container = rs.getString(13);
				Place_Delivery = rs.getString(10);
				Bonded_Truck = rs.getString(14);
			} else if (Request_Type.equals("Request of Permit")) {
				Permit_Type = rs.getString(15);
				if (rs.getString(4)!=null && !(rs.getString(4).equals("") || rs.getString(4).equals("null"))) {
					Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(4));

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					Collection_Date = sdf.format(temp_requested_Date);
				}

			} else if (Request_Type.equals("Local Delivery")) {
				if (rs.getString(4)!=null && !(rs.getString(4).equals("") || rs.getString(4).equals("null"))) {
					Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(4));

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					Date_Of_Delivery = sdf.format(temp_requested_Date);
				}

				Pickup_Address = rs.getString(26);
				Delivery_Address = rs.getString(27);
				Total_No_Package = rs.getString(28);
				Total_Weight = rs.getString(29);
				TOTAL_NO_OF_PALLETS = rs.getString(30);
				TOTAL_M3 = rs.getString(31);
				Package_Dimension = rs.getString(32);
				Special_Request = rs.getString(33);

			} else {

			}
			if (rs.getString(5)!=null && !(rs.getString(5).equals("") || rs.getString(5).equals("null"))) {
				Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(rs.getString(5));

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm a");

				Create_Date = sdf.format(temp_requested_Date);
			}
			if (rs.getString(6)!=null && !(rs.getString(6).equals("") || rs.getString(6).equals("null"))) {
				Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy").parse(rs.getString(6));

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

				Modify_Date = sdf.format(temp_requested_Date);
			}
			if (rs.getString(34)!=null && !(rs.getString(34).equals("") || rs.getString(34).equals("null"))) {

				Date d = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(34));

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				Expected_Comple = sdf.format(d);
			}
			Request_Status = rs.getString(16);

			Handle_by = rs.getString(19);
			Additional_Comment = rs.getString(35);

			Permit_to = rs.getString(21);
			Permit_from = rs.getString(22);
			Pickup_to = rs.getString(23);
			Pickup_from = rs.getString(24);
			Remarks = rs.getString(36);
			tx3id = rs.getString(37);
			By = rs.getString(2);
			cust_ref_no = rs.getString(38);
			vcc_instru = rs.getString(40);
		}
	} catch (Exception e) {

		System.out.println(e);
	}
%>
<%try { %>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.5 -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
<!-- Morris chart -->
<link rel="stylesheet" href="plugins/morris/morris.css">
<!-- jvectormap -->
<link rel="stylesheet"
	href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<!-- Date Picker -->
<link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
<!-- Daterange picker -->
<link rel="stylesheet"
	href="plugins/daterangepicker/daterangepicker-bs3.css">
<!-- bootstrap wysihtml5 - text editor -->
<link rel="stylesheet"
	href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
<style>
thead {
	background-color: #4471C4;
	color: #FFFFFF;
}

.even-row {
	background-color: #CFD4EA;
}

.odd-row {
	background-color: #E9EBF5;
}
</style>
 <link rel="stylesheet" href="upload.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">

				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu">
					<li class="header">Menus</li>
					<li class="active treeview"></li>
					<li><a href="staff_request_detail.jsp"> <i
							class="fa fa-reply" aria-hidden="true"></i> <span>Request</span>
					</a></li>
					<li><a href="staff_permit_dec_list.jsp"> <i
							class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Permit
								Declaration</span>
					</a></li>
					<li><a href="staff_pickup_date_list.jsp"> <i
							class="fa fa-gift" aria-hidden="true"></i> <span>Pickup</span>
					</a></li>
					<li
						style="position: absolute; bottom: 0px; width: 100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);">
						<a href="logout.jsp"> <i class="fa fa-power-off"
							aria-hidden="true"></i> <span>Sign out</span>
					</a>
					</li>
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<section class="content-header">
				<h1>
					View Details
					<!--<small>Control panel</small>-->
				</h1>
				<a href="javascript:;" class="pull-right btn btn-primary top_print"
					onclick="CallPrint('content')"> Print</a>
			</section>

			<!-- Main content -->
			<section class="content">
				<!-- Small boxes (Stat box) -->

				<!-- Main row -->
				<!-- /.row (main row) -->
				<div class="container" style="width: 100%;">
					<div class="panel panel-default">
						<div class="panel-body" id="content">
							<div class="panel-heading">
								<u> </u>
							</div>
							<div class="col-md-2 form-group">Request ID :</div>
							<div class="col-md-4 form-group">
								<pre><%=request_id%></pre>
							</div>
							<div class="col-md-2 form-group">Request Type :</div>
							<div class="col-md-4 form-group">
								<pre><%=Request_Type%></pre>
							</div>
							<div class="col-md-2 form-group">TX3ID :</div>
							<div class="col-md-4 form-group">
								<pre><%=tx3id%></pre>
							</div>
							<%
								if (Request_Type.equals("New Import")) {
							%>

							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Import Type :</div>
							<div class="col-md-2 form-group">
								<pre><%=type%></pre>
							</div>

							<%
								if (!type.equals("Air")) {
							%>
							<div class="col-md-2 form-group">FCL/ LCL :</div>
							<div class="col-md-2 form-group">
								<pre><%=fcl%></pre>
							</div>
							<%
								}
							%>

							<div class="col-md-2 form-group">Date of Arrival :</div>
							<div class="col-md-2 form-group">
								<pre><%=DArival%></pre>
							</div>

							<%
								} else if (Request_Type.equals("New Export")) {
							%>

							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Export Type :</div>
							<div class="col-md-4 form-group">
								<pre><%=type%></pre>
							</div>
							<%
								if (!type.equals("Air")) {
							%>
							<div class="col-md-2 form-group">FCL/ LCL :</div>
							<div class="col-md-4 form-group">
								<pre><%=fcl%></pre>
							</div>
							<%
								}
							%>


							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Expected Date of Departure
								:</div>
							<div class="col-md-4 form-group">
								<pre><%=EDDeparture%></pre>
							</div>
							<%
								if (!type.equals("Cross Border")) {
							%>
							<div class="col-md-2 form-group">Destination Ports(AIR/SEA)
								:</div>
							<div class="col-md-4 form-group">
								<pre><%=Desti_Ports%></pre>
							</div>
							<%
								}
							%>

							<%
								if (type.equals("Cross Border")) {
							%>
							<div class="col-md-2 form-group">Place of Delivery :</div>
							<div class="col-md-4 form-group">
								<pre><%=Place_Delivery%></pre>
							</div>
							<%
								}
							%>


							<div class="clearfix "></div>
							<%
								if (type.equals("Cross Border")) {
							%>
							<div class="col-md-2 form-group">Bonded Truck :</div>
							<div class="col-md-4 form-group">
								<pre><%=Bonded_Truck%></pre>
							</div>
							<%
								}
							%>
							<div class="clearfix "></div>
							<%-- <%
								if (!type.equals("Air")) {
										if (fcl.equals("FCL")) {
											try {
												String NoOfContainer = "", SizeOfContainer = "", TypeOfContainer = "";
												PreparedStatement ps_container = conn
														.prepareStatement("select * from request_container where request_id='" + tid + "'");
												ResultSet rs_container = ps_container.executeQuery();
												int ci = 1;
												while (rs_container.next()) {
													if (ci == 1) {
							%>
							<div class="col-md-12 bg-blue"
								style="padding-top: 10px; margin-bottom: 10px;">
								<%
									}
														NoOfContainer = rs_container.getString(3);
														SizeOfContainer = rs_container.getString(4);
														TypeOfContainer = rs_container.getString(5);
								%>

								<div class="col-md-12">
									<div class="col-md-1">
										<div class="form-group" style="margin-top: 5%">
											<%=ci%>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">No. of
											Container :</div>
									</div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">

											<%=NoOfContainer%>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">Size of
											Container :</div>
									</div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">

											<%=SizeOfContainer%>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">Type of
											Container :</div>
									</div>
									<div class="col-md-1">
										<div class="form-group" style="margin-top: 5%">
											<%=TypeOfContainer%>
										</div>
									</div>

								</div>
								<%
									ci++;
													}
													if (ci > 0) {
								%>
							</div>
							<%
								}
											} catch (Exception e) {
												System.out.println(e);
											}
							%>

							<%
								}
									}
							%> --%>


							<%
								} else if (Request_Type.equals("Request of Permit")) {
							%>



							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Permit Type :</div>
							<div class="col-md-4 form-group">
								<pre><%=Permit_Type%></pre>
							</div>
							<div class="col-md-2 form-group">Cargo Collection Date From
								Bonded/ Licence Warehouse :</div>
							<div class="col-md-4 form-group">
								<pre><%=Collection_Date%></pre>
							</div>




							<%
								} else if (Request_Type.equals("Local Delivery")) {
							%>


							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Date of Delivery :</div>
							<div class="col-md-4 form-group">
								<pre><%=Date_Of_Delivery%></pre>
							</div>
							<div class="col-md-2 form-group">Total Weight :</div>
							<div class="col-md-4 form-group">
								<pre><%=Total_Weight%></pre>
							</div>
							<div class="clearfix "></div>

							<div class="col-md-2 form-group">Total no. of pallets /
								packages :</div>
							<div class="col-md-4 form-group">
								<pre><%=TOTAL_NO_OF_PALLETS%></pre>
							</div>
							<div class="col-md-2 form-group">Total M3 :</div>
							<div class="col-md-4 form-group">
								<pre><%=TOTAL_M3%></pre>
							</div>

							<div class="clearfix "></div>

							<div class="col-md-2 form-group">Package Dimension</div>
							<div class="col-md-4 form-group">
								<pre><%=Package_Dimension%></pre>
							</div>
							<div class="col-md-2 form-group">Special Request :</div>
							<div class="col-md-4 form-group">
								<pre><%=Special_Request%></pre>
							</div>
							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Pick up Address :</div>
							<div class="col-md-10 form-group">
								<pre><%=Pickup_Address%></pre>
							</div>
							<div class="clearfix "></div>
							<div class="col-md-2 form-group">Delivery Address :</div>
							<div class="col-md-10 form-group">
								<pre><%=Delivery_Address%></pre>
							</div>



							<%
								} else {
							%>
							<div class="clearfix"></div>

							<%
								}
							%>
							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Customer Reference No:</div>
							<div class="col-md-4 form-group">
								<pre><%=cust_ref_no%></pre>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Remark :</div>
							<div class="col-md-10 form-group">
								<pre><%=Remarks%></pre>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Request On :</div>
							<div class="col-md-4 form-group">
								<pre><%=Create_Date%></pre>
							</div>
							<div class="col-md-2 form-group">Request Status :</div>
							<div class="col-md-4 form-group">
								<pre><%=Request_Status%></pre>
							</div>


							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Expected Completion :</div>
							<div class="col-md-4 form-group">
								<pre><%=Expected_Comple%></pre>
							</div>
							<div class="col-md-2 form-group">By :</div>
							<div class="col-md-4 form-group">
								<%
									String name = "";
									PreparedStatement ps_name = conn
											.prepareStatement("SELECT id, name,cname FROM reg WHERE id ='" + By + "' and flag = '1'");
									ResultSet rs_name = ps_name.executeQuery();
									while (rs_name.next()) {
										name = rs_name.getString(3) + " (" + rs_name.getString(2) + ")";
									}
								%>

								<pre><%=name%></pre>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Permit Date To :</div>
							<div class="col-md-4 form-group">
								<pre><%=Permit_to%></pre>
							</div>
							<div class="col-md-2 form-group">Permit Date From :</div>
							<div class="col-md-4 form-group">
								<pre><%=Permit_from%></pre>
							</div>
							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Pickup Date To :</div>
							<div class="col-md-4 form-group">
								<pre><%=Pickup_to%></pre>
							</div>
							<div class="col-md-2 form-group">Pickup Date From :</div>
							<div class="col-md-4 form-group">
								<pre><%=Pickup_from%></pre>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-2 form-group">Status :</div>
							<div class="col-md-10 form-group">
								<pre><%=Additional_Comment%></pre>
							</div>

							<div class="clearfix"></div>

							<div class="col-md-2">
								<div class="form-group" style="margin-top: 5%">Instruction
									to VCC :</div>
							</div>
							<div class="col-md-10">
								<div class="form-group">
									<pre><%=vcc_instru%></pre>
								</div>
							</div>
							<div class="clearfix"></div>




							<div class="col-md-2 form-group">Handle by :</div>
							<div class="col-md-4 form-group">
								<pre><%=Handle_by%></pre>
							</div>

							<div class="clearfix"></div>
							<div class="col-md-12" id="uploadimg">
								<div class="col-md-2">Upload File :</div>
								<div class="col-md-10">
									<%
										try {
											String upfile = "", img_icon_path = "",img_name="";
											PreparedStatement ps = conn.prepareStatement("select * from request_upload where req_id='" + tid + "'");
											ResultSet rs1 = ps.executeQuery();
											int i = 1;
											while (rs1.next()) {
												upfile = rs1.getString(3);
												if (rs1.getString(4).equals("Customer")) {
													img_icon_path = "img/Downloads.png";
												} else if (rs1.getString(4).equals("Staff")) {
													img_icon_path = "img/RedDownloads.png";
												}
												img_name=rs1.getString("upload_file");
												
									%>


									|<a class="upload_<%= rs1.getString("UPLOAD_BY_TYPE") %>" href="uploadproduct/<%=upfile%>"
										download="uploadproduct/<%=upfile%>" target="_blank"> 
                                          <%= img_name %>
									</a>&nbsp;

									<%
										i++;
											}
										} catch (Exception e) {
											System.out.println(e);
										}
									%>
								</div>
							</div>
							<!-- Start For More File Upload -->

							<div class="panel-body">
								<fieldset>
									<legend>Upload More Files</legend>
									<form
										action="code/request_staff_upload_more_image.jsp?tid=<%=s%>"
										method="post" enctype="multipart/form-data" id="form1">
										<div class="clearfix"></div>

										<div class="col-md-12 form-group">
											<div class="optionBox">
												<!-- <input name="file" type="file" id="file" multiple="true">-->
												<div class="col-md-3 block">
													<input type="file" name="uploadfiles" required="required"
														multiple="true">
												</div>
											</div>
										</div>
										<div class="clearfix"></div>

										<div class="col-md-12 form-group">

											<a href="javascript:;" class="btn btn-primary add">Add
												More Image</a>
											<button type="submit" class="btn btn-warning">Update
											</button>


										</div>
										<div class="col-md-6"></div>
									</form>
								</fieldset>
							</div>


							<!-- End For More File Upload -->

							<div class="clearfix"></div>
							<div class="col-md-12">
								<h2>
									Revision log
									<h2>
							</div>
							<div class="col-md-12">
								<table id="rev_table" class="table table-bordered"
									cellspacing="0">
									<thead>
										<tr>
											<th>Status</th>
											<th>Updated By</th>
											<th>Date & Time</th>
										</tr>
									</thead>
									<tbody>
										<%
											try {

												PreparedStatement rev_log_pen = conn.prepareStatement(
														"select * from request_update_log where req_id='" + tid + "' order by tid Desc");
												ResultSet rev_log_row_pen = rev_log_pen.executeQuery();

												int i = 1;
												while (rev_log_row_pen.next()) {
													String Status = rev_log_row_pen.getString(3);

													String name_log = "";
													PreparedStatement ps_name_log = conn.prepareStatement("SELECT id, name FROM reg WHERE id ='"
															+ rev_log_row_pen.getString(4) + "' and flag = '1'");
													ResultSet rs_name_log = ps_name_log.executeQuery();
													while (rs_name_log.next()) {
														name_log = rs_name_log.getString(2);
													}

													String Updated_By = name_log;
													String Date_Time = rev_log_row_pen.getString(5);
													if (rev_log_row_pen.getString(5)!=null && !rev_log_row_pen.getString(5).equals("")) {
														Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy HH:mm:ss").parse(Date_Time);

														SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

														Date_Time = sdf.format(temp_requested_Date);
													}
													if ((i % 2) == 0) {
										%>
										<tr class="odd-row">
											<%
												} else {
											%>
										
										<tr class="even-row">
											<%
												}
											%>
											<td><%=Status%></td>
											<td><%=Updated_By%></td>
											<td><%=Date_Time%></td>
										<tr>
											<%
												i++;
													}

												} catch (Exception e) {
e.printStackTrace();
													System.out.println(e);
												}
											%>
										
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

			</section>
		</div>
		<!-- /.content -->

		<!-- /.content-wrapper -->
		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
                     immediately after the control sidebar -->

	</div>
	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->
	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	<!-- jQuery UI 1.11.4 -->
	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
	<script>
		$.widget.bridge('uibutton', $.ui.button);
		(function() {

			var beforePrint = function() {
				$('#uploadimg').hide();

			};

			var afterPrint = function() {
				$('#uploadimg').show();

			};

			if (window.matchMedia) {
				var mediaQueryList = window.matchMedia('print');

				mediaQueryList.addListener(function(mql) {
					//alert($(mediaQueryList).html());
					if (mql.matches) {
						beforePrint();
					} else {
						afterPrint();
					}
				});
			}

			window.onbeforeprint = beforePrint;
			window.onafterprint = afterPrint;

		}());
		function CallPrint(divName) {
			var printContents = document.getElementById(divName).innerHTML;
			var originalContents = document.body.innerHTML;

			document.body.innerHTML = printContents;

			window.print();

			document.body.innerHTML = originalContents;
		}
		$('.add')
				.click(
						function() {
							$('.block:last')
									.after(
											'<div class="col-md-3 block"><input type="file" name="uploadfiles" required="required" multiple="true"><a href="javascript:;" class="remove">Remove</a></div>');
						});
		$('.optionBox').on('click', '.remove', function() {
			$(this).parent().remove();
		});
	</script>
	<!-- Bootstrap 3.3.5 -->
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<!-- Morris.js charts -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>

	<!-- daterangepicker -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>

	<!-- AdminLTE App -->
	<script src="ltmcss/js/app.min.js"></script>

	<!-- AdminLTE for demo purposes -->
	<script src="ltmcss/js/demo.js"></script>


</body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>