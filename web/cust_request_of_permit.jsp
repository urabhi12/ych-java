<%@page import="java.util.Random"%>
<%@page import="org.apache.commons.fileupload.FileItem"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.apache.commons.fileupload.FileUploadException"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.fileupload.FileItemFactory"%>
<%@page import="org.apache.commons.fileupload.disk.DiskFileItemFactory"%>
<%@page import="org.apache.commons.fileupload.servlet.ServletFileUpload"%>
<%@page import="pack1.Set_Para_Value"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="pack1.FileUploadManager"%>
<%@page import="java.io.File"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.util.Date"%>
<%@page import="pack1.Send_Email"%>
<%
	String type = "", user_id = "", Userid = "";
	if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
		//response.sendRedirect("../index.jsp"); 
		out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
	} else {
		type = session.getAttribute("type").toString();
		user_id = session.getAttribute("user_id").toString();
		Userid = session.getAttribute("uname").toString();
	}
	if (!type.equals("Customer") && !type.equals("Customer-Sub")) {
		out.println("<script>window.history.back();</script>");
	}
	String id = "";
	String permitid = "";
	Date d = new Date();
	SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy HH:mm:ss");
	String y = sdf.format(d);
	ConnectionManager cm = new ConnectionManager();
	Connection conn = cm.getCon();

	String Req_job_no = "", Req_id = "", Req_Permit_Type = "", Req_CC_Date = "", Req_Reference_No = "",
			Req_Remark = "",Req_Status="", Req_PostType = "New";

	try {
		boolean isPost = "POST".equals(request.getMethod());

		if (isPost) {
			//String Path = "C://Program Files//Apache Software Foundation//Tomcat 8.5//webapps//YCH//uploadproduct";
			String Path = Send_Email.getPath();

			String Old_img_For_Update = "";

			ArrayList<String> arr = new ArrayList<String>();

			Set_Para_Value SPV = new Set_Para_Value();
			boolean isMultipart = ServletFileUpload.isMultipartContent(request);
			if (!isMultipart) {
			} else {
				FileItemFactory factory = new DiskFileItemFactory();
				ServletFileUpload upload = new ServletFileUpload(factory);
				List items = null;
				try {
					items = upload.parseRequest(request);
				} catch (FileUploadException e) {
					e.printStackTrace();
				}
				Iterator itr = items.iterator();
				while (itr.hasNext()) {
					FileItem item = (FileItem) itr.next();

					if (item.isFormField()) {

						String Fname = item.getFieldName();
						String Fvalue = item.getString();
						if (Fname.equals("req_img_path")) {
							if (Old_img_For_Update == "") {
								Old_img_For_Update = "'" + Fvalue + "'";
							} else {
								Old_img_For_Update = Old_img_For_Update + ",'" + Fvalue + "'";
							}
						}

						SPV.Set_Value(item);
					} else {

						try {
							String itemName = item.getName();
							if (!itemName.equals("")) {
								Random randid = new Random();
								int n = randid.nextInt(10000) + 1;
								itemName = "rop_" + String.valueOf(n) + "_" + itemName.replace("#", "_").replace("%", "_ ").replace("+", "_ ");
								File savedFile = new File(Path + itemName);
								item.write(savedFile);
								arr.add(itemName);
							}

							//out.println("<tr><td><b>Your file has been saved at the loaction:</b></td></tr><tr><td><b>" + config.getServletContext().getRealPath("/") + "uploadedFiles" + "\\" + itemName + "</td></tr>");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				}
			}

			Req_PostType = SPV.getFormValue("reqtype");
			Req_id = SPV.getFormValue("reqid");
			Req_job_no = SPV.getFormValue("reqjobno");

			String cso_name = "", cso_email = "", UserEmail = "";

			PreparedStatement ps_cso_name = conn.prepareStatement(
					"SELECT id, name,cname,type,cust_master_id,cso,email FROM reg WHERE id ='"
							+ Userid + "' and flag = '1'");

			ResultSet rs_cso_name = ps_cso_name.executeQuery();

			while (rs_cso_name.next()) {
				UserEmail = rs_cso_name.getString(7);
				if (rs_cso_name.getString(4).equalsIgnoreCase("Customer-Sub")) {

					String SQl_main_com = "SELECT * FROM reg where id = '" + rs_cso_name.getString(5) + "'";
					PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
					ResultSet rs_main_com = ps_main_com.executeQuery();
					while (rs_main_com.next()) {
						cso_name = rs_main_com.getString(18);
					}

				} else {
					cso_name = rs_cso_name.getString(6);
				}
				String SQl_main_cso_email = "SELECT email FROM reg where id = '" + cso_name + "'";
				PreparedStatement ps_main_cso_email = conn.prepareStatement(SQl_main_cso_email);
				ResultSet rs_main_cso_email = ps_main_cso_email.executeQuery();
				while (rs_main_cso_email.next()) {
					cso_email = rs_main_cso_email.getString(1);
				}

			}

			if (Req_id == "" && Req_PostType.equals("New")) {

				//if (arr.size() > 0) {

					String Requestid = "REQPER";
					String generatedColumns[] = {"tid"};
					PreparedStatement ps = conn.prepareStatement(
							"insert into req values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",
							generatedColumns);
					ps.setString(1, "REQPER" + SPV.getFormValue("id"));
					ps.setString(2, SPV.getFormValue("uid"));
					ps.setString(3, "");
					ps.setString(4, SPV.getFormValue("da"));
					ps.setString(5, y);
					ps.setString(6, y);
					ps.setString(7, "");
					ps.setString(8, "");
					ps.setString(9, "");
					ps.setString(10, "");
					ps.setString(11, "");
					ps.setString(12, "");
					ps.setString(13, "");
					ps.setString(14, "");
					ps.setString(15, SPV.getFormValue("pertype"));
					ps.setString(16, "Pending");
					ps.setString(17, "Request of Permit");
					ps.setString(18, SPV.getFormValue("cmpname"));
					ps.setString(19, cso_name);
					ps.setString(20, SPV.getFormValue("filename"));
					ps.setString(21, "");
					ps.setString(22, "");
					ps.setString(23, "");
					ps.setString(24, "");
					ps.setObject(25, null);//sequence
					ps.setString(26, "");
					ps.setString(27, "");
					ps.setString(28, "");
					ps.setString(29, "");
					ps.setString(30, "");
					ps.setString(31, "");
					ps.setString(32, "");
					ps.setString(33, "");
					ps.setString(34, "");
					ps.setString(35, "");
					ps.setString(36, SPV.getFormValue("remark"));
					ps.setString(37, "");
					ps.setString(38, SPV.getFormValue("customer_ref_no"));
					ps.setInt(39, 0);
					ps.setString(40, "");
					int n = ps.executeUpdate();
					if (n == 1) {
						ResultSet rs = ps.getGeneratedKeys();

						if (rs.next()) {
							int abc = rs.getInt(1);
							Requestid = Requestid + abc;
							PreparedStatement ps_update = conn.prepareStatement(
									"update req set id='" + Requestid + "'  where tid='" + abc + "'");

							ps_update.executeUpdate();
							int ecouny = 0;
							for (int i = 0; i < arr.size(); i++) {
								//                    while (abc > 1) {
								PreparedStatement ps2 = conn.prepareStatement(
										"insert into request_upload values(?,?,?,?,?)");
								ps2.setObject(1, null);//sequence
								ps2.setString(2, String.valueOf(abc));
								ps2.setString(3, arr.get(i).toString());
								ps2.setString(4, "Customer");
								ps2.setString(5, Userid);
								int n2 = ps2.executeUpdate();
								if (n2 == 1) {
									ecouny++;
									//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_new_request.jsp'</script>");
								}
							}

							// For Company and Customer Name
							String company = "", Custname = "";
							PreparedStatement ps_name = conn.prepareStatement(
									"SELECT id, name,cname,type,cust_master_id  FROM reg WHERE id ='"
											+ SPV.getFormValue("uid") + "'");
							ResultSet rs_name = ps_name.executeQuery();
							while (rs_name.next()) {
								Custname = rs_name.getString(2);
								if (rs_name.getString(4).equalsIgnoreCase("Customer-Sub")) {
									String SQl_main_com = "SELECT * FROM reg where id = '"
											+ rs_name.getString(5) + "'";
									PreparedStatement ps_main_com = conn.prepareStatement(SQl_main_com);
									ResultSet rs_main_com = ps_main_com.executeQuery();
									while (rs_main_com.next()) {
										company = rs_main_com.getString(6);
									}

								} else {
									company = rs_name.getString(3);
								}
							}
							// For CSO Email
							if (!cso_email.equals("")) {
								String[] cso_emails = new String[1];
								cso_emails[0] = cso_email;

								String cso_Header = "YCH Freight Portal";
								String cso_Subject = "New job request created-" + company;
								String cso_message = Custname + " of " + company
										+ " created a Request of Permit request. The request id is " + Requestid
										+ ". <br> <br> This is an automated email. Please do not reply to this.";

								boolean cso_mailstatus = Send_Email.send(cso_emails, cso_Header, null, null,
										cso_Subject, cso_message, null);
							}
							// For Email
							boolean imail = SPV.getFormValue("ismail") != null;
							boolean issent;
							if (imail) {
								try {
									String SQL = "select * from email_master where user_id = '" + user_id
											+ "' and set_type = 'Request of Permit'";
									PreparedStatement psemail = conn.prepareStatement(SQL,
											ResultSet.TYPE_SCROLL_INSENSITIVE,
										    ResultSet.CONCUR_READ_ONLY);
									ResultSet rsemail = psemail.executeQuery();
									rsemail.last();
									int count = rsemail.getRow();
									rsemail.beforeFirst();
									//out.println("<script>alert("+count+")</script>");

									String[] emails = new String[count];

									int cntemaill = 0;
									while (rsemail.next()) {

										emails[cntemaill] = rsemail.getString("emails");
										cntemaill++;
									}
									String Message = SPV.getFormValue("remark");
									/* String Message  = "Dear Concern,<br/><br/>"
									+ "New Request of Permit Request submission is successful."
									+ " Reference ID for this request is " + Requestid + "<br/><br/>Regards,<br/>YCH Team"; */

									String Subject = "Request of Permit-" + SPV.getFormValue("customer_ref_no")
											+ "-" + SPV.getFormValue("da");

									String strheader = Custname + "-" + company;

									ArrayList<String> arr_attachment = new ArrayList<String>();

									for (int attachcnt = 0; attachcnt < arr.size(); attachcnt++) {
										arr_attachment
												.add(Send_Email.getPath() + arr.get(attachcnt).toString());
									}

									String[] usr_emails = new String[1];
									usr_emails[0] = UserEmail;

									boolean mailstatus = Send_Email.sendwithAttachment(emails, strheader,
											usr_emails, null, Subject, Message, arr_attachment);

									//boolean mailstatus =  Send_Email.send(emails,strheader, null, null,Subject, Message,null);
									if (mailstatus) {
										issent = true;
									} else {
										issent = false;
									}
									if (ecouny == arr.size()) {
										out.println(
												"<script>alert('Request submission is successful. Your reference ID for this request is "
														+ Requestid
														+ "'); window.location.href='cust_my_request.jsp';</script>");
									} else {
										out.println(
												"<script>alert('Request submission not Successfull'); window.location.href='cust_request_of_permit.jsp';</script>");
									}
								} catch (Exception e) {
									System.out.println(e);
								}
							} else {
								if (ecouny == arr.size()) {
									out.println(
											"<script>alert('Request submission is successful. Your reference ID for this request is "
													+ Requestid
													+ "'); window.location.href='cust_my_request.jsp';</script>");
								} else {
									out.println(
											"<script>alert('Request submission not Successfull'); window.location.href='cust_request_of_permit.jsp';</script>");
								}
							}

						}
						PreparedStatement psi = conn.prepareStatement("update sno set sno=sno+1");
						psi.executeUpdate();
						//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_export_request.jsp'</script>");
					} else {
						out.println(
								"<script>alert('Request submission is not Successfull');window.location.href='cust_request_of_permit.jsp'</script>");
					}

				//}
			} else {
				// For Update Page

				String imp_type = "";
				String pRemark = SPV.getFormValue("remark").replaceAll("'","&#039;");
				String status = SPV.getFormValue("status");
				boolean updateStatus = status!=null && status.matches("Draft\\s+Ready|Draft\\s+Ok");
				PreparedStatement ps = conn.prepareStatement("update req set pertype='"
						+ SPV.getFormValue("pertype") + "', da='" + SPV.getFormValue("da") + "'," + " remarks='"
						+ pRemark + "', cust_ref_no='" + SPV.getFormValue("customer_ref_no")
						+ "', is_send_vcc = '0', "+(updateStatus?("status = '"+status+"',"):"status='Pending',")+"    mdate='" + y + "' where tid='" + Req_id
						+ "'");
				int n = ps.executeUpdate();
				if (n == 1) {
					int ecouny = 0;
					String generatedColumns[] = {"tid"};
					if (!Old_img_For_Update.equals("")) {
						PreparedStatement psdel = conn
								.prepareStatement("Delete from request_upload where upload_file not in ("
										+ Old_img_For_Update + ") and req_id = '" + Req_id + "'");
						int ndel = psdel.executeUpdate();
					}else{
						PreparedStatement psdel = conn
								.prepareStatement("Delete from request_upload where req_id = '" + Req_id + "'");
						int ndel = psdel.executeUpdate();
					}
					for (int i = 0; i < arr.size(); i++) {

						PreparedStatement ps2 = conn.prepareStatement(
								"insert into request_upload values(?,?,?,?,?)");
						ps2.setObject(1, null);//sequence
						ps2.setString(2, String.valueOf(Req_id));
						ps2.setString(3, arr.get(i).toString());
						ps2.setString(4, "Customer");
						ps2.setString(5, Userid);

						int n2 = ps2.executeUpdate();
						if (n2 == 1) {
							ecouny++;
							//out.println("<script>alert('Request submission is Successfull');window.location.href='cust_new_request.jsp'</script>");
						}
					}

					// For CSO Email 
					if (!cso_email.equals("")) {
						String[] cso_emails = new String[1];
						cso_emails[0] = cso_email;

						String cso_Header = "YCH Freight Portal";
						String cso_Subject = "Job " + Req_job_no + " was edited";
						String cso_message = "";

						boolean cso_mailstatus = Send_Email.send(cso_emails, cso_Header, null, null,
								cso_Subject, cso_message, null);
					}

					if (ecouny == arr.size()) {
						out.println(
								"<script>alert('Update successful!');window.location.href='cust_my_request.jsp'</script>");
					} else {
						out.println(
								"<script>alert('Update not successful!');window.location.href='cust_my_request.jsp'</script>");
					}

				} else {
					out.println(
							"<script>alert('Update not successful!');window.location.href='cust_my_request.jsp'</script>");
				}

			}
		} else {
			if (request.getParameter("id") != null) {
				Req_id = request.getParameter("id");
				PreparedStatement ps = conn.prepareStatement("select * from req where tid='" + Req_id + "'");
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					Req_job_no = rs.getString(1);
					Req_PostType = "Edit";
					Req_Permit_Type = rs.getString(15);
					Req_CC_Date = rs.getString(4);
					Req_Reference_No = rs.getString(38);
					Req_Remark = rs.getString(36);
					Req_Status = rs.getString("STATUS");

				}

			}
		}
	} catch (Exception e) {
		e.printStackTrace();
		out.println(e.getMessage());
	}
%>
<%try { %>
<!DOCTYPE html>

<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>YCH | Dashboard</title>

<!-- Tell the browser to be responsive to screen width -->

<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">

<!-- Bootstrap 3.3.5 -->

<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.css">
<link rel="stylesheet" href="bootstrap/css/bootstrap-datepicker.min.css">
<!-- Font Awesome -->

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Ionicons -->

<link rel="stylesheet"
	href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

<!-- Theme style -->

<link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">

<!-- AdminLTE Skins. Choose a skin from the css/skins
        
                 folder instead of downloading all of them to reduce the load. -->

<link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">

<!-- iCheck -->

<link rel="stylesheet" href="plugins/iCheck/flat/blue.css">



<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->

<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<!--[if lt IE 9]>
        
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        
            <![endif]-->
<link rel="stylesheet" href="upload.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<jsp:include page="header_include.jsp" />

		<!-- Left side column. contains the logo and sidebar -->

		<jsp:include page="customer_menu.jsp" />

		<!-- Content Wrapper. Contains page content -->

		<div class="content-wrapper">

			<!-- Content Header (Page header) -->

			<section class="content-header">
				<h1>
					Request of Permit

					<!--<small>Control panel</small>-->

				</h1>
			</section>

			<!-- Main content -->

			<section class="content">

				<!-- Small boxes (Stat box) -->

				<!-- /.row -->

				<!-- Main row -->

				<!-- /.row (main row) -->

				<div class="container" style="margin-top: 50px; width: 100%;">
					<div class="panel panel-default">
						<div class="panel-body">
							<form action="cust_request_of_permit.jsp" method="post"
								enctype="multipart/form-data">
								<%
									if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
										//response.sendRedirect("../index.jsp");
										out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
									}

									try {
										//        Class.forName("com.mysql.jdbc.Driver");
										//        Connection con = DriverManager.getConnection(host, username, password);

										PreparedStatement ps = conn.prepareStatement("select * from sno");
										ResultSet rs = ps.executeQuery();
										while (rs.next()) {
											id = rs.getString(1);
											permitid = id;

										}
									} catch (Exception e) {
										System.out.println(e);
									}
								%>
								<div class="panel-heading">
									<u> </u>
								</div>




								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Permit
										Type :</div>
								</div>
								<div class="col-md-4">
									<input class="form-control" placeholder="Permit Type"
										type="text" name="pertype" required="required"
										value="<%=Req_Permit_Type%>">
								</div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Cargo
										Collection Date From Bonded/ Licence Warehouse:</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control" type="text" name="da" id="cc_date"
											required="required" value="<%=Req_CC_Date%>">
										<!--  <p>Date: <input type="text" id="datepicker"></p>-->
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Customer
										Reference No:</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<input class="form-control"
											placeholder="Customer Reference No" type="text"
											name="customer_ref_no" value="<%=Req_Reference_No%>">
									</div>
								</div>
								<div class="clearfix"></div>
								<%
									if (Req_Status.equalsIgnoreCase("accepted")) {
									%>
								
									<div class="clearfix"></div>
									<div class="col-md-2">
										<div class="form-group" style="margin-top: 5%">Status :
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<select class="form-control" name="status">
											<option value=""></option>
											<option>Draft Ok</option>
											<option>Draft Ready</option>
											</select>
										</div>
									</div>
									<%
								}
								%>

								<div class="clearfix"></div>
								<div class="col-md-2">
									<div class="form-group" style="margin-top: 5%">Remarks :
									</div>
								</div>
								<div class="col-md-10">
									<div class="form-group">
										<textarea class="form-control" rows="6" type="text" name="remark"><%=Req_Remark%></textarea>
									</div>
								</div>
								<div class="clearfix"></div>
								<div class="col-md-2">

									<div class="form-group" style="margin-top: 5%">Upload
										Files :</div>

								</div>

								<div class="col-md-10">

									<div class="form-group optionBox">
										<!-- <input name="file" type="file" id="file" multiple="true">-->


										<div class="col-md-12">
											<%
												try {
													String upfile = "",upfile_name="";
													PreparedStatement ps = conn.prepareStatement(
															"select * from request_upload where req_id='" + Req_id + "' and upload_by_type = 'Customer'");
													ResultSet rs1 = ps.executeQuery();
													int i = 1;
													while (rs1.next()) {
														upfile = rs1.getString(3);
														upfile_name=rs1.getString("UPLOAD_FILE");
											%>
											<div class="col-md-2" id="prev_<%=i%>">
												<input type="hidden" name="req_img_path" value="<%=upfile%>">
												<a class="upload_<%= rs1.getString("UPLOAD_BY_TYPE") %>" href="uploadproduct/<%=upfile%>"
													download="uploadproduct/<%=upfile%>" target="_blank"><%= upfile_name %></a>&nbsp;
												<a href="javascript:;" class="remove text-danger">Remove</a>
											</div>
											<%
												i++;
													}
												} catch (Exception e) {
													System.out.println(e);
												}
											%>
											<br>
										</div>


										<div class="col-md-4 block">
											<input type="file" name="uploadfiles" multiple="true">
										</div>
									</div>

								</div>
								<div class="clearfix"></div>
								<div class="col-md-10 pull-right">

									<div class="form-group">
										<a href="javascript:;" class="btn btn-primary add">Add</a>

									</div>

								</div>

								<input type="hidden" name="id" value="<%=permitid%>" /> <input
									type="hidden" name="uid"
									value="<%=session.getAttribute("uname")%>">
								<%
									String cmpname = "";
									try {
										//        Class.forName("com.mysql.jdbc.Driver");
										//        Connection con = DriverManager.getConnection(host, username, password);

										PreparedStatement ps8 = conn
												.prepareStatement("select * from reg where id='" + session.getAttribute("uname") + "'");
										ResultSet rs8 = ps8.executeQuery();
										while (rs8.next()) {
											cmpname = rs8.getString(6);
										}
									} catch (Exception e) {
										System.out.println(e);
									}
								%>

								<input type="hidden" name="cmpname" value="<%=cmpname%>" />
								<div class="col-md-12" align="center">
									<br> <input type="hidden" name="reqid" value="<%=Req_id%>">
									<input type="hidden" name="reqtype" value="<%=Req_PostType%>">
									<input type="hidden" name="reqjobno" value="<%=Req_job_no%>">
									<%
										if (Req_id == "") {
									%>
									<span class="btn btn-primary"><input type="checkbox"
										name="ismail"> Mail Send </span>
<!-- 									<button type="submit" class="btn btn-primary">Submit</button> -->
									<button type="button" class="btn btn-primary" onclick="this.form.submit(); this.disabled=true;">Submit</button>
									<%
										} else {
									%>
<!-- 									<button type="submit" class="btn btn-primary">Update</button> -->
									<button type="button" class="btn btn-primary" onclick="this.form.submit(); this.disabled=true;">Update</button>
									
									<%
										}
									%>



								</div>
							</form>
						</div>
					</div>
				</div>
			</section>

			<!-- /.content -->

		</div>

		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs"></div>
			<strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.
			</strong> All rights reserved.
		</footer>

		<!-- Add the sidebar's background. This div must be placed
          
                     immediately after the control sidebar -->

	</div>

	<!-- ./wrapper -->

	<!-- jQuery 2.1.4 -->

	<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>

	<!-- jQuery UI 1.11.4 -->

	<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

	<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

	<script>
		$.widget.bridge('uibutton', $.ui.button);
	</script>

	<!-- Bootstrap 3.3.5 -->

	<script src="bootstrap/js/bootstrap.min.js"></script>

	<!-- Morris.js charts -->

	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>



	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.js"></script>
	<script src="bootstrap/js/bootstrap-datepicker.min.js"></script>

	<!-- AdminLTE App -->

	<script src="ltmcss/js/app.min.js"></script>



	<!-- AdminLTE for demo purposes -->

	<script src="ltmcss/js/demo.js"></script>
	<script>
		$(function() {
			$('#cc_date').datepicker({
				format : "dd/mm/yyyy"
			});
			//$('#cc_date').datepicker('setDate', 'today');

			$('.add')
					.click(
							function() {
								$('.block:last')
										.after(
												'<div class="col-md-4 block"><input type="file" name="uploadfiles" required="required" multiple="true"><a href="javascript:;" class="remove">Remove</a></div>');
							});
			$('.optionBox').on('click', '.remove', function() {
				$(this).parent().remove();
			});
		});
	</script>
</body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>