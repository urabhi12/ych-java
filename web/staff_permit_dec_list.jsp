<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%
    String type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
    }
    if(!type.equals("Staff")){
        out.println("<script>window.history.back();</script>");
    } 
    
    ConnectionManager cm = new ConnectionManager();
    Connection conn = cm.getCon();
%>
<%try { %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
                 folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
         <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
<style>
            div.dataTables_wrapper {

                margin: 0 auto;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar"> 
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar"> 

                    <!-- /.search form --> 
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menus</li>
                        <li class="active treeview"> </li>
                        <li> <a href="staff_request_detail.jsp"> <i class="fa fa-reply" aria-hidden="true"></i> <span>Request</span> </a> </li>
                        <li> <a href="staff_permit_dec_list.jsp"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span>Permit Declaration</span> </a> </li>
                        <li> <a href="staff_pickup_date_list.jsp"> <i class="fa fa-gift" aria-hidden="true"></i> <span>Pickup</span> </a> </li>
                        <li style="position:absolute; bottom:0px; width:100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);"> <a href="logout.jsp"> <i class="fa fa-power-off" aria-hidden="true"></i> <span>Sign out</span> </a> </li>
                    </ul>
                </section>
                <!-- /.sidebar --> 
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Permit Declaration
                        <!--<small>Control panel</small>--> 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    <!-- Small boxes (Stat box) -->
                    <form action="staff_permit_dec_list.jsp" method="post">
                        <div class="row">
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"> 
                                <!-- small box -->
                                <div> Request Type</div>
                                <select class="form-control" name="reqtype">
                                    <option>Select</option>
                                    <% if (request.getParameter("reqtype") == null) {%>
                                    <option>New Import</option>
                                    <option>New Export</option>
                                    <option>Request of Permit</option>
                                    <option>Local Delivery</option>
                                    <option>Draft Ready</option>
                                    <option>Draft Ok</option>
                                    <%} else {
                                        if (request.getParameter("reqtype").equals("New Import")) {%>
                                    <option selected="true">New Import</option>
                                    <% } else { %>
                                    <option>New Import</option>
                                    <%}
                                        if (request.getParameter("reqtype").equals("New Export")) {%>
                                    <option selected="true">New Export</option>
                                    <% } else { %>
                                    <option>New Export</option>
                                    <% }
                                        if (request.getParameter("reqtype").equals("Request of Permit")) {%>                                                
                                    <option selected="true">Request of Permit</option>
                                    <% } else { %>
                                    <option >Request of Permit</option>
                                    <%}
                                        if (request.getParameter("reqtype").equals("Local Delivery")) {%>                                                
                                    <option selected="true">Local Delivery</option>
                                    <% } else { %>
                                    <option>Local Delivery</option>
                                    <%}
                                        }%>
                                </select>
                            </div>
                            <!-- ./col -->
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6"> 
                                <!-- small box -->
                                <div> Status</div>
                                <select class="form-control" name="status">
                                    <option>Select</option>                                  
                                    <%
                                        try {
                                            PreparedStatement pst = conn.prepareStatement("select * from status order by status asc");
                                            ResultSet rst = pst.executeQuery();
                                            while (rst.next()) {
                                                if (request.getParameter("status") == null) {%>                                                
                                                    <option><%=rst.getString(1)%></option> 
                                                <%} else {  
                                                    if(request.getParameter("status").equals(rst.getString(1))){ %>
                                                        <option value="<%=rst.getString(1)%>" selected="true"><%=rst.getString(1)%></option>
                                                    <%} else {%>
                                                        <option value="<%=rst.getString(1)%>"><%=rst.getString(1)%></option>   
                                                    <%} 
                                                }
                                            }
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }%>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary" style="margin-top:20px;">Search </button>
                            <!-- ./col --> 
                            <!----><!-- ./col --> 
                        </div>
                    </form>
                    <!-- /.row --> 
                    <!-- Main row --> 
                    <!-- /.row (main row) -->
                    
                        <div   style="margin-top:10px;  height: 100%;" >
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                             <table id="example" class="table table-responsive"  cellspacing="0">
                                <thead>
                                    <tr>
                                         
                                        <th>Requested ID</th>
                                        <th>REQUESTED ON </th>
                                        <th>EXPECTED COMPLETION</th>
                                        <th>REQUEST TYPE</th>
                                        <th>STATUS</th>
                                        <th>Special Instruction</th>
                                        <th>HANDLING BY</th>
                                        <th>PERMIT DATE TO</th>
                                        <th>PERMIT DATE FROM</th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                        String reqtype = request.getParameter("reqtype");
                                        String status = request.getParameter("status");
                                        String permit_on_date = "";
                                        String permit_from_date = "";
                                        String SQL = "";
                                        String dddd = "";
                                        String expected_com="";
                                        String requested_on="";
                                        
                                        if (status == null && reqtype == null) {
                                            SQL = "select * from req where handby ='" + session.getAttribute("uname") + "' and (pdateto !='' and pdatefrom !='') ORDER BY tid DESC";

                                        } else {
                                            String Where = "";
                                            if (!status.equals("Select")) {
                                                Where = Where + " and status='" + status + "' ";
                                            }
                                            if (!reqtype.equals("Select")) {
                                                Where = Where + " and reqtype='" + reqtype + "'";
                                            }
                                            SQL = "select * from req where (pdateto !='' and pdatefrom !='')  and handby = '" + session.getAttribute("uname") + "' " + Where + " ORDER BY tid DESC";

                                        }
                                        
                                        try {
                                            //                                        Class.forName("com.mysql.jdbc.Driver");
                                            //                                        Connection con = DriverManager.getConnection(host, username, password);
                                           // out.println(SQL);
                                            PreparedStatement ps = conn.prepareStatement(SQL);
                                            ResultSet rs = ps.executeQuery();
                                            int cnt =1;
                                            while (rs.next()) {
                                                 
                                                expected_com = "";
                                                requested_on = "";
                                                permit_on_date = "";
                                                permit_from_date = "";
                                                if(!rs.getString(34).equals("")){
                                                    dddd = rs.getString(34);

                                                    Date d = new SimpleDateFormat("dd/MM/yyyy").parse(dddd);

                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                                                    expected_com = sdf.format(d); 
                                                }
                                                if(!rs.getString(5).equals("")){ 
                                                    Date temp_requested_Date = new SimpleDateFormat("dd MMM yyyy").parse(rs.getString(5));

                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                                    requested_on = sdf.format(temp_requested_Date);
                                                }
                                                if(!rs.getString(21).equals("")){ 
                                                    Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(21));

                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                                    permit_on_date = sdf.format(temp_requested_Date);
                                                }
                                                if(!rs.getString(22).equals("")){ 
                                                    Date temp_requested_Date = new SimpleDateFormat("dd/MM/yyyy").parse(rs.getString(22));

                                                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

                                                    permit_from_date = sdf.format(temp_requested_Date);
                                                } 
                                    %>
                                    <tr>
                                        
                                        <td><%=rs.getString(1)%></td>
                                        <td><%=requested_on%></td>
                                        <td><%=expected_com%></td>
                                        <td><%=rs.getString(17)%></td>
                                        <td><%=rs.getString(16)%></td>
                                        <td><%=rs.getString(35)%></td>
                                        <td><%=rs.getString(19)%></td>
                                        <td><%=permit_on_date%></td>
                                        <td><%=permit_from_date%></td>
                                        <td><a href="sview.jsp?m=<%=rs.getString(25)%>"><img src="img/view.png" style="width: 16px; height: 16px;" title="View"/></a></td>
                                           
                                        <td><a href="staff_req_del_edit.jsp?m=<%=rs.getString(25)%>"><img src="img/edit.png" style="width: 16px; height: 16px;" title="Edit"/></a></td>
                                            
                                    </tr>
                                    <%
                                        cnt++;
                                            }
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        }
                                    %>
                                </tbody>
                            </table>
                        
                            </div>
                    </div>
                        </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>

            <!-- Add the sidebar's background. This div must be placed
                     immediately after the control sidebar --> 

        </div>
        <!-- ./wrapper --> 
<script type="text/javascript" class="init">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": false,
                    "aaSorting": []
                });
            });

        </script>
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
            $.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
       
        <!-- daterangepicker --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
         
        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 
         
        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>
    </body>
</html>
<%} finally {
	if(cm != null) {
		cm.close();
	}
}%>