<%@page import="pack1.ConnectionManager"%>
<%@page import="java.sql.*"%>
<%
    String type="";
    if (session.getAttribute("uname") == null || session.getAttribute("uname") == "") {
        //response.sendRedirect("../index.jsp");
        out.println("<script>alert('Please Login first...');window.location.href='index.jsp';</script>");
    }else{
        type = session.getAttribute("type").toString();
    }
    if(!type.equals("Admin")){
        out.println("<script>window.history.back();</script>");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>YCH | Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="bootstrap/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="ltmcss/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="ltmcss/css/skins/_all-skins.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
<style>
            div.dataTables_wrapper {

                margin: 0 auto;
            }
        </style>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <jsp:include page="header_include.jsp" />
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar"> 
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar"> 

                    <!-- /.search form --> 
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">Menus</li>
                        <li class="active treeview"> </li>
                        <li> <a href="staff_profile_details.jsp"> <i class="fa fa-user" aria-hidden="true"></i> <span>Staff</span> </a> </li>
                        <li> <a href="customer_profile_details.jsp"> <i class="fa fa-users" aria-hidden="true"></i> <span>Customer</span> </a> </li>
                        <li> <a href="location_view.jsp"> <i class="fa fa-location-arrow" aria-hidden="true"></i> <span>Location</span> </a> </li>
                        <li> <a href="status_view.jsp"> <i class="fa fa-star" aria-hidden="true"></i> <span>Status</span> </a> </li>
                        <li> <a href="pie_chart.jsp"> <i class="fa fa-pie-chart" aria-hidden="true"></i><span>Pie Chart</span> </a> </li>
                        <li> <a href="bar_chart.jsp"> <i class="fa fa-bar-chart" aria-hidden="true"></i>
                                <span>Bar Chart</span> </a> </li>
                        <li style="position:absolute; bottom:0px; width:100%; background: linear-gradient(to bottom, #2c77d0 0%, #304cd9 100%);"> <a href="logout.jsp"> <i class="fa fa-power-off" aria-hidden="true"></i> <span>Sign out</span> </a> </li>
                    </ul>
                </section>
                <!-- /.sidebar --> 
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper"> 
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Staff Profile
                        <!--<small>Control panel</small>--> 
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content"> 
                    <!-- Small boxes (Stat box) --> 
                    <!--<div class="row">
                      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                        <div> Request Type</div>
                        <select class="form-control">
                          <option>All</option>
                          <option>ID</option>
                          <option>All</option>
                        </select>
                      </div>
                      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                        <div> Status</div>
                        <select class="form-control">
                          <option>All</option>
                          <option>ID</option>
                        
                        </select>
                      </div>
                      <div class="col-lg-2 col-md-3 col-sm-3 col-xs-6">
                        <div>Search</div>
                        <input class="form-control" placeholder="Search" type="text">
                      </div>
                      
                    </div>-->
                    <button type="button" class="btn btn-primary" style="margin-top:20px;" onClick="location.href = 'staff_create_new.jsp';">Create New</button>
                    <!-- /.row --> 
                    <!-- Main row --> 
                    <!-- /.row (main row) -->
                    <div class="container-fluid" style="margin-top:10px;  height: 100%;" >
                            <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <table id="example" class="table table-responsive"  cellspacing="0">
                                <thead>
                                    <tr>
                                        
                                        <th>USER NAME</th>
                                        <th>PASSWORD </th>
                                        <th>NAME</th>
                                        <th>CONTACT NO</th>
                                        <th>EMAIL</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <%
                                    	ConnectionManager cm = null;
                                        try { 
                                            cm = new ConnectionManager();
                                            Connection conn = cm.getCon();
                                            PreparedStatement ps = conn.prepareStatement("select * from reg where flag='1' and (type='Staff' or type='Admin')");
                                            ResultSet rs = ps.executeQuery();
                                            int i = 1;
                                            while (rs.next()) {
                                    %>
                                    <tr>
                                         
                                        <td><%=rs.getString(1)%></td>
                                        <td><%=rs.getString(5)%></td>
                                        <td><%=rs.getString(2)%></td>
                                        <td><%=rs.getString(3)%></td>
                                        <td><%=rs.getString(4)%></td>
                                        <form action="code/statusupdate.jsp" method="post">
                                        <td>
                                            <select name="status" class="form-control">
                                                <% if(rs.getString(8).equals("Staff")) {%>
                                                    <option selected="true">Staff</option>
                                                    <option>Admin</option>
                                                <% } else if(rs.getString(8).equals("Admin")) {%>
                                                    <option>Staff</option>
                                                    <option selected="true">Admin</option>
                                                <% }%>                                                
                                            </select>
                                                
                                        </td>
                                        <td>
                                            <input type="hidden" name="tid" value="<%=rs.getString(15)%>"/>
                                                <input type="hidden" name="handby" value="<%=session.getAttribute("uname")%>"/>
                                                
                                            <button type="submit" style="background-color: transparent;border: none;padding: 0 !important;" title="Update Status"><img src="img/update.png" style="width: 24px; height: 24px;"/></button></td>
                                        </form>
                                        
                                <td><a href="staff_details_updation.jsp?m=<%=rs.getString(15)%>"><img src="img/edit.png" style="width: 24px; height: 24px;" title="Edit"/></a></td>
                                <td><a href="code/sresetpwd.jsp?m=<%=rs.getString(1)%>"><img src="img/reset_icon.png" style="width: 24px; height: 24px;" title="Reset Password"/></a></td>
                                <td><a href="code/sdeletecode.jsp?m=<%=rs.getString(1)%>"><img src="img/delete.png" style="width: 24px; height: 24px;" title="Delete"/></a></td>
                                     <td>
                                            <%  if(rs.getString(17).equals("0")){ %>
                                                    <a onclick="return confirm('Are you sure? You want to Block Staff')" href="code/unblock_staff.jsp?uid=<%=rs.getString(15)%>&st=1" style='color:green;'>Block</a>                                                    
                                            <%  } else{ %>
                                                    <a onclick="return confirm('Are you sure? You want to Un-Block Staff')" href="code/unblock_staff.jsp?uid=<%=rs.getString(15)%>&st=0" style='color:red;'>Un-Block</a>
                                            <%  }   %>                                            
                                        </td>
                                </tr>
                                    <%
                                                i++;
                                            }
                                        } catch (Exception e) {
                                            System.out.println(e);
                                        } finally {
                                        	if(cm != null) {
                                        		cm.close();
                                        	}
                                        }
                                    %>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>
                </section>
                <!-- /.content --> 
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs"> </div>
                <strong>Copyright &copy; 2014-2015 <a href="#">YCH</a>.</strong> All rights reserved. </footer>

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar --> 

        </div>
        <!-- ./wrapper --> 

        <script type="text/javascript" class="init">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": false
                });
            });

        </script>
        <!-- jQuery UI 1.11.4 --> 
        <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> 
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip --> 
        <script>
                        $.widget.bridge('uibutton', $.ui.button);
        </script> 
        <!-- Bootstrap 3.3.5 --> 
        <script src="bootstrap/js/bootstrap.min.js"></script> 
        <!-- Morris.js charts --> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> 
         
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> 
         
        <!-- AdminLTE App --> 
        <script src="ltmcss/js/app.min.js"></script> 
         
        <!-- AdminLTE for demo purposes --> 
        <script src="ltmcss/js/demo.js"></script>
    </body>
</html>
