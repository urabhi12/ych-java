-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2020 at 08:00 AM
-- Server version: 5.5.39
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ych_server`
--

-- --------------------------------------------------------

--
-- Table structure for table `email_master`
--

CREATE TABLE IF NOT EXISTS `email_master` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `set_type` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `emails` varchar(500) NOT NULL,
  `status` varchar(255) NOT NULL,
  `createdate` varchar(255) NOT NULL,
  `modifydate` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `email_master`
--

INSERT INTO `email_master` (`id`, `user_id`, `user_type`, `set_type`, `name`, `emails`, `status`, `createdate`, `modifydate`) VALUES
(2, 2, 'Customer', 'New Export', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '21 Sep 2018 05:42:58 GMT', '21 Sep 2018 05:42:58 GMT'),
(3, 2, 'Customer', 'New Import', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '21 Sep 2018 05:43:11 GMT', '21 Sep 2018 05:43:11 GMT'),
(4, 2, 'Customer', 'Request of Permit', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '21 Sep 2018 05:43:19 GMT', '21 Sep 2018 05:43:19 GMT'),
(5, 2, 'Customer', 'Local Delivery', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '21 Sep 2018 05:43:25 GMT', '21 Sep 2018 05:43:25 GMT'),
(6, 33, 'Customer', 'New Import', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '1 Oct 2018 06:57:23 GMT', '1 Oct 2018 06:57:23 GMT'),
(7, 33, 'Customer', 'New Export', 'Mohit', 'mohit.r@softgentechnologies.com', '1', '1 Oct 2018 06:57:38 GMT', '1 Oct 2018 06:57:38 GMT');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE IF NOT EXISTS `location` (
  `location` varchar(45) NOT NULL,
  `cdate` varchar(45) NOT NULL,
  `mdate` varchar(45) NOT NULL,
`id` bigint(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`location`, `cdate`, `mdate`, `id`) VALUES
('America', '8 Dec 2017 17:02:47 GMT', '8 Dec 2017 17:02:47 GMT', 1),
('Bangladesh', '8 Dec 2017 17:08:05 GMT', '8 Dec 2017 17:08:05 GMT', 2),
('India', '8 Dec 2017 16:57:02 GMT', '9 Dec 2017 07:16:46 GMT', 3),
('Singapore', '1 Feb 2018 05:16:03 GMT', '1 Feb 2018 05:16:03 GMT', 4);

-- --------------------------------------------------------

--
-- Table structure for table `login_access`
--

CREATE TABLE IF NOT EXISTS `login_access` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` text NOT NULL,
  `created_date` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=57 ;

--
-- Dumping data for table `login_access`
--

INSERT INTO `login_access` (`id`, `user_id`, `token`, `created_date`) VALUES
(26, 25, '93360', '3 Nov 2018 10:36:28 GMT'),
(48, 22, '58373', '18 Dec 2018 07:41:35 GMT'),
(49, 2, '52912', '18 Dec 2018 07:41:44 GMT'),
(52, 33, '66008', '13 Apr 2019 06:12:28 GMT'),
(53, 35, '12023', '13 Apr 2019 07:15:04 GMT'),
(55, 1, '51843', '26 Jul 2019 11:38:49 GMT'),
(56, 34, '86045', '26 Jul 2019 11:39:01 GMT');

-- --------------------------------------------------------

--
-- Table structure for table `login_log`
--

CREATE TABLE IF NOT EXISTS `login_log` (
`id` bigint(20) NOT NULL,
  `date` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `login_log`
--

INSERT INTO `login_log` (`id`, `date`, `time`, `user_id`) VALUES
(1, '19/Sep/2018', '17:49:08.603', 22),
(2, '20/Sep/2018', '17:49:47.922', 22),
(3, '21/Sep/2018', '17:57:36.674', 22),
(4, '24/Sep/2018', '10:26:37.795', 22),
(5, '25/Sep/2018', '10:19:41.580', 2),
(6, '28/Sep/2018', '10:52:47.602', 2),
(7, '01/Oct/2018', '11:21:01.928', 2),
(8, '03/Oct/2018', '18:37:08.754', 2),
(9, '04/Oct/2018', '12:13:54.691', 2),
(10, '05/Oct/2018', '18:37:30.694', 2),
(11, '08/Oct/2018', '18:07:35.488', 22),
(12, '12/Oct/2018', '10:27:13.906', 2),
(13, '18/Oct/2018', '12:44:05.702', 2),
(14, '20/Oct/2018', '13:54:08.615', 2),
(15, '22/Oct/2018', '10:51:54.340', 1),
(16, '23/Oct/2018', '14:58:06.338', 2),
(17, '03/Nov/2018', '12:43:04.387', 1),
(18, '05/Nov/2018', '12:04:11.762', 22),
(19, '14/Nov/2018', '10:30:11.814', 2),
(20, '21/Nov/2018', '14:50:41.644', 2),
(21, '23/Nov/2018', '10:40:53.948', 2),
(22, '27/Nov/2018', '10:34:56.528', 2),
(23, '18/Dec/2018', '13:11:35.598', 22),
(24, '13/Apr/2019', '11:17:28.559', 1),
(25, '26/Jul/2019', '17:07:52.364', 1);

-- --------------------------------------------------------

--
-- Table structure for table `reg`
--

CREATE TABLE IF NOT EXISTS `reg` (
  `id` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `cno` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `pwd` varchar(45) NOT NULL,
  `cname` varchar(45) NOT NULL,
  `repname` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `count` varchar(45) NOT NULL,
  `flag` varchar(45) NOT NULL,
  `dt` varchar(45) NOT NULL,
  `location` varchar(45) NOT NULL,
  `cdate` varchar(45) NOT NULL,
  `mdate` varchar(45) NOT NULL,
`tid` bigint(20) unsigned NOT NULL,
  `cust_master_id` varchar(255) NOT NULL,
  `isblocked` int(11) DEFAULT '0',
  `cso` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

--
-- Dumping data for table `reg`
--

INSERT INTO `reg` (`id`, `name`, `cno`, `email`, `pwd`, `cname`, `repname`, `type`, `count`, `flag`, `dt`, `location`, `cdate`, `mdate`, `tid`, `cust_master_id`, `isblocked`, `cso`) VALUES
('Admin1234', 'Admin', '1234567890', 'admin@gmail.com', '1234', '', '', 'Admin', '0', '1', '17:08:49.930', '', '2 Dec 2017 10:28:20 GMT', '17 Jul 2018 17:44:38 GMT', 1, '', 0, ''),
('Customer1234', 'Customer', '1234567890', 'mohit.r@softgentechnologies.com', '12345678', 'IVIPLL', '', 'Customer', '0', '1', '13:11:44.538', 'India', '2 Dec 2017 10:32:58 GMT', '20 Oct 2018 10:50:14 GMT', 2, '', 0, 'Afiza'),
('Dev1234', 'Dev', '1236547890', 'dev@gmail.com', '1236547890', '', '', 'Admin', '0', '0', '16:10:26.571', '', '2 Dec 2017 10:40:26 GMT', '7 Mar 2018 07:39:47 GMT', 3, '', 0, ''),
('mjadmin', 'MJ', '1234567890', 'mjadmin@gmail.com', '1234', '', '', 'Admin', '5', '0', '04:55:17.462', '', '8 Dec 2017 20:54:18 GMT', '7 Mar 2018 07:40:40 GMT', 4, '', 0, ''),
('Shubham1234', 'Shubham', '1234569870', 'shubham@gmail.com', '1234', 'BBPCCL', 'xyz', 'Customer', '0', '0', '16:04:26.637', 'Australia', '2 Dec 2017 10:34:26 GMT', '7 Mar 2018 07:41:08 GMT', 5, '', 0, ''),
('Staff1234', 'Staff Tan Toh Lim', '1234', 'staff@gmail.com', '1234', '', '', 'Staff', '0', '0', '17:09:01.537', '', '2 Dec 2017 10:31:28 GMT', '1 Apr 2018 13:46:24 GMT', 6, '', 0, ''),
('Vinay121', 'VInay', '1472583690', 'vinay@gmail.com', '1234', '', '', 'Admin', '0', '0', '15:59:06.176', '', '2 Dec 2017 10:29:06 GMT', '7 Mar 2018 07:43:38 GMT', 7, '', 0, ''),
('Testing Sub', 'Sub 1', '123', 'abc@gmail.com', '123', 'SGT', 'ABC', 'Customer-Sub', '0', '1', '21:33:00.355', 'India', '20 Jan 2018 13:33:00 GMT', '20 Jan 2018 13:33:21 GMT', 8, 'Customer1234', 0, ''),
('abc12', 'ABC1', '1234567890', 'abc@gmail.com', '1234567890', '', '', 'Customer-Sub', '2', '1', '18:22:36.662', 'India', '20 Jan 2018 13:34:11 GMT', '13 Sep 2018 06:25:07 GMT', 9, 'Customer1234', 0, ''),
('QWE', 'QWE12', '142536', 'QWE@gmail.com', '142536', 'SGT', 'abc', 'Customer-Sub', '0', '1', '21:35:48.196', 'India', '20 Jan 2018 13:35:48 GMT', '29 Jan 2018 06:04:16 GMT', 10, 'Customer1234', 0, ''),
('Angela', 'Angela', '1234567', 'jenny.yong@ych.com', '1234567', 'Valspar Spore', 'Angela', 'Customer-Sub', '0', '1', '15:07:13.096', '#', '27 Jan 2018 07:07:13 GMT', '27 Jan 2018 07:51:37 GMT', 11, 'customer1234', 0, ''),
('Jenny customer', 'Jenny customer', '1234567', 'jenny.yong@ych.com', 'jenny customer', 'Valspar Spore', 'Jenny customer', 'Customer-Sub', '0', '1', '15:52:30.893', '#', '27 Jan 2018 07:52:30 GMT', '27 Jan 2018 07:52:30 GMT', 12, 'customer1234', 0, ''),
('Nelson Ng', 'Nelson Ng', '64172548', 'nelson.ng@ych.com', 'nelson', 'Fonterra', 'Nelson Ng', 'Customer-Sub', '0', '1', '10:17:59.346', 'America', '29 Jan 2018 02:17:59 GMT', '29 Jan 2018 06:04:10 GMT', 13, 'customer1234', 0, ''),
('Nelson  ', 'Nelson Ng', '12345678', 'nelson.ng@ych.com', 'nelson', 'Fonterra', 'Operation dept', 'Customer-Sub', '0', '1', '14:09:50.284', 'India', '29 Jan 2018 06:09:50 GMT', '29 Jan 2018 06:09:50 GMT', 14, 'customer1234', 0, ''),
('aide', 'Samuel Lim Yi Jie', '97640816', 'samuel@aide-technologies.com', '1234', 'AIDE Technologies Pte. Ltd.', '', 'Customer', '0', '1', '14:19:44.998', 'Singapore', '1 Feb 2018 05:15:42 GMT', '6 Mar 2018 23:30:30 GMT', 15, '', 0, ''),
('jensen', 'Jensen Lim', '91231234', 'jensen@aide-technologies.com', '1234', 'AIDE Technologies Pte. Ltd.', '', 'Customer-Sub', '1', '1', '14:16:46.811', 'Singapore', '1 Feb 2018 05:19:05 GMT', '1 Feb 2018 05:19:05 GMT', 16, 'aide', 0, ''),
('Don ', 'Don Toh', '64172426', 'yewchee@ych.com', 'don', 'APBS', 'Don', 'Customer-Sub', '0', '1', '15:17:18.921', 'India', '7 Feb 2018 07:17:18 GMT', '7 Feb 2018 07:17:18 GMT', 17, 'customer1234', 0, ''),
('Eddie', 'Eddie', '64172539', 'chengyong.chua@ych.com', 'eddie', 'NTUC', 'Eddie', 'Customer-Sub', '0', '1', '16:14:04.077', 'India', '7 Feb 2018 08:14:04 GMT', '7 Feb 2018 08:14:04 GMT', 18, 'customer1234', 0, ''),
('michelle', 'michelle', '64172432', 'cheaupok.gaik@ych.com', 'michelle', 'NTUC', 'michelle', 'Customer-Sub', '0', '1', '16:15:59.109', 'India', '7 Feb 2018 08:15:59 GMT', '7 Feb 2018 08:15:59 GMT', 19, 'customer1234', 0, ''),
('Dallen Sinna Raja ', 'Dallen Sinna Raja ', '64172447', 'dallen.s@ych.com', 'dallen', 'YCH APBS', 'YCH APBS', 'Customer-Sub', '0', '1', '11:16:35.047', 'India', '20 Feb 2018 03:16:35 GMT', '20 Feb 2018 03:16:35 GMT', 20, 'customer1234', 0, ''),
('Toh Yew Chee', 'Toh Yew Chee ', '64172426', 'yeechee@ych.com', 'tohyewchee', 'YCH APBS', 'Toh Yew Chee', 'Customer', '0', '1', '11:34:27.344', 'Singapore', '20 Feb 2018 03:34:27 GMT', '6 Mar 2018 23:30:37 GMT', 21, '', 0, ''),
('Afiza', 'Afiza Kim', '64172639', 'mohit.r@softgentechnologies.com', 'afiza', '', '', 'Staff', '0', '1', '13:11:35.598', '', '20 Feb 2018 03:43:43 GMT', '17 Jul 2018 17:45:26 GMT', 22, '', 0, ''),
('Kim', 'Kim', '64172668', 'kim@kemira.com.sg', 'kim', 'Kemira HK', 'Kim', 'Customer', '0', '1', '22:42:13.265', 'Singapore', '27 Feb 2018 14:42:13 GMT', '6 Mar 2018 23:30:34 GMT', 23, '', 0, ''),
('Angelyn ', 'Angelyn', '64172678', 'angelyn@ych.com', 'angelyn', '', '', 'Staff', '0', '1', '22:57:24.265', '', '27 Feb 2018 14:57:24 GMT', '6 Mar 2018 23:30:09 GMT', 24, '', 0, ''),
('YCH-admin', 'YCH Admin Account', '0', 'admin@ych.com.sg', 'admin', '', '', 'Admin', '0', '1', '16:06:28.413', '', '6 Mar 2018 23:29:49 GMT', '6 Mar 2018 23:29:49 GMT', 25, '', 0, ''),
('kerwin', 'Kerwin Lim', '82220812', 'kerwin@aide-technologies.com', '1234', 'Aide', '', 'Customer-Sub', '0', '1', '17:03:24.756', 'Singapore', '19 Mar 2018 23:42:58 GMT', '17 Jul 2018 17:34:01 GMT', 26, 'aide', 0, ''),
('kerwin97', 'kerwin', '82220812', 'kerwin@aide', '1234', 'aide', 'kerwin', 'Customer-Sub', '0', '0', '03:25:15.312', 'Singapore', '6 Jul 2018 19:25:15 GMT', '12 Jul 2018 06:18:32 GMT', 27, 'aide', 0, ''),
('kerwin97', 'kerwin', '82220812', 'kerwin@aide', '123', 'aide', 'kerwin', 'Customer-Sub', '0', '0', '14:19:11.189', 'Singapore', '12 Jul 2018 06:19:11 GMT', '12 Jul 2018 06:20:21 GMT', 28, 'aide', 0, ''),
('abc123', 'ABC 123', '1234567890', 'abc123@gmail.com', '1234567890', '', '', 'Staff', '0', '1', '11:15:07.432', '', '23 Aug 2018 05:45:07 GMT', '23 Aug 2018 05:54:55 GMT', 29, '', 0, ''),
('abc1234', 'ABC 1234', '1234567890', 'abc1234@gmail.com', '123456', '', '', 'Staff', '0', '1', '18:22:41.855', '', '23 Aug 2018 05:46:02 GMT', '23 Aug 2018 05:46:02 GMT', 30, '', 0, ''),
('abc12345', 'abc 12345', '1212121212', 'abc12345@gmail.com', '123456', 'abc 1', 'AB', 'Customer', '0', '1', '11:19:40.275', 'India', '23 Aug 2018 05:49:40 GMT', '23 Aug 2018 05:50:16 GMT', 31, '', 0, ''),
('TestingSub', 'Testing Sub', '1425367485', 'testingsub@gmail.com', '12345678', '', '', 'Customer-Sub', '0', '1', '11:57:11.499', 'India', '13 Sep 2018 07:57:00 GMT', '13 Sep 2018 07:57:00 GMT', 32, 'Customer1234', 0, ''),
('MohitMj', 'Mohit Mj', '1234567890', 'mj@gmail.com', '14253612', 'MJ', '', 'Customer', '0', '1', '11:42:28.268', 'India', '15 Sep 2018 11:05:32 GMT', '15 Sep 2018 12:49:47 GMT', 33, '', 0, 'staff1234'),
('staff1234', 'Staff Testing', '1425367485', 'mohit.r@softgentechnologies.com', '12345678', '', '', 'Staff', '0', '1', '17:09:01.537', '', '15 Sep 2018 12:49:29 GMT', '15 Sep 2018 12:49:29 GMT', 34, '', 0, ''),
('MjSub1', 'Mj Sub One', '1425367485', 'mjsub1@gmail.com', '14253612', '', '', 'Customer-Sub', '0', '1', '12:45:04.782', 'India', '15 Sep 2018 12:51:52 GMT', '15 Sep 2018 12:51:52 GMT', 35, 'MohitMj', 0, ''),
('TestingSub1', 'Testing Sub1', '1425367485', 'testingsub1@gmail.com', '12345678', '', '', 'Customer-Sub', '0', '1', '12:17:32.046', 'India', '17 Sep 2018 06:47:32 GMT', '17 Sep 2018 06:47:32 GMT', 36, 'Customer1234', 0, ''),
('fg', 'fgd', '1425367485', 'kwang_meng.low@se1.bp.com', '12345678', '', '', 'Customer-Sub', '0', '1', '16:11:34.387', 'India', '3 Nov 2018 10:41:34 GMT', '3 Nov 2018 10:41:34 GMT', 37, 'Customer1234', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `req`
--

CREATE TABLE IF NOT EXISTS `req` (
  `id` varchar(45) NOT NULL,
  `uid` varchar(45) NOT NULL,
  `imptype` varchar(45) DEFAULT NULL,
  `da` varchar(45) DEFAULT NULL,
  `cdate` varchar(45) DEFAULT NULL,
  `mdate` varchar(45) DEFAULT NULL,
  `exptype` varchar(45) DEFAULT NULL,
  `fcl` varchar(45) DEFAULT NULL,
  `desport` varchar(45) DEFAULT NULL,
  `placedel` varchar(45) DEFAULT NULL,
  `nocont` varchar(45) DEFAULT NULL,
  `sizecont` varchar(45) DEFAULT NULL,
  `typecont` varchar(45) DEFAULT NULL,
  `bondedtruck` varchar(45) DEFAULT NULL,
  `pertype` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `reqtype` varchar(45) DEFAULT NULL,
  `cmpname` varchar(45) DEFAULT NULL,
  `handby` varchar(45) DEFAULT NULL,
  `file` varchar(45) DEFAULT NULL,
  `pdateto` varchar(45) DEFAULT NULL,
  `pdatefrom` varchar(45) DEFAULT NULL,
  `pudateto` varchar(45) DEFAULT NULL,
  `pudatefrom` varchar(45) DEFAULT NULL,
`tid` bigint(20) NOT NULL,
  `pua` text,
  `dadd` text,
  `tpkg` varchar(45) DEFAULT NULL,
  `twgt` varchar(45) DEFAULT NULL,
  `tnop` varchar(45) DEFAULT NULL,
  `tm3` varchar(45) DEFAULT NULL,
  `pkgd` varchar(45) DEFAULT NULL,
  `sreq` varchar(45) DEFAULT NULL,
  `Expected_Comp` varchar(255) NOT NULL,
  `additional_comment` text NOT NULL,
  `remarks` text NOT NULL,
  `tx3id` varchar(255) NOT NULL,
  `cust_ref_no` varchar(255) NOT NULL,
  `is_send_vcc` int(1) DEFAULT '0',
  `vcc_instru` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=321 ;

--
-- Dumping data for table `req`
--

INSERT INTO `req` (`id`, `uid`, `imptype`, `da`, `cdate`, `mdate`, `exptype`, `fcl`, `desport`, `placedel`, `nocont`, `sizecont`, `typecont`, `bondedtruck`, `pertype`, `status`, `reqtype`, `cmpname`, `handby`, `file`, `pdateto`, `pdatefrom`, `pudateto`, `pudatefrom`, `tid`, `pua`, `dadd`, `tpkg`, `twgt`, `tnop`, `tm3`, `pkgd`, `sreq`, `Expected_Comp`, `additional_comment`, `remarks`, `tx3id`, `cust_ref_no`, `is_send_vcc`, `vcc_instru`) VALUES
('IMP14', 'customer1234', 'Air', '13/01/2018', '13 Dec 2017 13:59:21', '30 May 2018 18:22:09', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'Afiza', 'IMP14.txt', '', '', '', '', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('EXP15', 'customer1234', '', '13/01/2018', '13 Dec 2017 13:59:34', '22 Jan 2018 12:22:46', 'Air', '', 'testing', '', '1', '20''', 'GP', 'Yes', '', 'Accepted', 'New Export', 'IVIPLL', 'staff1234', 'EXP15.txt', '', '23/01/2018', '', '', 2, '', '', '', '', '', '', '', '', '', '', '', 'TX3131241241', '', 0, ''),
('REQPER16', 'customer1234', '', '13/01/2018', '13 Dec 2017 13:59:48', '13 Dec 2017 13:59:48 GMT', '', '', '', '', '', '', '', '', 'Testing Type', 'Pending', 'Request of Permit', 'IVIPLL', '', 'REQPER16.txt', '', '', '', '', 3, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD17', 'customer1234', '', '13/01/2018', '13 Dec 2017 14:00:09', '27 Jan 2018 16:28:18', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', 'LD17.txt', '', '', '', '', 4, 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', 'testing', '', '', '', '', '', 0, ''),
('IMP18', 'customer1234', 'Air', '22/01/2018', '13 Dec 2017 15:49:32', '13 Dec 2017 15:49:32 GMT', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', 'IMP18.jpg', '', '', '', '', 5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP19', 'customer1234', 'Air', '18/01/2018', '14 Dec 2017 18:37:43', '14 Dec 2017 18:37:43 GMT', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', 'IMP19.xls', '', '', '', '', 6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP20', 'customer1234', 'Air', '17/01/2018', '15 Dec 2017 14:21:44', '15 Dec 2017 14:21:44 GMT', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', 'IMP20.pdf', '', '', '', '', 7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP21', 'customer1234', 'Air', '19/01/2018', '19 Dec 2017 11:10:22', '19 Dec 2017 08:45:49 GMT', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP22', 'customer1234', 'Air', '28/01/2018', '27 Dec 2017 11:10:22', '27 Dec 2017 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP23', 'customer1234', 'Sea', '30/01/2018', '27 Dec 2017 11:10:22', '27 Dec 2017 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('EXP24', 'customer1234', '', '30/01/2018', '27 Dec 2017 11:10:22', '27 Jan 2018 16:28:28', 'Air', '', 'hk', '', '1', '20''', 'GP', 'Yes', '', 'Accepted', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 11, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IMP25', 'customer1234', 'Air', '28/01/2018', '27 Dec 2017 11:10:22', '27 Dec 2017 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP26', 'customer1234', 'Air', '28/01/2018', '29 Dec 2017 11:10:22', '07 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '07/01/2018', '07/02/2018', '07/01/2018', '07/02/2018', 13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '07/01/2018', '', '', '', '', 0, ''),
('EXP27', 'customer1234', '', '19/01/2018', '02 Jan 2018 11:10:22', '3 Jan 2018 06:27:22 GMT', 'Air', '', 'Hong Kong', '', '1', '20''', 'GP', 'Yes', '', 'Submitted to TX3', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 14, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EXP28', 'customer1234', '', '31/01/2018', '04 Jan 2018 11:10:22', '04 Jun 2018 21:51:48', 'Air', '', 'Japan', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'afiza', NULL, '', '', '', '', 15, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IMP29', 'Customer1234', 'Air', '31/01/2018', '04 Jan 2018 11:10:22', '04 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 16, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP30', 'customer1234', 'Air', '05/01/2018', '06 Jan 2018 11:10:22', '06 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP31', 'customer1234', 'Air', '05/01/2018', '06 Jan 2018 11:10:22', '06 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP32', 'customer1234', 'Sea', '31/01/2018', '06 Jan 2018 11:10:22', '06 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP33', 'customer1234', 'Air', '08/01/2018', '07 Jan 2018 11:10:22', '07 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 20, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('EXP34', 'customer1234', '', '08/01/2018', '07 Jan 2018 11:10:22', '07 Jan 2018 11:10:22', 'Air', '', 'Testing MJ', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 21, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('REQPER35', 'customer1234', '', '08/01/2018', '07 Jan 2018 11:10:22', '07 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', 'Testing MJ Type', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 22, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD36', 'customer1234', '', '08/01/2018', '07 Jan 2018 11:10:22', '07 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Submitted to TX3', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '07/01/2018', '10/01/2018', '07/01/2018', '10/01/2018', 23, 'MJ Testing', 'MJ Testing', '12', '12', '12', '12', '12', 'MJ Testing', '07/01/2018', '', '', '', '', 0, ''),
('IMP37', 'customer1234', 'Air', '10/01/2018', '08 Jan 2018 11:10:22', '8 Jan 2018 14:32:35 GMT', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 24, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP38', 'customer1234', 'Sea', '12/01/2018', '09 Jan 2018 11:10:22', '8 Jan 2018 16:03:26 GMT', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 25, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('EXP39', 'customer1234', '', '12/01/2018', '09 Jan 2018 11:10:22', '8 Jan 2018 16:40:21 GMT', 'Air', '', '', '', '1', '20''', 'GP', 'Yes', '', 'Rejected', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 26, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IMP40', 'customer1234', 'Air', '09/01/2018', '09 Jan 2018 11:10:22', '9 Jan 2018 13:22:34 GMT', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'IVIPLL', 'staff1234', NULL, '09/01/2018', '09/02/2018', '09/01/2018', '09/02/2018', 27, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '09/01/2018', 'Non-compliance to shipping regulation.', '', '', '', 0, ''),
('EXP41', 'customer1234', '', '09/01/2018', '09 Jan 2018 11:10:22', '09 Jan 2018 11:10:22', 'Air', '', 'testing mj', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '09/01/2018', '09/02/2018', '09/01/2018', '09/02/2018', 28, '', '', '', '', '', '', '', '', '09/02/2018', 'Testing Additional Comment', '', '', '', 0, ''),
('IMP42', 'customer1234', 'Sea', '12/01/2018', '09 Jan 2018 11:10:22', '10 Jan 2018 03:34:49 GMT', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 29, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('EXP43', 'customer1234', '', '12/01/2018', '09 Jan 2018 11:10:22', '09 Jan 2018 11:10:22', 'Air', '', 'Hong Kong', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 30, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EXP44', 'customer1234', '', '15/01/2018', '09 Jan 2018 11:10:22', '09 Jan 2018 11:10:22', 'Sea', 'FCL', 'Yantian', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 31, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EXP45', 'customer1234', '', '11/01/2018', '09 Jan 2018 11:10:22', '09 Jan 2018 11:10:22', 'Cross Border', '', '', 'JOHOR BAHRU', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 32, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD46', 'customer1234', 'Cross Border', '01/10/2018', '09 Jan 2018 11:10:22', '20 Oct 2018 19:41:11', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 33, 'SCC Level 3 Singapore 124444', 'Tampines Hub Greenwich', '(can be optional)', '500', '(this field for what purpose)', '(can be optional)', '(can be optional)', 'to reach Tampines by 4pm', '', '', 'Testing For Body', '', 'testing000', 0, ''),
('IMP47', 'customer1234', 'Sea', '12/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 34, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP48', 'customer1234', 'Sea', '31/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 35, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP49', 'customer1234', 'Air', '12/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 36, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP50', 'customer1234', 'Air', '10/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 37, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP51', 'customer1234', 'Air', '11/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 08:52:59 GMT', '', '', '', '', '', '', '', '', '', 'Submitted to TX3', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 38, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP52', 'customer1234', 'Air', '11/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 08:42:33 GMT', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'IVIPLL', 'staff1234', NULL, '10/01/2018', '10/02/2018', '10/01/2018', '10/02/2018', 39, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '10/01/2018', 'duplicated order from customer vs IMP41.', '', '', '', 0, ''),
('REQPER53', 'customer1234', '', '11/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 08:57:57 GMT', '', '', '', '', '', '', '', '', 'IG PERMIT', 'Accepted', 'Request of Permit', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 40, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD54', 'customer1234', '', '11/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 41, 'dddd', 'ssss', '1 pallet', '500', '1 pallet', '1', '0.03', 'delivery at 4pm', '', '', '', '', '', 0, ''),
('LD55', 'customer1234', '', '10/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 42, 'ghhh', 'kkkk', '1 plt', '500', '1 pallet', '1', '0.3', 'delivery at 4pm', '', '', '', '', '', 0, ''),
('LD56', 'customer1234', '', '11/01/2018', '10 Jan 2018 11:10:22', '10 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 43, 'SCC Level 3 Singapore 124444', 'Tampines Hub Greenwich', '1 pallet', '500', '1 pallet', '1', '0.03', 'delivery at 4pm', '', '', '', '', '', 0, ''),
('REQPER57', 'customer1234', '', '17/01/2018', '11 Jan 2018 11:10:22', '11 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', 'Testing Permit', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 44, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IMP58', 'customer1234', 'Air', '24/01/2018', '11 Jan 2018 11:10:22', '11 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 45, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('REQPER59', 'customer1234', '', '24/01/2018', '11 Jan 2018 11:10:22', '11 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', 'Testing', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 46, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IMP60', 'customer1234', 'Air', '23/01/2018', '11 Jan 2018 11:10:22', '13 Jan 2018 17:01:48', '', '', '', '', '', '', '', '', '', 'Submitted to TX3', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 47, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP61', 'customer1234', 'Air', '12/01/2018', '13 Jan 2018 11:10:22', '13 Jan 2018 21:56:53', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 48, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('IMP62', 'customer1234', 'Air', '30/01/2018', '19 Jan 2018 11:10:22', '19 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 49, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing for remarks', '', '', 0, ''),
('IMP63', 'Customer1234', 'Air', '20/01/2018', '20 Jan 2018 11:10:22', '20 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 50, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing Upload', '', '', 0, ''),
('EXP64', 'Customer1234', '', '20/01/2018', '20 Jan 2018 11:10:22', '20 Jan 2018 11:10:22', 'Cross Border', 'FCL', '', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 51, '', '', '', '', '', '', '', '', '', '', 'Testing Upload', '', '', 0, ''),
('REQPER65', 'Customer1234', '', '20/01/2018', '20 Jan 2018 11:10:22', '20 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', 'Testing', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 52, '', '', '', '', '', '', '', '', '', '', 'Testing Upload', '', '', 0, ''),
('LD66', 'Customer1234', '', '20/01/2018', '20 Jan 2018 11:10:22', '20 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 53, 'Testing Upload', 'Testing Upload', '', '12', '12', '12', '12', '12', '', '', 'Testing Upload', '', '', 0, ''),
('IMP67', 'abc12', 'Air', '20/01/2018', '20 Jan 2018 11:10:22', '22 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'SGT', '', NULL, '', '', '', '', 54, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing Sub Upload', '', '', 0, ''),
('LD68', 'customer1234', '', '24/01/2018', '22 Jan 2018 11:10:22', '22 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 55, 'Toa Payoh 62561169 10am-10pm\r\nBlk490 Lorong 6 Toa Payoh HDB #01-23 S(310490)\r\n\r\nMarine Parade 64404869 10am-10pm/10am-10:30pm(Fri-Sat)\r\nBlk83 Marine Parade Central #01-590 S(440083)\r\n\r\nAng Mo Kio 64591518 10am-10pm\r\nBlk703 Ang Mo Kio Ave8 #01-2543 S(560703)\r\n\r\nBishan 62581729 10am-10pm\r\nBlk513 Bishan Street 13 #01-502 S(570513)\r\n\r\nBugis+ 68846817 10:30am-10:30pm\r\nBugis Plus #01-01 S(188067)\r\n\r\nClementi 67731476 10am-10pm \r\nBlk 443 Clementi Ave3 #01-67 S(120443)\r\n\r\nTampines \r\n67899106 10am-10pm\r\nBlk510 Tampines Central 1 #01-250 S(520510)\r\n\r\nBedok 62461780 10am-10pm\r\n311 New Upper Changi Road Bedok Mall #01-97 S(467360)\r\n\r\nCompass One 68818470 10:30am-10:30pm\r\nCompass One Mall #01-19 S(545078) \r\n\r\nLucky Plaza 67358917 10am-10pm\r\nLucky Plaza #01-15 S(238863)\r\n\r\nJurong East 65690271 10am-10pm\r\n3 Getway Drive #B1-27 Westgate S(608532)', '\r\nYishun10 67547639 10:30am-10:30pm\r\nYishun 10 Multiplex #01-02 S(768794)\r\n\r\nBuona Vista 67795643 10am-10pm\r\nBuona Vista MRT #01-02/03 S(139345)\r\n\r\nMillenia Walk 63369427 11am-9pm(Sun-Thu&PH)/11am-10pm(Fri-Sat)\r\nMillenia Walk Shopping Mall #01-85 S(039596)\r\n\r\nVivo City 62724085 10:30am-10:30pm\r\nVivo City Shopping Mall #B2-06B S(098585)\r\n\r\nPlaza Singapura 62380365 10:30am-10:30pm\r\nPlaza Singapura #01-68 S(238839)\r\n\r\n313@somerset 68359476 10:30am-10:30pm\r\n313@somerset #B3-51 S(238895)\r\n\r\nChangi Airport T3 65432080 10:30am-11pm \r\nChangi Airport #B2-12 S(819663)\r\n\r\nSerangoon Nex 66348623 10:00am-10:00pm(Sun-Thu), 10:00am-10:30pm(Fri-Sat)\r\nSerangoon Nex Mall #B1-07 S(556083)\r\n\r\nCity Square 66349361 10am-10pm\r\nCity Square Mall #B1-K6 S(208539)\r\n\r\nChangi City Point 66360347 11am-10pm\r\n5 Changi Business Park Central 1 #B1-18 S(486038)', '', '100kg', '10', '20', '20'' x 30'' x 45''', 'Nope', '', '', 'Test test', '', '', 0, ''),
('IMP69', 'customer1234', 'Air', '24/01/2018', '24 Jan 2018 11:10:22', '24 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 56, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing 24 jan', '', '', 0, ''),
('LD70', 'customer1234', '', '25/01/2018', '25 Jan 2018 11:10:22', '25 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 57, 'Pickup address 1\r\nSingapore 123123\r\n\r\nPickup address 2\r\nSingapore 123123\r\n\r\nPickup address 5\r\nSingapore 123123\r\n\r\nPickup address 111\r\nSingapore 123123', 'Delivery address 1\r\nSingapore 123123\r\n\r\nDelivery address 2\r\nSingapore 123123\r\n\r\nDelivery address 5\r\nSingapore 123123\r\n\r\nDelivery  address 111\r\nSingapore 123123', '', '', '10', '10', '10', '10', '', '', 'Test', '', '', 0, ''),
('IMP71', 'customer1234', 'Air', '31/01/2018', '25 Jan 2018 11:10:22', '25 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 58, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'will be sending you NOA once available. ', '', '', 0, ''),
('IMP72', 'customer1234', 'Air', '27/01/2018', '25 Jan 2018 11:10:22', '25 Jan 2018 14:40:46', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 59, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cargoes urgently and require Reefer truck', '', '', 0, ''),
('IMP73', 'customer1234', 'Sea', '25/01/2018', '25 Jan 2018 11:10:22', '25 Jan 2018 21:40:22', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 60, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'PLS CLEAR THIS CONTAINER URGENTLY', '', '', 0, ''),
('IMP74', 'customer1234', 'Air', '27/01/2018', '26 Jan 2018 11:10:22', '26 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 61, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cargoes urgently and need Reefer truck', '', '', 0, ''),
('IMP75', 'customer1234', 'Sea', '21/01/2018', '26 Jan 2018 11:10:22', '26 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 62, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'top urgent. vsl had  arrived pls clear container and let me know storage charge', '', '', 0, ''),
('LD76', 'customer1234', '', '29/01/2018', '26 Jan 2018 11:10:22', '26 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 63, '', ' ', '', '', '', '', '', '', '', '', 'Driver must reach customer premises before 3pm', '', '', 0, ''),
('EXP77', 'customer1234', '', '31/01/2018', '27 Jan 2018 11:10:22', '27 Jan 2018 15:37:39', 'Sea', 'FCL', 'Huangpu', '', '1', '20''', 'DP', 'Yes', '', 'Rejected', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 64, '', '', '', '', '', '', '', '', '', '', 'require via shipment to HKG and Form E require', '', '', 0, ''),
('IMP78', 'customer1234', 'Sea', '31/01/2018', '27 Jan 2018 11:10:22', '27 Jan 2018 16:21:38', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 65, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear ccontainer urgently', '', '', 0, ''),
('LD79', 'customer1234', '', '31/01/2018', '27 Jan 2018 11:10:22', '27 Jan 2018 16:29:31', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 66, 'YCH warehouse Bulim avenue ', 'XPO warehouse', '', '', '', '', '', '', '', '', 'time of delivery at 3pm and look for Mr Sim', '', '', 0, ''),
('IMP80', 'customer1234', 'Sea', '30/01/2018', '28 Jan 2018 11:10:22', '28 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 67, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'testing12333444444444444', 'pls truck container urgently on 31 Jan to warehouse', '', '', 0, ''),
('EXP81', 'customer1234', '', '28/01/2018', '28 Jan 2018 11:10:22', '28 Jan 2018 11:10:22', 'Air', 'FCL', '', '', '1', '20''', 'GP', 'Yes', '', 'Accepted', 'New Export', 'IVIPLL', 'customer1234', NULL, '', '', '', '', 68, '', '', '', '', '', '', '', '', '', 'VSL: NANTA BHUM V.121 ETA SIN 31/1 AND ARRIVE HKG 2/2', 'pls provide vsl details asap and require to do FORM D', '', '', 0, ''),
('LD82', 'customer1234', '', '31/01/2018', '29 Jan 2018 11:10:22', '29 Jan 2018 10:45:35', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 69, 'YCH 3RD FLOOR WAREHOUSE  - look for Nelson', 'Abbott warehouse at Greenwich place  ', '', '200', '1 pallet', '1.2', '', '', '', '', 'Need to reach  before 3pm. Look for Ah Seng. testing the line whether can YCH user see the comment.', '', '', 0, ''),
('IMP83', 'customer1234', 'Sea', '31/01/2018', '29 Jan 2018 11:10:22', '29 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 70, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'PO NO. 123456', '', '', 0, ''),
('LD84', 'Nelson  ', '', '31/01/2018', '29 Jan 2018 11:10:22', '29 Jan 2018 16:30:07', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 71, 'YCH warehouse 3rd floor look for Nelson', 'DHL Greenwich look for Mr Loh ', '', '', '', '', '', '', '', 'pls provide invoice sonnest for our permit declaration. testing how many characters show on the customer screen', 'delivery before 3pm', '', '', 0, ''),
('LD85', 'staff1234', '', '31/01/2018', '29 Jan 2018 11:10:22', '29 Jan 2018 17:04:42', '', '', '', '', '', '', '', '', '', 'Rejected', 'Local Delivery', '', 'staff1234', NULL, '', '', '', '', 72, 'YCH warehouse 3rd floor ', 'DHL warehouse at Greenwich', '', '', '', '', '', '', '', 'As per you request thru phone to cancel the order. \r\n i send this data back to you with status : Rejected', 'deliver before 3pm', '', '', 0, ''),
('LD86', 'Nelson  ', '', '31/01/2018', '30 Jan 2018 11:10:22', '30 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 73, 'YCH warehouse 3rd floor look for Mr Sin', 'DHL Greenwich 2nd floor look for Lim', '', '', '', '', '', '', '', 'pls note we can arrange delivery around 3.30pm . pls advise is it ok ?', 'Delivery to DHL before 3pm', '', '', 0, ''),
('LD87', 'Nelson  ', '', '31/01/2018', '30 Jan 2018 11:10:22', '30 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 74, 'YCH warehouse 4th floor ', 'Abbott warehouse at Greenwich ', '', '', '', '', '', '', '', '', 'need to deliver before 3pm', '', '', 0, ''),
('LD88', 'Nelson  ', '', '02/02/2018', '31 Jan 2018 11:10:22', '01 Feb 2018 13:21:54', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 75, 'YCH warehouse 4th floor', 'Abbot warehouse Tanjong Pagar look for Mr Lim', '', '200', '1 pallet', '', '', '', '', '', 'To arrange delivery before 3pm', '', '', 0, ''),
('LD88', 'Nelson  ', '', '02/02/2018', '31 Jan 2018 11:10:22', '31 Jan 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 76, 'YCH warehouse 4th floor', 'Abbot warehouse Tanjong Pagar look for Mr Lim', '', '200', '1 pallet', '', '', '', '', 'pls note we will arrange delivery around 3.30pm. pls confirm is  it ok? ', 'To arrange delivery before 3pm', '', '', 0, ''),
('IMP90', 'customer1234', 'Air', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 20:15:25', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 77, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing mj', '', '', 0, ''),
('EXP91', 'customer1234', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', 'Air', 'FCL', '', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 78, '', '', '', '', '', '', '', '', '', '', 'Testing mj', '', '', 0, ''),
('REQPER92', 'customer1234', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', 'testing mj', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 79, '', '', '', '', '', '', '', '', '', '', 'Testing MJ', '', '', 0, ''),
('LD93', 'customer1234', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 80, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD94', 'customer1234', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 81, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('', 'Nelson  ', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', '', '', NULL, '', '', '', '', 82, 'YCH 4th floor look for mr sim', 'DHL Greenwich', '', '200', '1', '1.2', '', '', '', '', 'delivery before 3pm', '', '', 0, ''),
('LD96', 'Nelson  ', '', '02/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 83, '4th lokyang', 'tampines', '', '200', '1', '1', '', '', '', 'We unable to deliver before 4pm. can we arrange at 2pm', 'deliveyr before 4pm', '', '', 0, ''),
('IMP97', 'customer1234', 'Air', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 84, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing mj', '', '', 0, ''),
('EXP98', 'customer1234', '', '01/02/2018', '01 Feb 2018 11:10:22', '01 Feb 2018 11:10:22', 'Air', 'FCL', 'testing', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 85, '', '', '', '', '', '', '', '', '', '', 'testing mj', '', '', 0, ''),
('IMP99', 'Nelson  ', 'Air', '13/02/2018', '05 Feb 2018 11:10:22', '05 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 86, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pls note we will clear cgo on Wed and deliver to warehouse on the same day', 'pls clear cgo sonnest', 'Adel ', '', 0, ''),
('IMP100', 'Nelson  ', 'Sea', '19/02/2018', '05 Feb 2018 11:10:22', '05 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 87, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls request NOA from carrier and truck in container asap.', '', '', 0, ''),
('EXP101', 'Nelson  ', '', '09/02/2018', '05 Feb 2018 11:10:22', '05 Feb 2018 11:10:22', 'Air', 'FCL', 'Hong Kong', '', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 88, '', '', '', '', '', '', '', '', '', '', 'pls provide flight details asap.', '', '', 0, ''),
('EXP102', 'Nelson  ', '', '09/02/2018', '05 Feb 2018 11:10:22', '05 Feb 2018 11:10:22', 'Sea', 'FCL', 'Hong Kong', '', '1', '20''', 'DP', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 89, '', '', '', '', '', '', '', '', '', '', 'testing', '', '', 0, ''),
('LD103', 'nelson', '', '23/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 90, 'Address 1\r\nAddress 2\r\nAddress 3', 'Address 4\r\nAddress 5\r\nAddress 6\r\nAddress 6', '', '10', '10', '10', '10', '10', '', '', 'Remarks test test', '', '', 0, ''),
('IMP104', 'staff1234', 'Air', '09/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 13:41:52', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', '', 'staff1234', NULL, '', '', '', '', 91, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pls note vsl eetails', 'pls clear cgo urgently', '', '', 0, ''),
('IMP105', 'staff1234', 'Sea', '08/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', '', '', NULL, '', '', '', '', 92, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'urgently ', '', '', 0, ''),
('EXP106', 'staff1234', '', '09/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', 'Air', 'FCL', 'Hong Kong', '', '', '', '', 'Yes', '', 'Pending', 'New Export', '', '', NULL, '', '', '', '', 93, '', '', '', '', '', '', '', '', '', 'dddd', 'require reefer truck', '', '', 0, ''),
('LD107', 'Nelson  ', '', '16/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 94, 'ddd', 'ddd', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LD108', 'Don', '', '08/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'APBS', '', NULL, '', '', '', '', 95, 'JLN AHMAD IBRAHIM', 'NO:8 BULIM AVE LEVEL 2 WHS', '', '1400kg', '100 empty pallets', '', '', 'LAST TIMING @1600HRS', '', 'Hi don,\r\nNoted will arrange accordingly', 'APB DEPT  SG1802LFR05579', '', '', 0, ''),
('LD109', 'Don', '', '08/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'APBS', '', NULL, '', '', '', '', 96, '', '', '', '13.8', '1 ctn', '', '', 'before 12 noon', '', 'NOTED.', '', '', '', 0, ''),
('LD110', 'customer1234', '', '15/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 16:48:42', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 97, '', '', '', '', '', '', '', '', '', 'customer / Ops can type information here', 'testing for local', '', '', 0, ''),
('LD110', 'customer1234', '', '08/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 16:49:47', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 98, 'R-logic International Pte Ltd\r\n3 Ubi Ave 3, #04-1', 'Seagate Technology\r\n22 Changi South Ave 2', '', '229', '1 PLT', '1.208', '', '', '', '', '', '', '', 0, ''),
('EXP112', 'customer1234', '', '15/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', 'Cross Border', 'FCL', '', 'JOHOR BAHRU', '1', '20''', 'GP', 'No', '', 'Accepted', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 99, '', '', '', '', '', '', '', '', '', 'had booked truck no. YCL1234  leaving sin 8/2 and arrive JB 9/2 afternoon', 'testing ', '', '', 0, ''),
('EXP113', 'customer1234', '', '15/02/2018', '07 Feb 2018 11:10:22', '07 Feb 2018 11:10:22', 'Sea', 'LCL', 'Hong Kong', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 100, '', '', '', '', '', '', '', '', '', '', 'UN1263 DG 3 pls advise DG approval asap.', '', '', 0, ''),
('LD114', 'Don', '', '09/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 10:39:57', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'APBS', 'staff1234', NULL, '', '', '', '', 101, '', 'APB WHS', '', '', '10 plts', '', '', '', '', '', 'MORNING', '', '', 0, ''),
('IMP115', 'Don', 'Sea', '12/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'APBS', '', NULL, '', '', '', '', 102, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'YBLA7980,YBLA7981', '', '', 0, ''),
('LD116', 'nelson', '', '09/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 15:35:41', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 103, 'YCH warehouse', 'tampines', '', '', '', '', '', '', '', 'delivery cannot be done before 4pm', 'pls arrange before 4pm', '', '', 0, ''),
('LD117', 'nelson', '', '09/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 104, 'Supply Chain City\r\n8 Bulim Avenue, R3\r\nSingapore 648166\r\n', 'NTUC Thomson Warehouse', '', '', '61', '', '', 'Delivery 12 to 3pm ONLY', '', 'attached permit', '51 pallet full, 10 pallets loose\r\n017/2018', '', '', 0, ''),
('LD118', 'nelson', '', '09/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 15:51:27', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 105, '8 Bulim Avenue', 'NTUC FP Hub Joo Koon', '', '', '', '', '', 'Delivery between 12pm to 3pm', '', '', 'NOTE: \r\n\r\nDC73 advise Daily delivery for wine is between 12.00nn to 15.00pm, if there is any urgent request to deliver earlier will inform accordingly.\r\n\r\n\r\n\r\n\r\nON HOLD : SKU 13085784 KING RNR   LTD ANN ED CAB-SAU   750ML (672EA, Inv 90076808, cont# SEGU1959299 , 20FT, ETA 15.09.17) ? Limited edition 10th Anniversary.\r\n                   Current stocks at DC73 is with description of Limited edition 9th Anniversary\r\n\r\n\r\nYCH To reply by daily not latest by 14.00pm before FP to proceed with STO.\r\n\r\n\r\nDo keep us update once FP started to withdraw Classic Series 1948 for the following SKU. (1st ETA 14.08.2017)\r\n\r\nSKU	Descriptions	BARCODE	Previously known as\r\n11690270	MAIPO      BV SAUA BLANC CHARD  750ML 	7804320063027	10096198 - MPO BLEND S.BLC/CHAR ST 750 12U 2016 GLO\r\nOne carton loose due to damaged, pending for transfer to DC73\r\n\r\n\r\n\r\nNote: To confirm on loose pallet quantity if any.\r\n\r\n\r\nNOTE: \r\n1)               With effective from 14.07.2016, YCH to combine loose pallet into one with SAME SKU, SAME VINTAGE,SAME PACK SIZE\r\n2)               YCH to inform on number of loose pallet upon receive of withdrawal request\r\n3)               To ensure stacking in the correct method to avoid stocks being reject by warehouse (DC73)\r\n4)               Loose pallet is based on last batch of same lot number\r\n\r\n\r\n\r\nDelivery Schedule slot book for THOMSON Warehouse : \r\n\r\n\r\nDelivery : 12.00 ~ 15.00HR\r\n\r\nWarehouse receiving cut-off at 16.00pm Mon - Fri\r\n\r\nNote: \r\n\r\n1)     Please take note that in future any deliveries found with double stack wines inside the container.\r\n\r\nWarehouse personal got the right to reject as this might cause the risk of fall of the goods when delivery man pull out using pallet jack and get injury.\r\n\r\n2)     Ensure all items deliver are tally with DO.\r\n\r\n3)     Note warehouse last receiving timing by 16.00pm (Mon to Fri). \r\n\r\n4)     YCH to inform FP in advance on the delivery and timing if is more than one truck/trip.\r\n\r\nSeek your understanding and co-operation.\r\n\r\nThank you.\r\n', '', '', 0, ''),
('REQPER119', 'Don', '', '09/02/2018', '08 Feb 2018 11:10:22', '08 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', 'DP PERMIT', 'Pending', 'Request of Permit', 'APBS', '', NULL, '', '', '', '', 106, '', '', '', '', '', '', '', '', '', '', 'New Order PO1262', '', '', 0, ''),
('LD120', 'Nelson', '', '09/02/2018', '09 Feb 2018 11:10:22', '09 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 107, '8 BULIM AVENUE, SINGAPORE 648166', 'ABBOTT MANUFACTURING SINGAPORE PRIVATE LIMITED, 26 TUAS SOUTH AVENUE 10, SINGAPORE 637437', '', '25200 KG', '18 Pallets / 36 BBG', '', '', '', '', 'NOTED.', 'SG1802LFR05517 - Testing Purpose', '', '', 0, ''),
('LD121', 'Don', '', '10/02/2018', '09 Feb 2018 11:10:22', '09 Feb 2018 11:01:30', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'APBS', 'staff1234', NULL, '', '', '', '', 108, 'APBS WHS', 'SCC LEVEL R2', '', '1400', '100 WOODEN EMPT PLT', '', '', 'MORNING TRIP', '', 'Noted.', 'WOODEN PLT FOR IMPORT RESTACKING', '', '', 0, ''),
('REQPER122', 'Don', '', '12/2/2018', '09 Feb 2018 11:10:22', '09 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', 'DP permit', 'Pending', 'Request of Permit', 'APBS', '', NULL, '', '', '', '', 109, '', '', '', '', '', '', '', '', '', '', 'SELF COLLECTION AT SCC R2 WHS', '', '', 0, ''),
('IMP123', 'Don', 'Sea', '21/02/2018', '09 Feb 2018 11:10:22', '09 Feb 2018 11:10:22', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'APBS', '', NULL, '', '', '', '', 110, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '17000210 1&2\r\n17000218\r\n17000217 1 & 2', '', '', 0, ''),
('IMP124', 'customer1234', 'Air', '10/02/2018', '10 Feb 2018 16:43:32', '10 Feb 2018 16:43:32', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 111, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'fsdf', '', 'testing 1', 0, ''),
('IFS125', 'customer1234', 'Sea', '10/02/2018', '10 Feb 2018 16:46:59', '10 Feb 2018 16:46:59', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 112, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'test cust1', 0, ''),
('ILS126', 'customer1234', 'Cross Border', '10/02/2018', '10 Feb 2018 16:47:13', '11 Feb 2018 23:44:08', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 113, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pls note cargoes damaged at KD CFS warehouse', 'sdfd', '', 'test cust2', 0, ''),
('EFS127', 'customer1234', '', '10/02/2018', '10 Feb 2018 16:47:28', '10 Feb 2018 16:47:28', 'Sea', 'FCL', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 114, '', '', '', '', '', '', '', '', '', '', 'xgfdg', '', 'test cust3', 0, ''),
('ELS128', 'customer1234', '', '10/02/2018', '10 Feb 2018 16:47:41', '10 Feb 2018 16:47:41', 'Sea', 'LCL', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 115, '', '', '', '', '', '', '', '', '', '', '', '', 'test cust4', 0, ''),
('LD129', 'customer1234', '', '10/02/2018', '10 Feb 2018 16:47:55', '10 Feb 2018 16:47:55', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 116, '', '', '', '', '', '', '', '', '', '', '', '', 'test cust5', 0, ''),
('IMP130', 'nelson', 'Air', '22/02/2018', '11 Feb 2018 23:13:44', '11 Feb 2018 23:28:28', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 117, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pls note will truck contr to warehouse on 30/2', 'attached docs fya, pls advise when truck contr to warehouse', '', 'PO. 12345 TEST', 0, ''),
('ILS131', 'Nelson  ', 'Sea', '22/02/2018', '11 Feb 2018 23:37:43', '15 Sep 2018 13:02:15', '', 'LCL', '', '', '', '', '', '', '', 'Rejected', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 118, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '<p>urgent clearance pls check with co-loader how much is the urgent unstuffing. i need cargoes on friday.</p>\r\n', 'txt_reg', 'order no. 1344 test', 0, ''),
('ILS132', 'Nelson  ', 'Sea', '15/02/2018', '11 Feb 2018 23:38:37', '11 Feb 2018 23:43:07', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 119, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'need urgent clearance ', '', 'order no. 1344 test', 0, ''),
('EFS133', 'nelson', '', '22/02/2018', '11 Feb 2018 23:48:13', '11 Feb 2018 23:48:13', 'Sea', 'FCL', 'Hong Kong', '', '1', '20''', 'DG', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 120, '', '', '', '', '', '', '', '', '', '', 'pls advise vsl details asap. let me know once DG approve', '', 'order no. 1344 test', 0, ''),
('ELA134', 'nelson', '', '28/02/2018', '11 Feb 2018 23:49:59', '11 Feb 2018 23:49:59', 'Air', '', 'h', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 121, '', '', '', '', '', '', '', '', '', '', 'pls advise flight details', '', '', 0, ''),
('CB135', 'nelson', '', '27/02/2018', '11 Feb 2018 23:51:46', '11 Feb 2018 23:51:46', 'Cross Border', 'FCL', '', 'penang', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 122, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IFS136', 'nelson', 'Sea', '13/02/2018', '12 Feb 2018 10:32:19', '12 Feb 2018 10:32:19', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 123, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', ' ', 0, ''),
('ELS137', 'nelson', '', '22/02/2018', '12 Feb 2018 10:33:10', '12 Feb 2018 10:33:10', 'Sea', 'LCL', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 124, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EFS138', 'customer1234', '', '04/04/2018', '12 Feb 2018 14:13:26', '12 Feb 2018 14:13:26', 'Sea', 'FCL', 'Bangkok', '', '3', '40''', 'DG', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 125, '', '', '', '', '', '', '', '', '', '', 'Test test', '', 'REF123123', 0, ''),
('IMP139', 'customer1234', 'Air', '27/02/2018', '12 Feb 2018 14:14:06', '12 Feb 2018 14:14:06', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 126, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Test test test', '', 'REF123123123', 0, ''),
('IMP140', 'customer1234', 'Air', '11/02/2018', '12 Feb 2018 14:14:37', '12 Feb 2018 14:14:37', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 127, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Hello', '', 'REF123123123', 0, ''),
('LD141', 'nelson', '', '16/02/2018', '14 Feb 2018 09:55:30', '14 Feb 2018 10:20:35', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 128, 'Fonterra warehouse level 4', 'Bukit Timah', '', '200', '1 pallet', '1.5', '120x120x120', 'delivery at 4pm', '', 'the driver will reach around 3pm', 'need open truck', '', 'PO. 12345 TEST', 0, ''),
('LD142', 'Don', '', '20/02/2018', '14 Feb 2018 11:59:46', '14 Feb 2018 11:59:46', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'APBS', '', NULL, '', '', '', '', 129, 'scc & CP whs', 'apbs whs', '', '200', '2 pallets', '', '', 'before 10.00am', '', '', '', '', '', 0, ''),
('LD143', 'nelson', '', '21/02/2018', '15 Feb 2018 00:17:35', '15 Feb 2018 00:17:35', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 130, 'YCH warehouse 4th floor', 'Paya Lebar', '', '200', '1 pallet', '1.2', '', 'delivery before 4pm 12344444444444444', '', '', '', '', '', 0, ''),
('ILS144', 'nelson', 'Sea', '28/02/2018', '15 Feb 2018 00:21:44', '15 Feb 2018 00:21:44', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 131, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cargoes urgently', '', 'PO. 12345 TEST', 0, ''),
('IFS145', 'nelson', 'Sea', '28/02/2018', '15 Feb 2018 00:24:25', '15 Feb 2018 00:24:25', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 132, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear ddddd', '', 'BL no.123444', 0, ''),
('IMP146', 'nelson', 'Air', '21/02/2018', '15 Feb 2018 00:25:15', '15 Feb 2018 00:25:15', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 133, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'PLS USE REEFER TRUCK', '', '', 0, ''),
('ELA147', 'nelson', '', '19/02/2018', '15 Feb 2018 00:29:22', '15 Feb 2018 00:29:22', 'Air', '', 'Hong Kong', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 134, '', '', '', '', '', '', '', '', '', '', 'Pls advise flight details sonnest.', '', '12333', 0, ''),
('EFS148', 'nelson', '', '28/02/2018', '15 Feb 2018 00:31:26', '15 Feb 2018 00:31:26', 'Sea', 'FCL', 'NHAVA SHEVA', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 135, '', '', '', '', '', '', '', '', '', '', 'PLS BOOK 1X20DG & 2X20RF CONTAINER AND ADVISE VSL DETAILS ASAP.', '', '1234 TESTING', 0, ''),
('ELS149', 'nelson', '', '26/02/2018', '15 Feb 2018 00:32:55', '15 Feb 2018 00:32:55', 'Sea', 'LCL', 'SHANGHAI', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 136, '', '', '', '', '', '', '', '', '', '', 'request FORM D and provide vsl details asap', '', '', 0, ''),
('CB150', 'nelson', '', '19/02/2018', '15 Feb 2018 00:34:33', '15 Feb 2018 00:34:33', 'Cross Border', 'FCL', '', 'JOHOR BAHRU', '1', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 137, '', '', '', '', '', '', '', '', '', '', '', '', 'TEST', 0, ''),
('CB151', 'nelson', '', '27/02/2018', '15 Feb 2018 00:38:44', '15 Feb 2018 00:38:44', 'Cross Border', 'LCL', '', 'penang', '', '', '', 'No', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 138, '', '', '', '', '', '', '', '', '', '', 'require reefer truck', '', '', 0, ''),
('IFS152', 'nelson', 'Cross Border', '26/02/2018', '15 Feb 2018 00:40:28', '15 Feb 2018 00:40:28', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 139, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'PLS SEND PERMIT TO ME ONCE AVAILABLE.', '', 'PO. 12345 TEST', 0, ''),
('ILS153', 'nelson', 'Cross Border', '24/02/2018', '15 Feb 2018 00:42:52', '15 Feb 2018 00:57:52', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 140, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'will send you once permit ready.', 'need permit urgently', '', '', 0, ''),
('ELR154', 'nelson', '', '28/02/2018', '19 Feb 2018 16:26:48', '19 Feb 2018 16:26:48', 'Cross Border', 'LCL', '', 'penang', '', '', '', 'No', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 141, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EFR155', 'nelson', '', '27/02/2018', '19 Feb 2018 16:27:38', '19 Feb 2018 16:27:38', 'Cross Border', 'FCL', '', '', '2', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 142, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('IFR156', 'nelson', 'Cross Border', '26/02/2018', '19 Feb 2018 16:28:38', '19 Feb 2018 16:28:38', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 143, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, ''),
('ILR157', 'nelson', 'Cross Border', '19/02/2018', '19 Feb 2018 16:29:23', '20 Feb 2018 13:59:37', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 144, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'Adel ', '', 0, ''),
('LLR158', 'nelson', '', '28/02/2018', '20 Feb 2018 13:12:59', '20 Feb 2018 13:56:53', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 145, 'YCH ABBOTT WAREHOUSE', 'DHL TAMPINES', '', '100', '1 pallet', '1.3', '', 'delivery before 4pm', '', '', '', '', 'testing 123', 0, '');
INSERT INTO `req` (`id`, `uid`, `imptype`, `da`, `cdate`, `mdate`, `exptype`, `fcl`, `desport`, `placedel`, `nocont`, `sizecont`, `typecont`, `bondedtruck`, `pertype`, `status`, `reqtype`, `cmpname`, `handby`, `file`, `pdateto`, `pdatefrom`, `pudateto`, `pudatefrom`, `tid`, `pua`, `dadd`, `tpkg`, `twgt`, `tnop`, `tm3`, `pkgd`, `sreq`, `Expected_Comp`, `additional_comment`, `remarks`, `tx3id`, `cust_ref_no`, `is_send_vcc`, `vcc_instru`) VALUES
('REQPER159', 'nelson', '', '26/02/2018', '22 Feb 2018 10:08:13', '22 Feb 2018 10:08:13', '', '', '', '', '', '', '', '', 'IG PERMIT', 'Pending', 'Request of Permit', 'Fonterra', '', NULL, '', '', '', '', 146, '', '', '', '', '', '', '', '', '', '', 'REQUEST PERMIT BY 23/2', '', 'testing 123', 0, ''),
('IFS160', 'nelson', 'Sea', '27/02/2018', '22 Feb 2018 10:37:34', '22 Feb 2018 10:56:39', '', 'FCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'nelson', NULL, '', '', '', '', 147, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'YCH JOB NO. SG1802IFS12345  noted your request of urgent trucking', 'pls clear container urgently', 'SG1802IFS12345', 'DN 1234', 0, ''),
('EFS161', 'nelson', '', '09/02/2018', '22 Feb 2018 11:11:50', '22 Feb 2018 11:18:46', 'Sea', 'FCL', '', '', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'Fonterra', 'nelson', NULL, '', '', '', '', 148, '', '', '', '', '', '', '', '', '', 'noted.', 'attached summary excel file for booking', '', '', 0, ''),
('IFS162', 'customer1234', 'Sea', '21/02/2018', '22 Feb 2018 16:45:35', '22 Feb 2018 16:48:55', '', 'FCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 149, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'APLU', 0, ''),
('ILA163', 'nelson', 'Air', '', '25 Feb 2018 14:04:09', '25 Feb 2018 14:04:09', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 150, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'PO. 12345 TEST', 0, ''),
('IFS164', 'nelson', 'Sea', '', '25 Feb 2018 14:06:17', '25 Feb 2018 14:06:17', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 151, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'PLS TRUCK CONTAINER URGENTLY', '', '', 0, ''),
('ILS165', 'nelson', 'Sea', '', '25 Feb 2018 14:07:06', '25 Feb 2018 16:47:58', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 152, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'TEST', '', 'TESST', 0, ''),
('IFR166', 'nelson', 'Cross Border', '', '25 Feb 2018 14:09:18', '25 Feb 2018 14:09:18', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 153, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'need 1x20 FT TRUCK', '', 'testing 123', 0, ''),
('ILR167', 'nelson', 'Cross Border', '28/02/2018', '25 Feb 2018 14:10:53', '25 Feb 2018 14:10:53', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 154, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'ddddd', '', 'testing 123', 0, ''),
('ELA168', 'nelson', '', '28/02/2018', '25 Feb 2018 14:12:30', '25 Feb 2018 14:12:30', 'Air', '', 'Hong Kong', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 155, '', '', '', '', '', '', '', '', '', '', 'pls book morning flight', '', 'testing 123', 0, ''),
('EFS169', 'nelson', '', '28/02/2018', '25 Feb 2018 14:14:23', '25 Feb 2018 14:14:23', 'Sea', 'FCL', 'SHANGHAI', '', '3', '20''', 'DG', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 156, '', '', '', '', '', '', '', '', '', '', 'pls advise DG approval asap', '', 'PO. 12345 TEST', 0, ''),
('ELS170', 'nelson', '', '03/03/2018', '25 Feb 2018 14:15:31', '25 Feb 2018 14:15:31', 'Sea', 'LCL', 'Bangkok TPT PORT', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'Fonterra', '', NULL, '', '', '', '', 157, '', '', '', '', '', '', '', '', '', '', '', '', 'TESST', 0, ''),
('LLR171', 'nelson', '', '28/02/2018', '25 Feb 2018 14:19:45', '25 Feb 2018 14:19:45', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 158, '4th floor Fonterra warehouse', 'DHL Tampines', '', '100', '2 pallets (24 ctns)', '1.2', '', 'dddd', '', '', 'need to deliver before 3pm', '', 'testing 123', 0, ''),
('LLR172', 'nelson', '', '28/02/2018', '25 Feb 2018 14:21:42', '25 Feb 2018 14:21:42', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'Fonterra', '', NULL, '', '', '', '', 159, '4th floor Fonterra warehouse', 'DHL Tampines look for Mr Sim', '', '100', '2 pallets (24 ctns)', '1.2', '', '', '', '', 'delivery before 3pm', '', 'testing 123', 0, ''),
('REQPER173', 'nelson', '', '28/02/2018', '25 Feb 2018 14:23:26', '25 Feb 2018 16:45:17', '', '', '', '', '', '', '', '', 'OO PERMIT', 'Accepted', 'Request of Permit', 'Fonterra', 'staff1234', NULL, '', '', '', '', 160, '', '', '', '', '', '', '', '', '', '', 'PLS SCAN ME THE PERMIT ONCE READY', '', 'testing 123', 0, ''),
('ILA174', 'nelson ', 'Air', '', '25 Feb 2018 16:09:11', '25 Feb 2018 16:09:11', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', '', NULL, '', '', '', '', 161, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cargo sonnest & require reefer truck', '', 'testing 123', 0, ''),
('IFS175', 'nelson ', 'Sea', '28/02/2018', '25 Feb 2018 16:10:06', '28 Feb 2018 09:48:25', '', 'FCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 162, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'ddddd', '', 'testing 123', 0, ''),
('ILS176', 'nelson ', 'Sea', '', '25 Feb 2018 16:10:50', '26 Feb 2018 11:44:22', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 163, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'dddd', '', 'tes', 0, ''),
('IFR177', 'nelson ', 'Cross Border', '28/02/2018', '25 Feb 2018 16:11:37', '26 Feb 2018 11:43:54', '', 'FCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 164, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'dddd', '', 'testing 123', 0, ''),
('ILR178', 'nelson ', 'Cross Border', '28/02/2018', '25 Feb 2018 16:12:18', '26 Feb 2018 11:44:02', '', 'LCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 165, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'dddd', '', 'dddd', 0, ''),
('ELA179', 'nelson ', '', '28/02/2018', '25 Feb 2018 16:13:25', '26 Feb 2018 11:28:57', 'Air', '', 'Hong Kong', '', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'Fonterra', 'staff1234', NULL, '', '', '', '', 166, '', '', '', '', '', '', '', '', '', 'pls note flight arriving Saturday and will clear cargoes on Monday for delivery before 5pm', 'pls provide flight details asap', '', 'ddd', 0, ''),
('EFS180', 'nelson ', '', '28/02/2018', '25 Feb 2018 16:15:14', '25 Feb 2018 16:47:21', 'Sea', 'FCL', 'SHANGHAI', '', '1', '20''', 'DG', 'Yes', '', 'Accepted', 'New Export', 'Fonterra', 'staff1234', NULL, '', '', '', '', 167, '', '', '', '', '', '', '', '', '', '', 'and 1x40 GP ', '', '', 0, ''),
('ELS181', 'nelson ', '', '10/03/2018', '25 Feb 2018 16:16:14', '25 Feb 2018 16:47:16', 'Sea', 'LCL', 'Bangkok TPT PORT', '', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'Fonterra', 'staff1234', NULL, '', '', '', '', 168, '', '', '', '', '', '', '', '', '', '', 'test', '', 'dddd', 0, ''),
('EFR182', 'nelson ', '', '09/03/2018', '25 Feb 2018 16:17:37', '25 Feb 2018 16:45:21', 'Cross Border', 'FCL', '', 'JOHOR BAHRU', '1', '40''', 'GP', 'No', '', 'Accepted', 'New Export', 'Fonterra', 'staff1234', NULL, '', '', '', '', 169, '', '', '', '', '', '', '', '', '', '', 'tet', '', 'dddd', 0, ''),
('ELR183', 'nelson ', '', '27/02/2018', '25 Feb 2018 16:18:51', '25 Feb 2018 16:44:47', 'Cross Border', 'LCL', '', 'JOHOR BAHRU', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'Fonterra', 'staff1234', NULL, '', '', '', '', 170, '', '', '', '', '', '', '', '', '', '', 'g', '', 'dddd', 0, ''),
('REQPER184', 'nelson ', '', '28/02/2018', '25 Feb 2018 16:23:55', '25 Feb 2018 16:43:15', '', '', '', '', '', '', '', '', 'OO PERMIT', 'Accepted', 'Request of Permit', 'Fonterra', 'staff1234', NULL, '', '', '', '', 171, '', '', '', '', '', '', '', '', '', '', 'pls send permit once ready', 'ELR123445', 'testing 123', 0, ''),
('LLR185', 'nelson ', '', '28/02/2018', '25 Feb 2018 16:26:11', '25 Feb 2018 16:43:49', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'Fonterra', 'staff1234', NULL, '', '', '', '', 172, 'YCH warehouse 4th floor look for Nelson', 'DHL TAMPINES', '', '200', '2 pallets (24 ctns)', '1.2', '', '', '', '', 'need open truck', 'LLR12345', 'testing 123', 0, ''),
('ILA186', 'Nelson  ', 'Air', '', '26 Feb 2018 16:35:43', '27 Feb 2018 22:59:06', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'angelyn', NULL, '', '', '', '', 173, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'ddd', '', 'dd', 0, ''),
('ILA187', 'Nelson  ', 'Air', '28/02/2018', '26 Feb 2018 16:36:14', '26 Feb 2018 16:42:42', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Fonterra', 'staff1234', NULL, '', '', '', '', 174, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pending invoice ', 'dddd', 'ifs12344', 'TESST', 0, ''),
('ELA188', 'customer1234', '', '28/02/2018', '27 Feb 2018 10:38:28', '27 Feb 2018 10:43:11', 'Air', '', 'hong komg', '', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 175, '', '', '', '', '', '', '', '', '', 'wil l provide flight details asap', 'ddd', 'susan', '123333', 0, ''),
('IFS189', 'customer1234', 'Sea', '28/02/2018', '27 Feb 2018 10:45:50', '27 Feb 2018 10:53:28', '', 'FCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 176, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'angelyn', 'poii', 0, ''),
('ILA190', 'staff1234', 'Air', '01/03/2018', '27 Feb 2018 11:01:56', '27 Feb 2018 11:03:52', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', '', 'staff1234', NULL, '', '', '', '', 177, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'hhhhh', 'nil', 'angelyn', 'ifs456', 0, ''),
('ILA191', 'kim', 'Air', '28/02/2018', '27 Feb 2018 22:43:34', '27 Feb 2018 22:49:20', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Kemira HK', 'staff1234', NULL, '', '', '', '', 178, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'Pls note will arrange delivery on Tuesday', '', '', 'PO. 12345 TEST', 0, ''),
('ILA192', 'admin1234', 'Air', '28/02/2018', '28 Feb 2018 14:14:14', '28 Feb 2018 14:14:14', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', '', '', NULL, '', '', '', '', 179, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cargo urgently', '', 'testing 123', 0, ''),
('ILA192', 'admin1234', 'Air', '28/02/2018', '28 Feb 2018 14:14:17', '28 Feb 2018 14:23:44', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', '', 'admin1234', NULL, '', '', '', '', 180, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'pls provid invoice sonnest', 'pls clear cargo urgently', 'Angelyn', 'testing 123', 0, ''),
('ILA194', 'customer1234', 'Air', '02/03/2018', '01 Mar 2018 14:10:27', '01 Mar 2018 14:21:28', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'staff1234', NULL, '', '', '', '', 181, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'ghjjkkkk', '', 'Susan', 'HAP - P&C - Estée Lauder Companies - Shipping Documents - ETA 28.02.2018 - International ', 0, ''),
('EFS195', 'customer1234', '', '', '01 Mar 2018 14:25:24', '01 Mar 2018 14:25:24', 'Sea', 'FCL', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 182, '', '', '', '', '', '', '', '', '', '', '1X20gp & 1 X40rF', '', '', 0, ''),
('ILA196', 'Customer1234', 'Air', '07/03/2018', '07 Mar 2018 15:45:04', '20 Mar 2018 07:31:53', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Staff1234', NULL, '', '', '', '', 183, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing ', 'Afiza', 'mj testing', 0, ''),
('ILA197', 'Customer1234', 'Air', '07/03/2018', '07 Mar 2018 15:46:43', '19 Mar 2018 22:04:45', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'Staff1234', NULL, '', '', '', '', 197, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing again', 'Afiza', 'mj testing again', 0, ''),
('IFR198', 'kerwin', 'Cross Border', '22/03/2018', '23 Mar 2018 23:16:12', '25 Mar 2018 18:11:21', '', 'FCL', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Aide', 'Staff1234', NULL, '25/03/2018', '26/03/2018', '22/03/2018', '23/03/2018', 198, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24/03/2018', 'TEST TX3ID AND PERMIT', 'TEST01', 'KER01TX3ID', 'KER01', 0, ''),
('EFS199', 'kerwin', '', '22/03/2018', '25 Mar 2018 18:51:56', '30 May 2018 18:20:29', 'Sea', 'FCL', 'SEANGAPORE', '', '3', '20''', 'HC', 'Yes', '', 'Accepted', 'New Export', 'Aide', 'Afiza', NULL, '', '', '', '', 199, '', '', '', '', '', '', '', '', '', '', 'TEST', '', 'KER02', 0, ''),
('REQPER200', 'kerwin', '', '22/03/2018', '25 Mar 2018 19:08:39', '30 May 2018 18:20:11', '', '', '', '', '', '', '', '', 'FREE PERMIT', 'Accepted', 'Request of Permit', 'Aide', 'Afiza', NULL, '', '', '', '', 200, '', '', '', '', '', '', '', '', '', 'test', 'TEST', '', 'KER03', 0, ''),
('ILA201', 'don', 'Air', '05/04/2018', '03 Apr 2018 15:36:06', '10 May 2018 02:43:04', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'APBS', 'admin1234', NULL, '', '', '', '', 201, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'pls clear cgo urgently', '', 'PO. 12345 TEST', 0, ''),
('LLR202', 'aide', '', '01/05/2018', '30 May 2018 17:53:05', '30 May 2018 18:18:25', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 202, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LLR203', 'aide', '', '01/05/2018', '30 May 2018 18:30:31', '04 Jun 2018 18:55:53', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 203, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LLR204', 'aide', '', '01/05/2018', '30 May 2018 18:35:48', '04 Jun 2018 18:55:37', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 204, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LLR205', 'aide', '', '30/05/2018', '30 May 2018 20:13:25', '08 Jun 2018 15:56:01', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 205, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('LLR206', 'aide', '', '30/05/2018', '30 May 2018 20:23:30', '09 Jul 2018 16:25:34', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 206, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('ILA207', 'aide', 'Air', '02/06/2018', '08 Jun 2018 18:00:47', '08 Jun 2018 18:00:47', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 207, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '123', '', '123', 0, ''),
('ILA208', 'aide', 'Air', '02/06/2018', '08 Jun 2018 18:09:10', '08 Jun 2018 18:09:10', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 208, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '123', '', '123', 0, ''),
('ILA209', 'aide', 'Air', '03/06/2018', '08 Jun 2018 18:38:28', '19 Aug 2018 14:11:05', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 209, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '1234', 'do123123', '1234', 0, ''),
('ILA210', 'aide', 'Air', '26/06/2018', '08 Jun 2018 19:25:28', '13 Sep 2018 12:54:45', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 210, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '<p>12345</p>\r\n', 'Test', '12345', 0, ''),
('ILA211', 'aide', 'Air', '03/06/2018', '08 Jun 2018 19:26:17', '15 Aug 2018 23:17:56', '', '', '', '', '', '', '', '', '', 'Submitted to TX3', 'New Import', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 211, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '12345', '', '12345', 0, ''),
('ILA212', 'aide', 'Air', '04/06/2018', '08 Jun 2018 19:30:21', '15 Aug 2018 23:18:12', '', '', '', '', '', '', '', '', '', 'Submitted to TX3', 'New Import', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 212, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '123456', '1234', '123456', 0, ''),
('ILA213', 'aide', 'Air', '16/06/2018', '16 Jun 2018 22:30:46', '14 Jul 2018 18:45:05', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'AIDE Technologies Pte. Ltd.', 'Admin1234', NULL, '', '', '31/07/2018', '30/07/2018', 213, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA214', 'kerwin', 'Air', '26/07/2018', '26 Jul 2018 19:24:52', '26 Jul 2018 19:25:50', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'Aide', 'Afiza', NULL, '', '', '', '', 214, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', 'Txt1', 'testing', 0, ''),
('ILA215', 'aide', 'Air', '31/08/2018', '19 Aug 2018 14:22:06', '19 Aug 2018 14:57:56', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'AIDE Technologies Pte. Ltd.', 'Afiza', NULL, '', '', '', '', 215, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', 'hi', 'Nike', 'TX123', '', 0, ''),
('ELS216', 'aide', '', '31/08/2018', '19 Aug 2018 14:24:42', '19 Aug 2018 14:24:42', 'Sea', 'LCL', 'Japan', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 216, '', '', '', '', '', '', '', '', '', '', 'UNI', '', '', 0, ''),
('EFS217', 'aide', '', '31/08/2018', '19 Aug 2018 14:25:32', '19 Aug 2018 14:25:32', 'Sea', 'FCL', 'Japan', '', '1', '20''', 'DG', 'Yes', '', 'Pending', 'New Export', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 217, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EFR218', 'aide', '', '29/08/2018', '19 Aug 2018 14:26:30', '19 Aug 2018 14:26:30', 'Cross Border', 'FCL', '', 'KL', '3', '20''', 'GP', 'Yes', '', 'Pending', 'New Export', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 218, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('REQPER219', 'aide', '', '30/08/2018', '19 Aug 2018 14:42:03', '19 Aug 2018 14:42:03', '', '', '', '', '', '', '', '', 'Local', 'Pending', 'Request of Permit', 'AIDE Technologies Pte. Ltd.', '', NULL, '', '', '', '', 219, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('EFS220', 'Customer1234', '', '22/08/2018', '22 Aug 2018 15:18:15', '03 Sep 2018 10:54:29', 'Sea', 'FCL', 'Air', '', '', '', '', 'Yes', '', 'Accepted', 'New Export', 'IVIPLL', 'Afiza', NULL, '22/08/2018', '31/08/2018', '22/08/2018', '31/08/2018', 220, '', '', '', '', '', '', '', '', '23/08/2018', '<h3>Special Instruction</h3>\r\n', '<h2>Testing Content</h2>\r\n', 'TXT220', 'REF123', 0, ''),
('ILA221', 'Customer1234', 'Air', '27/08/2018', '27 Aug 2018 12:58:23', '27 Aug 2018 13:02:23', '', '', '', '', '', '', '', '', '', 'Accepted', 'New Import', 'IVIPLL', 'Afiza', NULL, '27/08/2018', '27/08/2018', '27/08/2018', '27/08/2018', 221, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '27/08/2018', '<p>testing specific</p>\r\n', '<p>testing</p>\r\n', 'txt221', 'testing', 0, ''),
('ILA222', 'Customer1234', 'Air', '13/09/2018', '13 Sep 2018 10:54:13', '13 Sep 2018 10:54:13', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 222, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing', '', 'testing', 0, ''),
('ILA223', 'Customer1234', 'Air', '13/09/2018', '13 Sep 2018 11:20:33', '13 Sep 2018 11:20:33', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 223, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ELA224', 'Customer1234', '', '13/09/2018', '13 Sep 2018 11:28:18', '13 Sep 2018 11:28:18', 'Air', '', 'testing', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', '', NULL, '', '', '', '', 224, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER225', 'Customer1234', '', '13/09/2018', '13 Sep 2018 11:28:35', '13 Sep 2018 11:28:35', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', '', NULL, '', '', '', '', 225, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR226', 'Customer1234', '', '13/09/2018', '13 Sep 2018 11:29:06', '14 Sep 2018 12:44:48', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', '', NULL, '', '', '', '', 226, '', '', '', '10', '1', '1', '1', '', '', '', '', 'tx3_test', '', 1, ''),
('ILA227', 'Customer1234', 'Air', '14/09/2018', '14 Sep 2018 12:49:22', '14 Sep 2018 14:20:50', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 227, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '<p>testing</p>\r\n', 'tx1222', 'testing', 1, ''),
('ILA228', 'TestingSub', 'Air', '15/09/2018', '15 Sep 2018 11:06:27', '15 Sep 2018 11:06:27', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', '', '', NULL, '', '', '', '', 228, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ILA229', 'Customer1234', 'Air', '15/09/2018', '15 Sep 2018 11:14:01', '15 Sep 2018 11:14:01', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 229, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ILA230', 'Customer1234', 'Air', '15/09/2018', '15 Sep 2018 11:15:49', '15 Sep 2018 11:15:49', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 230, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA231', 'Customer1234', 'Air', '15/09/2018', '15 Sep 2018 11:29:38', '15 Sep 2018 13:12:06', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 231, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'txt23', 'testing', 1, ''),
('ILA232', 'TestingSub', 'Air', '15/09/2018', '15 Sep 2018 11:39:52', '15 Sep 2018 13:09:45', '', '', '', '', '', '', '', '', '', 'Rejected', 'New Import', '', 'Afiza', NULL, '', '', '', '', 232, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'txid32', 'testing1', 1, ''),
('ELA233', 'TestingSub', '', '15/09/2018', '15 Sep 2018 11:40:06', '15 Sep 2018 12:36:59', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', '', '', NULL, '', '', '', '', 233, '', '', '', '', '', '', '', '', '', '', '', 'txt2', 'testing', 1, ''),
('REQPER234', 'TestingSub', '', '15/09/2018', '15 Sep 2018 11:40:41', '15 Sep 2018 12:16:59', '', '', '', '', '', '', '', '', 'testing', 'Accepted', 'Request of Permit', '', 'Afiza', NULL, '', '', '', '', 234, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('ILA235', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:01:57', '17 Sep 2018 11:01:57', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 235, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ILA236', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:08:24', '17 Sep 2018 11:08:24', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 236, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA237', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:12:06', '17 Sep 2018 11:12:06', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 237, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'tessss', '', 'testing', 0, ''),
('ILA238', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:17:58', '17 Sep 2018 11:17:58', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 238, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA239', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:20:09', '17 Sep 2018 11:20:09', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 239, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA240', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:25:13', '17 Sep 2018 11:25:13', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 240, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA241', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:31:00', '17 Sep 2018 11:31:00', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 241, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA242', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:33:13', '17 Sep 2018 11:33:13', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 242, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA243', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:41:09', '17 Sep 2018 11:41:09', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 243, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA244', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:43:25', '17 Sep 2018 11:43:25', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', '', NULL, '', '', '', '', 244, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA245', 'Customer1234', 'Air', '17/09/2018', '17 Sep 2018 11:44:54', '17 Sep 2018 11:44:54', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 245, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA246', 'TestingSub', 'Air', '17/09/2018', '17 Sep 2018 11:57:42', '17 Sep 2018 11:57:42', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', '', 'Afiza', NULL, '', '', '', '', 246, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ELA247', 'TestingSub', '', '17/09/2018', '17 Sep 2018 11:57:56', '17 Sep 2018 11:57:56', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', '', 'Afiza', NULL, '', '', '', '', 247, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER248', 'TestingSub', '', '17/09/2018', '17 Sep 2018 11:58:12', '17 Sep 2018 11:58:12', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', '', 'Afiza', NULL, '', '', '', '', 248, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR249', 'TestingSub', '', '17/09/2018', '17 Sep 2018 11:58:29', '17 Sep 2018 11:58:29', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', '', 'Afiza', NULL, '', '', '', '', 249, '', '', '', '10', '1', '1', '', 'none', '', '', '', '', '', 0, ''),
('ILA250', 'Customer1234', 'Air', '21/09/2018', '21 Sep 2018 11:01:32', '21 Sep 2018 16:29:37', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '22/09/2018', '26/09/2018', '22/09/2018', '23/09/2018', 250, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '21/09/2018', '<p>test</p>\r\n', '', 'tx122', 'testing', 1, 'Testing mj'),
('ILA251', 'Customer1234', 'Air', '21/09/2018', '21 Sep 2018 11:13:46', '21 Sep 2018 11:13:46', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 251, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ELA252', 'Customer1234', '', '21/09/2018', '21 Sep 2018 11:14:13', '21 Sep 2018 11:14:13', 'Air', '', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 252, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER253', 'Customer1234', '', '21/09/2018', '21 Sep 2018 11:14:38', '21 Sep 2018 11:14:38', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 253, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('ELA254', 'Customer1234', '', '21/09/2018', '21 Sep 2018 11:15:48', '21 Sep 2018 11:15:48', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 254, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR255', 'Customer1234', '', '21/09/2018', '21 Sep 2018 11:16:19', '21 Sep 2018 11:16:19', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 255, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('ILA256', 'Customer1234', 'Air', '22/09/2018', '21 Sep 2018 16:52:12', '21 Sep 2018 16:55:20', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '22/09/2018', '24/09/2018', '22/09/2018', '24/09/2018', 256, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '24/09/2018', '<p>status test</p>\r\n', '<p>Remark Test</p>\r\n', 'txtrm', 'testing', 0, 'Testing Body Text'),
('ELA257', 'Customer1234', '', '21/09/2018', '21 Sep 2018 16:52:37', '21 Sep 2018 16:52:37', 'Air', '', '', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 257, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER258', 'Customer1234', '', '21/09/2018', '21 Sep 2018 16:53:15', '21 Sep 2018 16:53:15', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 258, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR259', 'Customer1234', '', '21/09/2018', '21 Sep 2018 16:53:35', '24 Sep 2018 10:45:32', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 259, '', '', '', '10', '1', '', '', '', '', '', '', 'txt259', '', 0, ''),
('ILA260', 'Customer1234', 'Air', '24/09/2018', '24 Sep 2018 11:25:12', '24 Sep 2018 11:25:32', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 260, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'tx260', 'testing', 0, ''),
('ILA261', 'Customer1234', 'Air', '23/09/2018', '24 Sep 2018 11:49:12', '22 Oct 2018 17:06:30', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 261, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'tx161', 'testing', 0, ''),
('ILA262', 'Customer1234', 'Air', '24/09/2018', '24 Sep 2018 12:13:05', '24 Sep 2018 12:13:32', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 262, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', 'tx162', 'testing', 1, ''),
('ILA263', 'Customer1234', 'Air', '25/09/2018', '25 Sep 2018 10:37:48', '25 Sep 2018 10:37:48', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 263, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing Body Message', '', 'testing', 0, ''),
('ILA264', 'Customer1234', 'Air', '03/09/2018', '28 Sep 2018 11:10:29', '28 Sep 2018 11:10:29', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 264, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ILA265', 'Customer1234', 'Air', '25/09/2018', '28 Sep 2018 11:12:14', '28 Sep 2018 11:12:14', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 265, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA266', 'Customer1234', 'Air', '25/09/2018', '28 Sep 2018 11:16:34', '28 Sep 2018 11:16:34', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 266, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA267', 'Customer1234', 'Air', '24/10/2018', '01 Oct 2018 11:21:21', '01 Oct 2018 11:21:21', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 267, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA268', 'Customer1234', 'Air', '24/10/2018', '01 Oct 2018 11:22:32', '01 Oct 2018 11:22:32', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 268, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing Body', '', 'testing', 0, ''),
('ILA269', 'Customer1234', 'Air', '01/10/2018', '01 Oct 2018 12:23:23', '01 Oct 2018 12:23:23', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 269, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing For Body', '', 'testing', 0, ''),
('ILA270', 'MohitMj', 'Air', '01/10/2018', '01 Oct 2018 12:24:43', '01 Oct 2018 12:24:43', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'MJ', 'staff1234', NULL, '', '', '', '', 270, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing For Body', '', 'testing', 0, ''),
('ILA271', 'MohitMj', 'Cross Border', '01/10/2018', '01 Oct 2018 12:27:58', '01 Oct 2018 12:27:58', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'MJ', 'staff1234', NULL, '', '', '', '', 271, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing For Body', '', 'testing', 0, ''),
('ELA272', 'MohitMj', '', '01/10/2018', '01 Oct 2018 12:29:34', '01 Oct 2018 12:29:34', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'MJ', 'staff1234', NULL, '', '', '', '', 272, '', '', '', '', '', '', '', '', '', '', 'Testing For Body', '', 'testing', 0, ''),
('ILA273', 'Customer1234', 'Air', '01/10/2018', '03 Oct 2018 18:52:14', '03 Oct 2018 18:52:14', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 273, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA274', 'Customer1234', 'Air', '03/10/2018', '03 Oct 2018 18:55:18', '03 Oct 2018 18:55:18', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 274, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA275', 'Customer1234', 'Air', '08/10/2018', '03 Oct 2018 18:57:21', '03 Oct 2018 18:57:21', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 275, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ELA276', 'Customer1234', '', '03/10/2018', '03 Oct 2018 19:00:25', '03 Oct 2018 19:00:25', 'Air', '', 'testing', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 276, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER277', 'Customer1234', '', '03/10/2018', '03 Oct 2018 19:10:07', '03 Oct 2018 19:10:07', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 277, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR278', 'Customer1234', '', '03/10/2018', '03 Oct 2018 19:10:40', '03 Oct 2018 19:10:40', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 278, '', '', '', '', '', '', '', '', '', '', '', '', '', 0, ''),
('ILA279', 'Customer1234', 'Air', '04/10/2018', '04 Oct 2018 12:15:17', '04 Oct 2018 12:15:17', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 279, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA280', 'Customer1234', 'Air', '04/10/2018', '04 Oct 2018 12:25:07', '04 Oct 2018 12:25:07', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 280, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA281', 'Customer1234', 'Air', '01/10/2018', '04 Oct 2018 12:29:01', '04 Oct 2018 12:29:01', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 281, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA282', 'Customer1234', 'Air', '04/10/2018', '04 Oct 2018 12:30:40', '04 Oct 2018 12:30:40', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 282, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA283', 'Customer1234', 'Air', '04/10/2018', '04 Oct 2018 12:32:31', '04 Oct 2018 12:32:31', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 283, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ELA284', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:16:33', '04 Oct 2018 13:16:33', 'Air', '', 'testing', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 284, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('REQPER285', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:27:15', '04 Oct 2018 13:27:15', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 285, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR286', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:43:29', '04 Oct 2018 13:43:29', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 286, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR287', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:45:19', '04 Oct 2018 13:45:19', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 287, '', '', '', '10', '1', '1', '1', 'none', '', '', '', '', 'testing', 0, ''),
('ILA288', 'Customer1234', 'Air', '01/10/2018', '04 Oct 2018 13:47:44', '04 Oct 2018 13:47:44', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 288, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('EFR289', 'Customer1234', '', '16/10/2018', '04 Oct 2018 13:53:28', '04 Oct 2018 13:53:28', 'Cross Border', 'FCL', '', 'lko', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 289, '', '', '', '', '', '', '', '', '', '', 'testing', '', 'testing', 0, ''),
('REQPER290', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:54:33', '8 Oct 2018 12:53:26 GMT', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 290, '', '', '', '', '', '', '', '', '', '', '', 'tx290', 'testing', 1, ''),
('LLR291', 'Customer1234', '', '04/10/2018', '04 Oct 2018 13:56:16', '04 Oct 2018 14:11:18', '', '', '', '', '', '', '', '', '', 'Accepted', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 291, '', '', '', '10', '1', '1', '1', 'none', '', '', '', '', 'testing', 0, ''),
('ILA292', 'Customer1234', 'Air', '05/10/2018', '05 Oct 2018 18:38:10', '05 Oct 2018 18:38:10', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 292, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ELA293', 'Customer1234', '', '05/10/2018', '05 Oct 2018 18:48:34', '05 Oct 2018 18:48:34', 'Air', '', 'testing', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 293, '', '', '', '', '', '', '', '', '', '', 'testing', '', 'testing', 0, ''),
('REQPER294', 'Customer1234', '', '05/10/2018', '05 Oct 2018 18:48:57', '05 Oct 2018 18:48:57', '', '', '', '', '', '', '', '', 'testing', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 294, '', '', '', '', '', '', '', '', '', '', 'testing', '', 'testing', 0, ''),
('LLR295', 'Customer1234', '', '05/10/2018', '05 Oct 2018 18:50:56', '05 Oct 2018 18:50:56', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 295, '', '', '', '10', '1', '1', '1', 'none', '', '', '', '', 'testing', 0, ''),
('ILA296', 'Customer1234', 'Air', '18/10/2018', '18 Oct 2018 13:00:36', '18 Oct 2018 13:00:36', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 296, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing', '', 'testing', 0, ''),
('ILA297', 'Customer1234', 'Air', '18/10/2018', '18 Oct 2018 13:17:12', '18 Oct 2018 13:17:12', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 297, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing', '', 'testing', 0, ''),
('ILA298', 'Customer1234', 'Air', '18/10/2018', '18 Oct 2018 13:19:25', '18 Oct 2018 13:19:25', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 298, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA299', 'Customer1234', 'Air', '18/10/2018', '18 Oct 2018 13:22:52', '18 Oct 2018 13:22:52', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 299, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA300', 'Customer1234', 'Air', '17/10/2018', '18 Oct 2018 13:30:54', '18 Oct 2018 13:30:54', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 300, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA301', 'Customer1234', 'Air', '17/10/2018', '18 Oct 2018 13:35:07', '18 Oct 2018 13:35:07', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 301, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing', 0, ''),
('ILA302', 'Customer1234', 'Air', '17/10/2018', '18 Oct 2018 13:46:04', '22 Oct 2018 12:45:37', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 302, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', 'testing01', 0, ''),
('EFR303', 'Customer1234', '', '17/10/2018', '18 Oct 2018 13:47:02', '18 Oct 2018 13:47:02', 'Cross Border', 'FCL', '', 'lko', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 303, '', '', '', '', '', '', '', '', '', '', 'testing', '', 'testing', 0, ''),
('REQPER304', 'Customer1234', '', '17/10/2018', '18 Oct 2018 13:48:58', '22 Oct 2018 13:38:41', '', '', '', '', '', '', '', '', 'Allow', 'Pending', 'Request of Permit', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 304, '', '', '', '', '', '', '', '', '', '', '', '', 'testing', 0, ''),
('LLR305', 'Customer1234', '', '18 Oct 2018 13:49:45', '18 Oct 2018 13:49:45', '22 Oct 2018 14:18:02', '', '', '', '', '', '', '', '', '', 'Pending', 'Local Delivery', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 305, 'Testing Pickup', 'Testing Delivery', '', '10', '1', '1', '1', 'none', '', '', 'Testing Remark', '', 'testing Ref', 0, ''),
('ILR306', 'MohitMj', 'Cross Border', '01/10/2018', '20 Oct 2018 19:37:15', '20 Oct 2018 19:42:37', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'MJ', 'staff1234', NULL, '', '', '', '', 306, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing For Body', '', 'testing1212', 0, ''),
('ILR307', 'MohitMj', 'Cross Border', '01/10/2018', '20 Oct 2018 19:37:41', '20 Oct 2018 19:37:41', '', 'LCL', '', '', '', '', '', '', '', 'Pending', 'New Import', 'MJ', 'staff1234', NULL, '', '', '', '', 307, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing For Body', '', 'testing1111', 0, ''),
('ILA308', 'Customer1234', 'Air', '22/10/2018', '22 Oct 2018 12:46:05', '22 Oct 2018 13:34:53', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 308, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing', '', 'MJ Testing', 0, ''),
('EFS309', 'Customer1234', '', '31/10/2018', '22 Oct 2018 15:28:07', '22 Oct 2018 17:01:42', 'Sea', 'FCL', 'Air1', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 309, '', '', '', '', '', '', '', '', '', '', 'testing Sea', '', 'testing1', 0, ''),
('ILA310', 'Customer1234', 'Air', '22/10/2018', '22 Oct 2018 16:26:00', '22 Oct 2018 16:26:00', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 310, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'testing Air', '', 'testing Air', 0, ''),
('ELS311', 'Customer1234', '', '22/10/2018', '22 Oct 2018 16:26:46', '23 Oct 2018 19:10:59', 'Cross Border', 'LCL', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 311, '', '', '', '', '', '', '', '', '', '', 'testing Sea', '', 'testing Sea', 0, ''),
('EFR312', 'Customer1234', '', '22/10/2018', '22 Oct 2018 16:28:15', '14 Nov 2018 13:15:43', 'Cross Border', 'FCL', 'Sea', 'SG', '', '', '', 'Yes', '', 'Pending', 'New Export', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 312, '', '', '', '', '', '', '', '', '', '', 'testing Sea&#039;&#039;&#039;', '', 'testing Sea', 0, ''),
('ILA313', 'Customer1234', 'Air', '21/11/2018', '21 Nov 2018 15:26:49', '21 Nov 2018 15:26:49', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 313, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing Remark', '', 'MJ Testing', 0, ''),
('ILA314', 'Customer1234', 'Air', '21/11/2018', '21 Nov 2018 15:27:55', '21 Nov 2018 15:27:55', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Afiza', NULL, '', '', '', '', 314, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'Testing ', '', 'testing', 0, ''),
('ILA315', 'Customer1234', 'Air', '23/11/2018', '23 Nov 2018 11:11:18', '27 Nov 2018 12:04:42', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'IVIPLL', 'Staff1234', NULL, '', '', '', '', 315, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '<p>Testing ABC lkdfhjklsdhgjkffsjkhgjksjashdkjsahjkdhsajkdhjksahjkdsajkdhjksahjkdhsakjdhjksahdjksahkdhskahdkjsahkd asdjksajkd sakdklasjd</p>\r\n', 'txt', 'MJ Testing0101', 1, ''),
('ILA316', 'MohitMj', 'Air', '18/04/2019', '13 Apr 2019 12:08:20', '13 Apr 2019 12:09:06', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', 'MJ', 'staff1234', NULL, '', '', '', '', 316, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'ytyt', '', 'ytjyt', 0, ''),
('ELA317', 'MohitMj', '', '30/04/2019', '13 Apr 2019 12:17:39', '13 Apr 2019 12:20:01', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'MJ', 'staff1234', NULL, '', '', '', '', 317, '', '', '', '', '', '', '', '', '', '', 'ggfghfhfgh', '', 'MJ Testing0101', 0, ''),
('ELA318', 'MohitMj', '', '30/04/2019', '13 Apr 2019 12:17:51', '13 Apr 2019 12:20:27', 'Air', '', 'Air', '', '', '', '', 'Yes', '', 'Pending', 'New Export', 'MJ', 'staff1234', NULL, '', '', '', '', 318, '', '', '', '', '', '', '', '', '', '', 'testing', '', 'MJ Testing0101', 0, ''),
('REQPER319', 'MohitMj', '', '13/04/2019', '13 Apr 2019 12:23:18', '13 Apr 2019 12:23:33', '', '', '', '', '', '', '', '', 'testing req_per', 'Pending', 'Request of Permit', 'MJ', 'staff1234', NULL, '', '', '', '', 319, '', '', '', '', '', '', '', '', '', '', 'testing jjj', '', 'MJ Testing0101', 0, ''),
('ILA320', 'MjSub1', 'Air', '', '13 Apr 2019 12:45:12', '13 Apr 2019 12:45:12', '', '', '', '', '', '', '', '', '', 'Pending', 'New Import', '', 'staff1234', NULL, '', '', '', '', 320, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `request_container`
--

CREATE TABLE IF NOT EXISTS `request_container` (
`id` int(11) NOT NULL,
  `request_id` int(11) NOT NULL,
  `No_of_Container` varchar(255) NOT NULL,
  `Size_of_Container` varchar(255) NOT NULL,
  `Type_of_Container` varchar(255) NOT NULL,
  `status` int(1) NOT NULL,
  `create_date` varchar(255) NOT NULL,
  `modify_date` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `request_container`
--

INSERT INTO `request_container` (`id`, `request_id`, `No_of_Container`, `Size_of_Container`, `Type_of_Container`, `status`, `create_date`, `modify_date`) VALUES
(1, 220, '1', '20''', 'GP', 1, '22 Aug 2018 15:18:15', '22 Aug 2018 15:18:15'),
(2, 220, '2', '40''', 'DG', 1, '22 Aug 2018 15:18:15', '22 Aug 2018 15:18:15'),
(3, 220, '3', '20''', 'RF', 1, '22 Aug 2018 15:18:15', '22 Aug 2018 15:18:15'),
(4, 220, '4', '40''', 'HC', 1, '22 Aug 2018 15:18:15', '22 Aug 2018 15:18:15'),
(5, 289, '1', '20''', 'GP', 1, '04 Oct 2018 13:53:28', '04 Oct 2018 13:53:28'),
(6, 289, '2', '40''', 'DG', 1, '04 Oct 2018 13:53:28', '04 Oct 2018 13:53:28'),
(7, 303, '1', '20''', 'GP', 1, '18 Oct 2018 13:47:02', '18 Oct 2018 13:47:02'),
(17, 309, '1', '20''', 'DG', 1, '22 Oct 2018 17:01:42', '22 Oct 2018 17:01:42'),
(18, 309, '5', '40''', 'HC', 1, '22 Oct 2018 17:01:42', '22 Oct 2018 17:01:42'),
(19, 309, '3', '20''', 'Other', 1, '22 Oct 2018 17:01:42', '22 Oct 2018 17:01:42');

-- --------------------------------------------------------

--
-- Table structure for table `request_update_log`
--

CREATE TABLE IF NOT EXISTS `request_update_log` (
`tid` bigint(20) NOT NULL,
  `req_id` varchar(255) NOT NULL,
  `Status` text NOT NULL,
  `Updated_By` varchar(255) NOT NULL,
  `update_datetime` varchar(255) NOT NULL,
  `create_date` varchar(255) NOT NULL,
  `modify_date` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=187 ;

--
-- Dumping data for table `request_update_log`
--

INSERT INTO `request_update_log` (`tid`, `req_id`, `Status`, `Updated_By`, `update_datetime`, `create_date`, `modify_date`) VALUES
(1, '48', 'Accepted', 'staff1234', '13 Jan 2018 21:56:53', '13 Jan 2018 21:56:53', '13 Jan 2018 21:56:53'),
(2, '2', 'Accepted', 'staff1234', '19 Jan 2018 08:23:18', '19 Jan 2018 08:23:18', '19 Jan 2018 08:23:18'),
(3, '2', 'Accepted', 'staff1234', '22 Jan 2018 12:22:46', '22 Jan 2018 12:22:46', '22 Jan 2018 12:22:46'),
(4, '59', 'Pending', 'staff1234', '25 Jan 2018 14:38:39', '25 Jan 2018 14:38:39', '25 Jan 2018 14:38:39'),
(5, '59', 'Accepted', 'staff1234', '25 Jan 2018 14:40:46', '25 Jan 2018 14:40:46', '25 Jan 2018 14:40:46'),
(6, '60', 'Rejected', 'staff1234', '25 Jan 2018 21:40:22', '25 Jan 2018 21:40:22', '25 Jan 2018 21:40:22'),
(7, '64', 'Pending', 'staff1234', '27 Jan 2018 15:34:17', '27 Jan 2018 15:34:17', '27 Jan 2018 15:34:17'),
(8, '64', 'Rejected', 'staff1234', '27 Jan 2018 15:35:27', '27 Jan 2018 15:35:27', '27 Jan 2018 15:35:27'),
(9, '64', 'Rejected', 'staff1234', '27 Jan 2018 15:37:39', '27 Jan 2018 15:37:39', '27 Jan 2018 15:37:39'),
(10, '65', 'Rejected', 'staff1234', '27 Jan 2018 16:21:38', '27 Jan 2018 16:21:38', '27 Jan 2018 16:21:38'),
(11, '4', 'Pending', 'staff1234', '27 Jan 2018 16:27:51', '27 Jan 2018 16:27:51', '27 Jan 2018 16:27:51'),
(12, '4', 'Accepted', 'staff1234', '27 Jan 2018 16:28:18', '27 Jan 2018 16:28:18', '27 Jan 2018 16:28:18'),
(13, '11', 'Pending', 'staff1234', '27 Jan 2018 16:28:22', '27 Jan 2018 16:28:22', '27 Jan 2018 16:28:22'),
(14, '11', 'Accepted', 'staff1234', '27 Jan 2018 16:28:28', '27 Jan 2018 16:28:28', '27 Jan 2018 16:28:28'),
(15, '66', 'Accepted', 'staff1234', '27 Jan 2018 16:29:31', '27 Jan 2018 16:29:31', '27 Jan 2018 16:29:31'),
(16, '67', 'Accepted', 'staff1234', '28 Jan 2018 18:27:25', '28 Jan 2018 18:27:25', '28 Jan 2018 18:27:25'),
(17, '67', 'Rejected', 'staff1234', '28 Jan 2018 18:28:14', '28 Jan 2018 18:28:14', '28 Jan 2018 18:28:14'),
(18, '68', 'Accepted', 'customer1234', '28 Jan 2018 18:36:13', '28 Jan 2018 18:36:13', '28 Jan 2018 18:36:13'),
(19, '69', 'Accepted', 'staff1234', '29 Jan 2018 10:45:35', '29 Jan 2018 10:45:35', '29 Jan 2018 10:45:35'),
(20, '71', 'Accepted', 'staff1234', '29 Jan 2018 16:30:07', '29 Jan 2018 16:30:07', '29 Jan 2018 16:30:07'),
(21, '72', 'Accepted', 'staff1234', '29 Jan 2018 16:52:07', '29 Jan 2018 16:52:07', '29 Jan 2018 16:52:07'),
(22, '72', 'Rejected', 'staff1234', '29 Jan 2018 17:04:42', '29 Jan 2018 17:04:42', '29 Jan 2018 17:04:42'),
(23, '73', 'Accepted', 'staff1234', '30 Jan 2018 14:25:46', '30 Jan 2018 14:25:46', '30 Jan 2018 14:25:46'),
(24, '76', 'Accepted', 'staff1234', '31 Jan 2018 15:23:45', '31 Jan 2018 15:23:45', '31 Jan 2018 15:23:45'),
(25, '75', 'Accepted', 'staff1234', '01 Feb 2018 13:21:54', '01 Feb 2018 13:21:54', '01 Feb 2018 13:21:54'),
(26, '83', 'Accepted', 'staff1234', '01 Feb 2018 16:46:02', '01 Feb 2018 16:46:02', '01 Feb 2018 16:46:02'),
(27, '77', 'Pending', 'staff1234', '01 Feb 2018 20:15:25', '01 Feb 2018 20:15:25', '01 Feb 2018 20:15:25'),
(28, '86', 'Accepted', 'staff1234', '05 Feb 2018 23:17:56', '05 Feb 2018 23:17:56', '05 Feb 2018 23:17:56'),
(29, '91', 'Pending', 'staff1234', '07 Feb 2018 13:41:43', '07 Feb 2018 13:41:43', '07 Feb 2018 13:41:43'),
(30, '91', 'Accepted', 'staff1234', '07 Feb 2018 13:41:52', '07 Feb 2018 13:41:52', '07 Feb 2018 13:41:52'),
(31, '97', 'Accepted', 'staff1234', '07 Feb 2018 16:48:42', '07 Feb 2018 16:48:42', '07 Feb 2018 16:48:42'),
(32, '98', 'Accepted', 'staff1234', '07 Feb 2018 16:49:47', '07 Feb 2018 16:49:47', '07 Feb 2018 16:49:47'),
(33, '99', 'Accepted', 'staff1234', '07 Feb 2018 22:34:06', '07 Feb 2018 22:34:06', '07 Feb 2018 22:34:06'),
(34, '101', 'Accepted', 'staff1234', '08 Feb 2018 10:39:57', '08 Feb 2018 10:39:57', '08 Feb 2018 10:39:57'),
(35, '103', 'Pending', 'staff1234', '08 Feb 2018 15:35:33', '08 Feb 2018 15:35:33', '08 Feb 2018 15:35:33'),
(36, '103', 'Accepted', 'staff1234', '08 Feb 2018 15:35:41', '08 Feb 2018 15:35:41', '08 Feb 2018 15:35:41'),
(37, '104', 'Accepted', 'staff1234', '08 Feb 2018 15:39:35', '08 Feb 2018 15:39:35', '08 Feb 2018 15:39:35'),
(38, '105', 'Accepted', 'staff1234', '08 Feb 2018 15:51:27', '08 Feb 2018 15:51:27', '08 Feb 2018 15:51:27'),
(39, '107', 'Pending', 'staff1234', '09 Feb 2018 10:56:36', '09 Feb 2018 10:56:36', '09 Feb 2018 10:56:36'),
(40, '107', 'Accepted', 'staff1234', '09 Feb 2018 10:56:51', '09 Feb 2018 10:56:51', '09 Feb 2018 10:56:51'),
(41, '108', 'Accepted', 'staff1234', '09 Feb 2018 11:01:30', '09 Feb 2018 11:01:30', '09 Feb 2018 11:01:30'),
(42, '117', 'Accepted', 'staff1234', '11 Feb 2018 23:25:42', '11 Feb 2018 23:25:42', '11 Feb 2018 23:25:42'),
(43, '118', 'Rejected', 'staff1234', '11 Feb 2018 23:41:27', '11 Feb 2018 23:41:27', '11 Feb 2018 23:41:27'),
(44, '119', 'Accepted', 'staff1234', '11 Feb 2018 23:43:07', '11 Feb 2018 23:43:07', '11 Feb 2018 23:43:07'),
(45, '128', 'Accepted', 'staff1234', '14 Feb 2018 10:11:24', '14 Feb 2018 10:11:24', '14 Feb 2018 10:11:24'),
(46, '140', 'Accepted', 'staff1234', '15 Feb 2018 00:57:52', '15 Feb 2018 00:57:52', '15 Feb 2018 00:57:52'),
(47, '145', 'Accepted', 'staff1234', '20 Feb 2018 13:56:53', '20 Feb 2018 13:56:53', '20 Feb 2018 13:56:53'),
(48, '144', 'Accepted', 'staff1234', '20 Feb 2018 13:59:37', '20 Feb 2018 13:59:37', '20 Feb 2018 13:59:37'),
(49, '147', 'Accepted', 'nelson', '22 Feb 2018 10:49:21', '22 Feb 2018 10:49:21', '22 Feb 2018 10:49:21'),
(50, '148', 'Accepted', 'nelson', '22 Feb 2018 11:18:46', '22 Feb 2018 11:18:46', '22 Feb 2018 11:18:46'),
(51, '149', 'Accepted', 'staff1234', '22 Feb 2018 16:48:55', '22 Feb 2018 16:48:55', '22 Feb 2018 16:48:55'),
(52, '171', 'Accepted', 'staff1234', '25 Feb 2018 16:43:15', '25 Feb 2018 16:43:15', '25 Feb 2018 16:43:15'),
(53, '172', 'Accepted', 'staff1234', '25 Feb 2018 16:43:49', '25 Feb 2018 16:43:49', '25 Feb 2018 16:43:49'),
(54, '170', 'Accepted', 'staff1234', '25 Feb 2018 16:44:47', '25 Feb 2018 16:44:47', '25 Feb 2018 16:44:47'),
(55, '160', 'Accepted', 'staff1234', '25 Feb 2018 16:45:17', '25 Feb 2018 16:45:17', '25 Feb 2018 16:45:17'),
(56, '169', 'Accepted', 'staff1234', '25 Feb 2018 16:45:21', '25 Feb 2018 16:45:21', '25 Feb 2018 16:45:21'),
(57, '168', 'Accepted', 'staff1234', '25 Feb 2018 16:47:16', '25 Feb 2018 16:47:16', '25 Feb 2018 16:47:16'),
(58, '167', 'Accepted', 'staff1234', '25 Feb 2018 16:47:21', '25 Feb 2018 16:47:21', '25 Feb 2018 16:47:21'),
(59, '152', 'Accepted', 'staff1234', '25 Feb 2018 16:47:58', '25 Feb 2018 16:47:58', '25 Feb 2018 16:47:58'),
(60, '166', 'Accepted', 'staff1234', '26 Feb 2018 11:28:57', '26 Feb 2018 11:28:57', '26 Feb 2018 11:28:57'),
(61, '164', 'Accepted', 'staff1234', '26 Feb 2018 11:43:54', '26 Feb 2018 11:43:54', '26 Feb 2018 11:43:54'),
(62, '165', 'Accepted', 'staff1234', '26 Feb 2018 11:44:02', '26 Feb 2018 11:44:02', '26 Feb 2018 11:44:02'),
(63, '163', 'Accepted', 'staff1234', '26 Feb 2018 11:44:22', '26 Feb 2018 11:44:22', '26 Feb 2018 11:44:22'),
(64, '174', 'Accepted', 'staff1234', '26 Feb 2018 16:38:23', '26 Feb 2018 16:38:23', '26 Feb 2018 16:38:23'),
(65, '175', 'Accepted', 'staff1234', '27 Feb 2018 10:40:05', '27 Feb 2018 10:40:05', '27 Feb 2018 10:40:05'),
(66, '176', 'Accepted', 'staff1234', '27 Feb 2018 10:53:28', '27 Feb 2018 10:53:28', '27 Feb 2018 10:53:28'),
(67, '177', 'Accepted', 'staff1234', '27 Feb 2018 11:03:52', '27 Feb 2018 11:03:52', '27 Feb 2018 11:03:52'),
(68, '178', 'Accepted', 'staff1234', '27 Feb 2018 22:49:20', '27 Feb 2018 22:49:20', '27 Feb 2018 22:49:20'),
(69, '173', 'Accepted', 'angelyn', '27 Feb 2018 22:59:06', '27 Feb 2018 22:59:06', '27 Feb 2018 22:59:06'),
(70, '162', 'Pending', 'staff1234', '28 Feb 2018 09:48:25', '28 Feb 2018 09:48:25', '28 Feb 2018 09:48:25'),
(71, '180', 'Accepted', 'admin1234', '28 Feb 2018 14:23:44', '28 Feb 2018 14:23:44', '28 Feb 2018 14:23:44'),
(72, '181', 'Accepted', 'staff1234', '01 Mar 2018 14:18:11', '01 Mar 2018 14:18:11', '01 Mar 2018 14:18:11'),
(73, '197', 'Accepted', 'Staff1234', '19 Mar 2018 22:04:45', '19 Mar 2018 22:04:45', '19 Mar 2018 22:04:45'),
(74, '183', 'Pending', 'Staff1234', '20 Mar 2018 07:31:53', '20 Mar 2018 07:31:53', '20 Mar 2018 07:31:53'),
(75, '198', 'Accepted', 'Staff1234', '25 Mar 2018 00:57:23', '25 Mar 2018 00:57:23', '25 Mar 2018 00:57:23'),
(76, '201', 'Accepted', 'admin1234', '10 May 2018 02:43:04', '10 May 2018 02:43:04', '10 May 2018 02:43:04'),
(77, '202', 'Accepted', 'Afiza', '30 May 2018 18:18:25', '30 May 2018 18:18:25', '30 May 2018 18:18:25'),
(78, '200', 'Accepted', 'Afiza', '30 May 2018 18:20:11', '30 May 2018 18:20:11', '30 May 2018 18:20:11'),
(79, '199', 'Rejected', 'Afiza', '30 May 2018 18:20:17', '30 May 2018 18:20:17', '30 May 2018 18:20:17'),
(80, '199', 'Accepted', 'Afiza', '30 May 2018 18:20:29', '30 May 2018 18:20:29', '30 May 2018 18:20:29'),
(81, '1', 'Accepted', 'Afiza', '30 May 2018 18:22:09', '30 May 2018 18:22:09', '30 May 2018 18:22:09'),
(82, '203', 'Accepted', 'Afiza', '30 May 2018 18:31:01', '30 May 2018 18:31:01', '30 May 2018 18:31:01'),
(83, '204', 'Accepted', 'afiza', '30 May 2018 18:36:47', '30 May 2018 18:36:47', '30 May 2018 18:36:47'),
(84, '204', 'Pending', 'afiza', '30 May 2018 18:37:12', '30 May 2018 18:37:12', '30 May 2018 18:37:12'),
(85, '204', 'Accepted', 'afiza', '30 May 2018 20:12:54', '30 May 2018 20:12:54', '30 May 2018 20:12:54'),
(86, '205', 'Accepted', 'afiza', '30 May 2018 20:13:38', '30 May 2018 20:13:38', '30 May 2018 20:13:38'),
(87, '206', 'Accepted', 'Afiza', '30 May 2018 20:23:39', '30 May 2018 20:23:39', '30 May 2018 20:23:39'),
(88, '206', 'Pending', 'Afiza', '30 May 2018 20:24:23', '30 May 2018 20:24:23', '30 May 2018 20:24:23'),
(89, '206', 'Accepted', 'Afiza', '30 May 2018 20:24:29', '30 May 2018 20:24:29', '30 May 2018 20:24:29'),
(90, '206', 'Pending', 'Afiza', '30 May 2018 20:24:38', '30 May 2018 20:24:38', '30 May 2018 20:24:38'),
(91, '205', 'Pending', 'afiza', '30 May 2018 20:25:06', '30 May 2018 20:25:06', '30 May 2018 20:25:06'),
(92, '204', 'Pending', 'afiza', '30 May 2018 20:25:18', '30 May 2018 20:25:18', '30 May 2018 20:25:18'),
(93, '204', 'Accepted', 'Afiza', '30 May 2018 20:35:17', '30 May 2018 20:35:17', '30 May 2018 20:35:17'),
(94, '206', 'Accepted', 'Afiza', '04 Jun 2018 18:55:19', '04 Jun 2018 18:55:19', '04 Jun 2018 18:55:19'),
(95, '206', 'Pending', 'Afiza', '04 Jun 2018 18:55:29', '04 Jun 2018 18:55:29', '04 Jun 2018 18:55:29'),
(96, '204', 'Pending', 'Afiza', '04 Jun 2018 18:55:37', '04 Jun 2018 18:55:37', '04 Jun 2018 18:55:37'),
(97, '203', 'Pending', 'Afiza', '04 Jun 2018 18:55:53', '04 Jun 2018 18:55:53', '04 Jun 2018 18:55:53'),
(98, '206', 'Accepted', 'Afiza', '04 Jun 2018 19:04:57', '04 Jun 2018 19:04:57', '04 Jun 2018 19:04:57'),
(99, '15', 'Pending', 'afiza', '04 Jun 2018 21:51:48', '04 Jun 2018 21:51:48', '04 Jun 2018 21:51:48'),
(100, '205', 'Pending', 'Afiza', '08 Jun 2018 15:56:01', '08 Jun 2018 15:56:01', '08 Jun 2018 15:56:01'),
(101, '206', 'Accepted', 'Afiza', '09 Jul 2018 16:25:34', '09 Jul 2018 16:25:34', '09 Jul 2018 16:25:34'),
(102, '213', 'Accepted', 'Admin1234', '14 Jul 2018 18:45:05', '14 Jul 2018 18:45:05', '14 Jul 2018 18:45:05'),
(103, '183', 'XML Generated', 'Afiza', '26 Jul 2018 19:01:32', '26 Jul 2018 19:01:32', '26 Jul 2018 19:01:32'),
(104, '183', 'XML Generated', 'Afiza', '26 Jul 2018 19:22:37', '26 Jul 2018 19:22:37', '26 Jul 2018 19:22:37'),
(105, '183', 'XML Generated', 'Afiza', '26 Jul 2018 19:23:01', '26 Jul 2018 19:23:01', '26 Jul 2018 19:23:01'),
(106, '214', 'Accepted', 'Afiza', '26 Jul 2018 19:25:50', '26 Jul 2018 19:25:50', '26 Jul 2018 19:25:50'),
(107, '214', 'XML Generated', 'Afiza', '26 Jul 2018 19:26:08', '26 Jul 2018 19:26:08', '26 Jul 2018 19:26:08'),
(108, '183', 'XML Generated', 'Afiza', '26 Jul 2018 19:48:07', '26 Jul 2018 19:48:07', '26 Jul 2018 19:48:07'),
(109, '183', 'XML Generated', 'Afiza', '26 Jul 2018 21:16:14', '26 Jul 2018 21:16:14', '26 Jul 2018 21:16:14'),
(110, '183', 'XML Generated', 'Afiza', '27 Jul 2018 14:33:13', '27 Jul 2018 14:33:13', '27 Jul 2018 14:33:13'),
(111, '211', 'Submitted to TX3', 'Afiza', '15 Aug 2018 23:17:56', '15 Aug 2018 23:17:56', '15 Aug 2018 23:17:56'),
(112, '212', 'Submitted to TX3', 'Afiza', '15 Aug 2018 23:18:12', '15 Aug 2018 23:18:12', '15 Aug 2018 23:18:12'),
(113, '215', 'Accepted', 'Afiza', '19 Aug 2018 14:23:12', '19 Aug 2018 14:23:12', '19 Aug 2018 14:23:12'),
(114, '215', 'XML Generated', 'Afiza', '19 Aug 2018 14:30:44', '19 Aug 2018 14:30:44', '19 Aug 2018 14:30:44'),
(115, '220', 'New Remark:- <h2>Testing Content</h2>\r\n', 'Afiza', '22 Aug 2018 16:33:21', '22 Aug 2018 16:33:21', '22 Aug 2018 16:33:21'),
(116, '220', 'New Special Instruction :- <h3>Special Instruction</h3>\r\n', 'Afiza', '22 Aug 2018 16:33:21', '22 Aug 2018 16:33:21', '22 Aug 2018 16:33:21'),
(117, '220', 'Accepted', 'Afiza', '22 Aug 2018 16:40:41', '22 Aug 2018 16:40:41', '22 Aug 2018 16:40:41'),
(118, '220', 'Rejected', 'Afiza', '22 Aug 2018 16:40:58', '22 Aug 2018 16:40:58', '22 Aug 2018 16:40:58'),
(119, '220', 'Pending', 'Afiza', '22 Aug 2018 16:41:24', '22 Aug 2018 16:41:24', '22 Aug 2018 16:41:24'),
(120, '221', 'Accepted', 'Afiza', '27 Aug 2018 13:00:09', '27 Aug 2018 13:00:09', '27 Aug 2018 13:00:09'),
(121, '221', 'New Remark:- <p>testing</p>\r\n', 'Afiza', '27 Aug 2018 13:02:23', '27 Aug 2018 13:02:23', '27 Aug 2018 13:02:23'),
(122, '221', 'New Special Instruction :- <p>testing specific</p>\r\n', 'Afiza', '27 Aug 2018 13:02:23', '27 Aug 2018 13:02:23', '27 Aug 2018 13:02:23'),
(123, '221', 'XML Generated', 'Afiza', '27 Aug 2018 13:09:12', '27 Aug 2018 13:09:12', '27 Aug 2018 13:09:12'),
(124, '221', 'XML Generated', 'Afiza', '27 Aug 2018 14:51:42', '27 Aug 2018 14:51:42', '27 Aug 2018 14:51:42'),
(125, '221', 'XML Generated', 'Afiza', '27 Aug 2018 15:02:47', '27 Aug 2018 15:02:47', '27 Aug 2018 15:02:47'),
(126, '221', 'XML Generated', 'Afiza', '27 Aug 2018 15:04:44', '27 Aug 2018 15:04:44', '27 Aug 2018 15:04:44'),
(127, '221', 'XML Generated', 'Afiza', '27 Aug 2018 15:07:20', '27 Aug 2018 15:07:20', '27 Aug 2018 15:07:20'),
(128, '221', 'XML Generated', 'Afiza', '27 Aug 2018 15:12:10', '27 Aug 2018 15:12:10', '27 Aug 2018 15:12:10'),
(129, '220', 'Accepted', 'Afiza', '03 Sep 2018 10:54:29', '03 Sep 2018 10:54:29', '03 Sep 2018 10:54:29'),
(130, '210', 'New Remark:- <p>12345</p>\r\n', 'Afiza', '13 Sep 2018 12:54:08', '13 Sep 2018 12:54:08', '13 Sep 2018 12:54:08'),
(131, '226', 'New Tx3ID:- tx3_test', 'Afiza', '14 Sep 2018 12:44:48', '14 Sep 2018 12:44:48', '14 Sep 2018 12:44:48'),
(132, '226', 'XML Generated', 'Afiza', '14 Sep 2018 12:54:43', '14 Sep 2018 12:54:43', '14 Sep 2018 12:54:43'),
(133, '226', 'XML Generated', 'Afiza', '14 Sep 2018 12:58:37', '14 Sep 2018 12:58:37', '14 Sep 2018 12:58:37'),
(134, '227', 'New Tx3ID:- tx1222', 'Afiza', '14 Sep 2018 14:15:14', '14 Sep 2018 14:15:14', '14 Sep 2018 14:15:14'),
(135, '227', 'New Remark:- <p>testing</p>\r\n', 'Afiza', '14 Sep 2018 14:15:14', '14 Sep 2018 14:15:14', '14 Sep 2018 14:15:14'),
(136, '227', 'XML Generated', 'Afiza', '14 Sep 2018 14:15:24', '14 Sep 2018 14:15:24', '14 Sep 2018 14:15:24'),
(137, '234', 'Rejected', 'Afiza', '15 Sep 2018 12:16:47', '15 Sep 2018 12:16:47', '15 Sep 2018 12:16:47'),
(138, '234', 'Accepted', 'Afiza', '15 Sep 2018 12:16:59', '15 Sep 2018 12:16:59', '15 Sep 2018 12:16:59'),
(139, '233', 'New Tx3ID:- txt2', 'Afiza', '15 Sep 2018 12:36:59', '15 Sep 2018 12:36:59', '15 Sep 2018 12:36:59'),
(140, '118', 'New Tx3ID:- txt_reg', 'Afiza', '15 Sep 2018 13:02:15', '15 Sep 2018 13:02:15', '15 Sep 2018 13:02:15'),
(141, '118', 'New Remark:- <p>urgent clearance pls check with co-loader how much is the urgent unstuffing. i need cargoes on friday.</p>\r\n', 'Afiza', '15 Sep 2018 13:02:15', '15 Sep 2018 13:02:15', '15 Sep 2018 13:02:15'),
(142, '232', 'Rejected', 'Afiza', '15 Sep 2018 13:08:58', '15 Sep 2018 13:08:58', '15 Sep 2018 13:08:58'),
(143, '233', 'XML Generated', 'Afiza', '15 Sep 2018 13:09:02', '15 Sep 2018 13:09:02', '15 Sep 2018 13:09:02'),
(144, '232', 'New Tx3ID:- txid32', 'Afiza', '15 Sep 2018 13:09:45', '15 Sep 2018 13:09:45', '15 Sep 2018 13:09:45'),
(145, '232', 'XML Generated', 'Afiza', '15 Sep 2018 13:09:53', '15 Sep 2018 13:09:53', '15 Sep 2018 13:09:53'),
(146, '231', 'New Tx3ID:- txt23', 'Afiza', '15 Sep 2018 13:12:06', '15 Sep 2018 13:12:06', '15 Sep 2018 13:12:06'),
(147, '231', 'XML Generated', 'Afiza', '15 Sep 2018 13:12:10', '15 Sep 2018 13:12:10', '15 Sep 2018 13:12:10'),
(148, '250', 'New Tx3ID:- tx121', 'Afiza', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19'),
(149, '250', 'Expected Completion :- 21/09/2018', 'Afiza', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19'),
(150, '250', 'Permit Date To :- 22/09/2018', 'Afiza', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19'),
(151, '250', 'Permit Date From :- 26/09/2018', 'Afiza', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19', '21 Sep 2018 11:43:19'),
(152, '250', 'New Tx3ID:- tx122', 'Afiza', '21 Sep 2018 12:09:49', '21 Sep 2018 12:09:49', '21 Sep 2018 12:09:49'),
(153, '250', 'New Special Instruction :- <p>test</p>\r\n', 'Afiza', '21 Sep 2018 12:09:49', '21 Sep 2018 12:09:49', '21 Sep 2018 12:09:49'),
(154, '250', 'Pickup Date To :- 22/09/2018', 'Afiza', '21 Sep 2018 12:43:35', '21 Sep 2018 12:43:35', '21 Sep 2018 12:43:35'),
(155, '250', 'Pickup Date From :- 23/09/2018', 'Afiza', '21 Sep 2018 12:52:50', '21 Sep 2018 12:52:50', '21 Sep 2018 12:52:50'),
(156, '250', 'Date of Arival:- 26/09/2018', 'Afiza', '21 Sep 2018 16:28:02', '21 Sep 2018 16:28:02', '21 Sep 2018 16:28:02'),
(157, '250', 'Date of Arival:- 21/09/2018', 'Afiza', '21 Sep 2018 16:29:37', '21 Sep 2018 16:29:37', '21 Sep 2018 16:29:37'),
(158, '256', 'Date of Arival:- 22/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(159, '256', 'New Tx3ID:- txtrm', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(160, '256', 'New Remark:- <p>Remark Test</p>\r\n', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(161, '256', 'Expected Completion :- 24/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(162, '256', 'Permit Date To :- 22/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(163, '256', 'Permit Date From :- 24/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(164, '256', 'Pickup Date To :- 22/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(165, '256', 'Pickup Date From :- 24/09/2018', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(166, '256', 'New Special Instruction :- <p>status test</p>\r\n', 'Afiza', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20', '21 Sep 2018 16:55:20'),
(167, '256', 'XML Generated', 'Afiza', '21 Sep 2018 17:13:23', '21 Sep 2018 17:13:23', '21 Sep 2018 17:13:23'),
(168, '250', 'XML Generated', 'Afiza', '24 Sep 2018 10:26:45', '24 Sep 2018 10:26:45', '24 Sep 2018 10:26:45'),
(169, '259', 'New Tx3ID:- txt259', 'Afiza', '24 Sep 2018 10:45:32', '24 Sep 2018 10:45:32', '24 Sep 2018 10:45:32'),
(170, '260', 'New Tx3ID:- tx260', 'Afiza', '24 Sep 2018 11:25:32', '24 Sep 2018 11:25:32', '24 Sep 2018 11:25:32'),
(171, '261', 'New Tx3ID:- tx161', 'Afiza', '24 Sep 2018 11:49:38', '24 Sep 2018 11:49:38', '24 Sep 2018 11:49:38'),
(172, '261', 'XML Generated', 'Afiza', '24 Sep 2018 11:49:43', '24 Sep 2018 11:49:43', '24 Sep 2018 11:49:43'),
(173, '262', 'New Tx3ID:- tx162', 'Afiza', '24 Sep 2018 12:13:32', '24 Sep 2018 12:13:32', '24 Sep 2018 12:13:32'),
(174, '262', 'XML Generated', 'Afiza', '24 Sep 2018 12:13:37', '24 Sep 2018 12:13:37', '24 Sep 2018 12:13:37'),
(175, '291', 'Accepted', 'Afiza', '04 Oct 2018 14:11:18', '04 Oct 2018 14:11:18', '04 Oct 2018 14:11:18'),
(176, '290', 'New Tx3ID:- tx290', 'Afiza', '04 Oct 2018 14:11:32', '04 Oct 2018 14:11:32', '04 Oct 2018 14:11:32'),
(177, '290', 'Accepted', 'Afiza', '04 Oct 2018 14:12:30', '04 Oct 2018 14:12:30', '04 Oct 2018 14:12:30'),
(178, '290', 'XML Generated', 'Afiza', '04 Oct 2018 14:12:42', '04 Oct 2018 14:12:42', '04 Oct 2018 14:12:42'),
(179, '290', 'Sent to VCC', 'Afiza', '08 Oct 2018 18:22:01', '08 Oct 2018 18:22:01', '08 Oct 2018 18:22:01'),
(180, '290', 'New documents added by customer', 'Customer1234', '08 Oct 2018 18:23:26', '08 Oct 2018 18:23:26', '08 Oct 2018 18:23:26'),
(181, '308', 'Accepted', 'Afiza', '22 Oct 2018 13:12:19', '22 Oct 2018 13:12:19', '22 Oct 2018 13:12:19'),
(182, '290', 'Sent to VCC', 'Staff1234', '27 Nov 2018 10:42:37', '27 Nov 2018 10:42:37', '27 Nov 2018 10:42:37'),
(183, '315', 'New Tx3ID:- txt', 'Staff1234', '27 Nov 2018 12:04:21', '27 Nov 2018 12:04:21', '27 Nov 2018 12:04:21'),
(184, '315', 'New Remark:- <p>Testing ABC lkdfhjklsdhgjkffsjkhgjksjashdkjsahjkdhsajkdhjksahjkdsajkdhjksahjkdhsakjdhjksahdjksahkdhskahdkjsahkd asdjksajkd sakdklasjd</p>\r\n', 'Staff1234', '27 Nov 2018 12:04:21', '27 Nov 2018 12:04:21', '27 Nov 2018 12:04:21'),
(185, '315', 'Pending', 'Staff1234', '27 Nov 2018 12:04:42', '27 Nov 2018 12:04:42', '27 Nov 2018 12:04:42'),
(186, '315', 'Sent to VCC', 'Staff1234', '27 Nov 2018 12:04:50', '27 Nov 2018 12:04:50', '27 Nov 2018 12:04:50');

-- --------------------------------------------------------

--
-- Table structure for table `request_upload`
--

CREATE TABLE IF NOT EXISTS `request_upload` (
`id` bigint(20) unsigned NOT NULL,
  `req_id` varchar(45) NOT NULL,
  `upload_file` varchar(500) NOT NULL,
  `upload_by_type` varchar(255) NOT NULL,
  `upload_by_user` varchar(255) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=375 ;

--
-- Dumping data for table `request_upload`
--

INSERT INTO `request_upload` (`id`, `req_id`, `upload_file`, `upload_by_type`, `upload_by_user`) VALUES
(345, '302', 'nreq_8371_QbWaJfO7.jpg', 'Customer', 'Customer1234'),
(357, '302', 'nreq_5050_image1(1).jpeg', 'Customer', 'Customer1234'),
(359, '308', 'nreq_8545_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(360, '304', 'rop_5188_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(363, '310', 'nreq_4278_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(364, '311', 'ereq_348_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(365, '312', 'ereq_3921_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(366, '309', 'ereq_3345_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(367, '309', 'ereq_6199_no-profile-photo.jpg', 'Customer', 'Customer1234'),
(368, '256', 'nreq_9207_no-profile-photo.jpg', 'Staff', 'Afiza'),
(369, '313', 'nreq_3466_4401.exe', 'Customer', 'Customer1234'),
(370, '314', 'nreq_8159_4161.pdf', 'Customer', 'Customer1234'),
(371, '315', 'nreq_5575_ereq_7740_DN_4273024061_ 62_ COA_ PI.pdf', 'Customer', 'Customer1234'),
(372, '316', 'nreq_6148_01(1).jpg', 'Customer', 'MohitMj'),
(373, '317', 'ereq_6613_01(1).jpg', 'Customer', 'MohitMj'),
(374, '318', 'ereq_2938_01(1).jpg', 'Customer', 'MohitMj');

-- --------------------------------------------------------

--
-- Table structure for table `sno`
--

CREATE TABLE IF NOT EXISTS `sno` (
  `sno` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sno`
--

INSERT INTO `sno` (`sno`) VALUES
('316');

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE IF NOT EXISTS `status` (
  `status` varchar(45) NOT NULL,
  `cdate` varchar(45) NOT NULL,
  `mdate` varchar(45) NOT NULL,
`id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`status`, `cdate`, `mdate`, `id`) VALUES
('Accepted', '9 Dec 2017 07:51:49 GMT', '9 Dec 2017 07:51:56 GMT', 1),
('Pending', '9 Dec 2017 07:32:21 GMT', '24 Mar 2018 16:56:46 GMT', 2),
('Submitted to TX3', '9 Dec 2017 07:37:00 GMT', '9 Dec 2017 07:37:00 GMT', 3),
('Rejected', '3 Jan 2018 06:26:38 GMT', '3 Jan 2018 06:26:38 GMT', 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `email_master`
--
ALTER TABLE `email_master`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_access`
--
ALTER TABLE `login_access`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_log`
--
ALTER TABLE `login_log`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reg`
--
ALTER TABLE `reg`
 ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `req`
--
ALTER TABLE `req`
 ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `request_container`
--
ALTER TABLE `request_container`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_update_log`
--
ALTER TABLE `request_update_log`
 ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `request_upload`
--
ALTER TABLE `request_upload`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sno`
--
ALTER TABLE `sno`
 ADD PRIMARY KEY (`sno`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `email_master`
--
ALTER TABLE `email_master`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `login_access`
--
ALTER TABLE `login_access`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT for table `login_log`
--
ALTER TABLE `login_log`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `reg`
--
ALTER TABLE `reg`
MODIFY `tid` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `req`
--
ALTER TABLE `req`
MODIFY `tid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=321;
--
-- AUTO_INCREMENT for table `request_container`
--
ALTER TABLE `request_container`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `request_update_log`
--
ALTER TABLE `request_update_log`
MODIFY `tid` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=187;
--
-- AUTO_INCREMENT for table `request_upload`
--
ALTER TABLE `request_upload`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=375;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
