CREATE TABLE email_master (
  id NUMBER NOT NULL ,
  user_id NUMBER NOT NULL,
  user_type varchar(255) NOT NULL,
  set_type varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  emails varchar(500) NOT NULL,
  status varchar(255) NOT NULL,
  createdate varchar(255) NOT NULL,
  modifydate varchar(255) NOT NULL,
  PRIMARY KEY (id)
);


CREATE SEQUENCE  email_master_seq start with 100;
CREATE OR REPLACE trigger em_test  
  before insert on email_master              
  for each row 
begin  
  if :NEW.id is null then
    select email_master_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  em_test ENABLE;

CREATE TABLE location (
  location varchar(45) NOT NULL,
  cdate varchar(45) NOT NULL,
  mdate varchar(45) NOT NULL,
  id NUMBER NOT NULL ,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  location_seq start with 100;
CREATE OR REPLACE trigger lo_test  
  before insert on location              
  for each row 
begin  
  if :NEW.id is null then
    select location_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  lo_test ENABLE;


CREATE TABLE login_access (
  id NUMBER NOT NULL ,
  user_id NUMBER NOT NULL,
  token clob NOT NULL,
  created_date varchar(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  login_access_seq start with 5010;
CREATE OR REPLACE trigger la_test  
  before insert on login_access              
  for each row 
begin  
  if :NEW.id is null then
    select login_access_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  la_test ENABLE;


CREATE TABLE login_log (
  id NUMBER NOT NULL ,
  date_ varchar(255) NOT NULL,
  time varchar(255) NOT NULL,
  user_id NUMBER NOT NULL,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  login_log_seq start with 521;
CREATE OR REPLACE trigger LL_TEST  
  before insert on login_log              
  for each row 
begin  
  if :NEW.id is null then
    select login_log_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  ll_test ENABLE;


CREATE TABLE reg (
  id varchar(45) NOT NULL,
  name varchar(45) NOT NULL,
  cno varchar(45) NOT NULL,
  email varchar(45) NOT NULL,
  pwd varchar(45) NOT NULL,
  cname varchar(45) DEFAULT NULL,
  repname varchar(45) DEFAULT NULL,
  type varchar(45) NOT NULL,
  count varchar(45) NOT NULL,
  flag varchar(45) NOT NULL,
  dt varchar(45) NOT NULL,
  location varchar(45) DEFAULT NULL,
  cdate varchar(45) NOT NULL,
  mdate varchar(45) NOT NULL,
  tid NUMBER NOT NULL ,
  cust_master_id varchar(255) DEFAULT NULL,
  isblocked NUMBER DEFAULT '0',
  cso varchar(255) DEFAULT NULL,
  PRIMARY KEY (tid)
);
CREATE SEQUENCE  reg_seq start with 100;
CREATE OR REPLACE trigger EG_TEST  
  before insert on reg              
  for each row 
begin  
  if :NEW.tid is null then
    select reg_seq.nextval into :NEW.tid from dual;
  end if;
end;
/
ALTER TRIGGER  eg_test ENABLE;

CREATE TABLE req (
  id varchar(45) NOT NULL,
  uid_ varchar(45) NOT NULL,
  imptype varchar(45) DEFAULT NULL,
  da varchar(45) DEFAULT NULL,
  cdate varchar(45) DEFAULT NULL,
  mdate varchar(45) DEFAULT NULL,
  exptype varchar(45) DEFAULT NULL,
  fcl varchar(45) DEFAULT NULL,
  desport varchar(45) DEFAULT NULL,
  placedel varchar(45) DEFAULT NULL,
  nocont varchar(45) DEFAULT NULL,
  sizecont varchar(45) DEFAULT NULL,
  typecont varchar(45) DEFAULT NULL,
  bondedtruck varchar(45) DEFAULT NULL,
  pertype varchar(45) DEFAULT NULL,
  status varchar(45) DEFAULT NULL,
  reqtype varchar(45) DEFAULT NULL,
  cmpname varchar(45) DEFAULT NULL,
  handby varchar(45) DEFAULT NULL,
  file_ varchar(45) DEFAULT NULL,
  pdateto varchar(45) DEFAULT NULL,
  pdatefrom varchar(45) DEFAULT NULL,
  pudateto varchar(45) DEFAULT NULL,
  pudatefrom varchar(45) DEFAULT NULL,
  tid NUMBER NOT NULL ,
  pua clob,
  dadd clob,
  tpkg varchar(45) DEFAULT NULL,
  twgt varchar(45) DEFAULT NULL,
  tnop varchar(45) DEFAULT NULL,
  tm3 varchar(45) DEFAULT NULL,
  pkgd varchar(45) DEFAULT NULL,
  sreq varchar(45) DEFAULT NULL,
  Expected_Comp varchar(255) DEFAULT NULL,
  additional_comment clob,
  remarks clob,
  tx3id varchar(255) DEFAULT NULL,
  cust_ref_no varchar(255) DEFAULT NULL,
  is_send_vcc NUMBER(1) DEFAULT '0',
  vcc_instru clob,
  PRIMARY KEY (tid)
);
CREATE SEQUENCE  req_seq start with 1400;
CREATE OR REPLACE trigger EQ_TEST  
  before insert on req             
  for each row 
begin  
  if :NEW.tid is null then
    select req_seq.nextval into :NEW.tid from dual;
  end if;
end;
/
ALTER TRIGGER  eq_test ENABLE;

CREATE TABLE request_container (
  id NUMBER NOT NULL ,
  request_id NUMBER NOT NULL,
  No_of_Container varchar(255) NOT NULL,
  Size_of_Container varchar(255) NOT NULL,
  Type_of_Container varchar(255) NOT NULL,
  status number(1) NOT NULL,
  create_date varchar(255) NOT NULL,
  modify_date varchar(255) NOT NULL,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  request_container_seq start with 1000;
CREATE OR REPLACE trigger RC_TEST  
  before insert on request_container              
  for each row 
begin  
  if :NEW.id is null then
    select request_container_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  rc_test ENABLE;


CREATE TABLE request_update_log (
  tid NUMBER NOT NULL ,
  req_id varchar(255) NOT NULL,
  Status clob NOT NULL,
  Updated_By varchar(255) NOT NULL,
  update_datetime varchar(255) NOT NULL,
  create_date varchar(255) NOT NULL,
  modify_date varchar(255) NOT NULL,
  PRIMARY KEY (tid)
);
CREATE SEQUENCE  request_update_log_seq start with 7000;
CREATE OR REPLACE trigger EO_TEST  
  before insert on request_update_log              
  for each row 
begin  
  if :NEW.tid is null then
    select request_update_log_seq.nextval into :NEW.tid from dual;
  end if;
end;
/
ALTER TRIGGER  eq_test ENABLE;


CREATE TABLE request_upload (
  id NUMBER NOT NULL ,
  req_id varchar(45) NOT NULL,
  upload_file varchar(500) DEFAULT NULL,
  upload_by_type varchar(255) DEFAULT NULL,
  upload_by_user varchar(255) DEFAULT NULL,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  request_upload_seq start with 3000;
CREATE OR REPLACE trigger AD_TEST  
  before insert on request_upload              
  for each row 
begin  
  if :NEW.id is null then
    select request_upload_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  ad_test ENABLE;


CREATE TABLE sno (
  sno varchar(45) NOT NULL,
  PRIMARY KEY (sno)
);


CREATE TABLE status (
  status varchar(45) NOT NULL,
  cdate varchar(45) NOT NULL,
  mdate varchar(45) NOT NULL,
  id NUMBER NOT NULL ,
  PRIMARY KEY (id)
);
CREATE SEQUENCE  status_seq start with 100;
CREATE OR REPLACE trigger US_TEST  
  before insert on status              
  for each row 
begin  
  if :NEW.id is null then
    select status_seq.nextval into :NEW.id from dual;
  end if;
end;
/
ALTER TRIGGER  us_test ENABLE;







