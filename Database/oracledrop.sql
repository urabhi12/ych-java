drop table email_master;
drop table location;
drop table login_access;
drop table login_log;
drop table reg;
drop table req;
drop table request_container;
drop table request_update_log;
drop table request_upload;
drop table sno;
drop table status;


drop sequence email_master_seq;
drop sequence location_seq;
drop sequence login_access_seq;
drop sequence login_log_seq;
drop sequence reg_seq;
drop sequence req_seq;
drop sequence request_container_seq;
drop sequence request_update_log_seq;
drop sequence request_upload_seq;
drop sequence status_seq;
