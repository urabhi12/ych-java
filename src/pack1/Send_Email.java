package pack1;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Send_Email {

//	private static final String ADMIN_EMAIL = "ych.developers@gmail.com";
//	private static final String ADMIN_EMAIL_PASSWORD = "SL@409051";
//	private static final String ADMIN_EMAIL_SMTP = "smtp.gmail.com";
//	private static final String ADMIN_FROM = "no-reply@ych.com";// change accordingly
//	private static final String ADMIN_PORT = "587";//465 or 587// change accordingly

	private static final String ADMIN_EMAIL = "noreply-fp";
    private static final String ADMIN_EMAIL_PASSWORD = "u063b18$76";
    private static final String ADMIN_EMAIL_SMTP = "appsmail2.ychsystems.com";
    private static final String ADMIN_FROM = "noreply-fp@ychsystems.com";// change accordingly
    private static final String ADMIN_PORT = "25";

	public static boolean send(String[] reciever,String header, String[] cc, String[] bcc, String subject, String text, Object attachment) {
		// Get the session object
		Properties props = new Properties();
//		props.put("mail.smtp.user", ADMIN_EMAIL);
		props.put("mail.smtp.host", ADMIN_EMAIL_SMTP);
//		props.put("mail.smtp.localhost", "uatfreight.ych.com");
		props.put("mail.smtp.port", ADMIN_PORT);
		props.put("mail.smtp.starttls.enable","false");
//		props.put("mail.smtp.ssl.enable","true");
		props.put("mail.smtp.debug", "true");
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.transport.protocol", "smtp");
//		props.put("mail.smtp.socketFactory.port", ADMIN_PORT);
//		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.socketFactory.fallback", "false");
//		props.put("mail.smtp.quitwait", "false");

		Session session;
		session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Send_Email.ADMIN_EMAIL, Send_Email.ADMIN_EMAIL_PASSWORD);
			}
		});
		// Used to debug SMTP issues
		session.setDebug(true);

		//compose the message
		try {

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Send_Email.ADMIN_FROM, header));
			message.setReplyTo(new Address[]{
					new InternetAddress(Send_Email.ADMIN_FROM)
			});
			InternetAddress[] recipientAddress = new InternetAddress[reciever.length];

			for(int i=0; i<reciever.length; i++){
                recipientAddress[i] = new InternetAddress(reciever[i]);
            } 
			message.setRecipients(Message.RecipientType.TO, recipientAddress);
			message.setSubject(subject);
			message.setContent(text, "text/html");
			Transport.send(message);
			System.out.println("Email sent successfully");
			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		}

	}
	public static boolean sendwithAttachment(String[] reciever, String header, String[] cc, String[] bcc, String subject, String text, ArrayList<String> arr) {
		// Get the session object
		Properties props = new Properties();
//		props.put("mail.smtp.user", ADMIN_EMAIL);
		props.put("mail.smtp.host", ADMIN_EMAIL_SMTP);
//		props.put("mail.smtp.localhost", "uatfreight.ych.com");
		props.put("mail.smtp.port", ADMIN_PORT);
		props.put("mail.smtp.starttls.enable","false");
//		props.put("mail.smtp.ssl.enable","true");
		props.put("mail.smtp.debug", "true");
//		props.put("mail.smtp.auth", "true");
//		props.put("mail.transport.protocol", "smtp");
//		props.put("mail.smtp.socketFactory.port", ADMIN_PORT);
//		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
//		props.put("mail.smtp.socketFactory.fallback", "false");
//		props.put("mail.smtp.quitwait", "false");

		Session session;
		session = Session.getInstance(props,
				new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(Send_Email.ADMIN_EMAIL, Send_Email.ADMIN_EMAIL_PASSWORD);
			}
		});
		// Used to debug SMTP issues
		session.setDebug(true);

		//compose the message
		try {

			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(Send_Email.ADMIN_FROM, header));
			message.setReplyTo(new Address[]{
					new InternetAddress(Send_Email.ADMIN_FROM)
			});

			// For To
			InternetAddress[] recipientAddress = new InternetAddress[reciever.length];
			for(int i=0; i<reciever.length; i++){
                recipientAddress[i] = new InternetAddress(reciever[i]);
            } 
			message.setRecipients(Message.RecipientType.TO, recipientAddress);

			// For CC
			if(cc != null) {
				InternetAddress[] recipientCCAddress = new InternetAddress[cc.length];
				for(int c=0;  c < cc.length; c++){
	            	recipientCCAddress[c] = new InternetAddress(cc[c]);
	            }
				message.setRecipients(Message.RecipientType.CC, recipientCCAddress);
			}

			message.setSubject(subject);

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			messageBodyPart.setText(text);   

			// Create a multipart message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			multipart.addBodyPart(messageBodyPart);

			if(arr != null) {
				for(int i=0; i<arr.size(); i++){
					// Part two is attachment
					messageBodyPart = new MimeBodyPart();  
					String filename = arr.get(i);
					DataSource source = new FileDataSource(filename);
					messageBodyPart.setDataHandler(new DataHandler(source));
					messageBodyPart.setFileName(filename);
					multipart.addBodyPart(messageBodyPart);
				} 
			}

			// Send the complete message parts
			message.setContent(multipart); 

			// message.setContent(text, "text/html");
			Transport.send(message);
			System.out.println("Email sent successfully");

			return true;
		} catch (MessagingException | UnsupportedEncodingException e) {
			return false;
		}

	}

	public static String getPath(){
		String path = Send_Email.class.getClassLoader().getResource("").getPath();
		String fullPath = "";
		try {
			fullPath = URLDecoder.decode(path, "UTF-8");
			String pathArr[] = fullPath.split("/WEB-INF/classes/");
			String directoryPath = pathArr[0]+"/";
			String directoryName = directoryPath+"uploadproduct/";
			File directory = new File(directoryName);
			if (! directory.exists()){
				directory.mkdir();
			}
			return directoryName;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			//			String directoryPath = System.getProperty("catalina.base")+"/";
			//	    	String directoryName = directoryPath+"temp/";
			//	    	File directory = new File(directoryName);
			//	        if (! directory.exists()){
			//	            directory.mkdir();
			//	        }
			//	        return directoryName;
			return null;
		}

		//    	try {
		//        	String path = Send_Email.class.getClassLoader().getResource("").getPath();
		//            String fullPath = URLDecoder.decode(path, "UTF-8");
		//            String pathArr[] = fullPath.split("/WEB-INF/classes/");
		////            System.out.println(fullPath);
		////            System.out.println(pathArr[0]);
		//			Files.createLink(Paths.get(pathArr[0],"uploadproduct"), Paths.get("",directoryName));
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}

		//        return "D://Workspace//Eclipse//YCH//web//uploadproduct//";
		//return "C://Program Files//Apache Software Foundation//Tomcat 8.5//webapps//YCH//uploadproduct//";
		//return "C://Program Files//Apache Software Foundation//Tomcat 8.5//webapps//YCH-DEV//uploadproduct//";

	}
}
