package pack1;

import org.apache.commons.fileupload.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

public class FileUploadManager {

    private Properties form;
    private FileItem item, data;
    private String deposit;
    private String fileName;
    private String fileExtension;

    public FileUploadManager(HttpServletRequest request, File folder) {
        form = new Properties();
        try {
            deposit = folder.getAbsolutePath();
            ServletFileUpload p = new ServletFileUpload(new DiskFileItemFactory(-1, folder)); //predefined class of jar
            List list = p.parseRequest(request);
            Iterator iter = list.iterator();
            while (iter.hasNext()) {
                item = (FileItem) iter.next();
                if (!item.isFormField()) {
                    data = item;
                    fileExtension = item.getName().substring(item.getName().lastIndexOf("."));
                } else {
                    String a  = item.getFieldName();
                    if(item.getSize()<1){
                        form.setProperty(item.getFieldName(), "");
                    }else{
                         form.setProperty(item.getFieldName(), item.getString());
                    }
                }
            }
        } catch (Exception e) {
            form.setProperty("ERROR", e.getMessage());
        }
    }

    public String getFormValue(String name) {
        return form.getProperty(name);
    }

    public boolean save(String name) {
        boolean flag = false;
        try {
            fileName = name + fileExtension;
            form.setProperty("filename", fileName);

            File file = new File(fileName);
            File temp = new File(deposit + "/" + fileName);
            file.renameTo(temp);
            data.write(temp);
            flag = temp.exists();
        } catch (Exception e) {
        }
        return flag;
    }

}
