package pack1;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class ConnectionManager {

    Connection con;

    public Connection getCon() 
    {
    	Context ctx = null;
    	try {
			if( con != null )
			con.close();
			
			Class.forName("oracle.jdbc.OracleDriver");
//			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ych", "root", "AIDE@123");
//			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/ych-dev", "root", "AIDE@123");
//			con = DriverManager.getConnection("jdbc:mysql://192.168.0.101:3306/ych", "chege", "1234");
//			jdbc:oracle:oci:tiger/scott@localhost:1521:productDB
//			con = DriverManager.getConnection("jdbc:oracle:thin:@192.168.0.101:1521:XE", "lee", "1234");
//			con = DriverManager.getConnection("jdbc:oracle:thin:@10.202.94.97:1521:ysduat32","YCH_UAT_FREIGHT_PROTAL","YCH_UAT_FREIGHT_PROTAL");
			ctx = new InitialContext();
			DataSource ds = (DataSource) ctx.lookup("java:/comp/env/jdbc/YCHLocalDB");
			
			con = ds.getConnection();

		} catch (ClassNotFoundException | SQLException | NamingException e1) {
//			e1.printStackTrace();
			System.out.println(e1);
		}
        return con;
    }
    
    public void close() {
    	if(con != null) {
    		try {
				con.close();
			} catch (SQLException e) {
				System.out.println("Error while closing connection : " + e.getMessage());
			}
    	}
    }
}
