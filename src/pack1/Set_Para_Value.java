package pack1;


import java.util.Properties;
import org.apache.commons.fileupload.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author MJ
 */
public class Set_Para_Value {
    private Properties form;
    
    public Set_Para_Value(){
        form = new Properties();
    }

    public void Set_Value(FileItem item){       
        if(item.getSize()<1){
            form.setProperty(item.getFieldName(), "");
        }else{
             form.setProperty(item.getFieldName(), item.getString());
        }
    }
     public String getFormValue(String name) {
        return form.getProperty(name);
    }
}
