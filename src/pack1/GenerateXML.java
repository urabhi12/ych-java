/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pack1;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import pack1.Send_Email;


/**
 *
 * @author dell
 */
public class GenerateXML {
    
     public static String XMLGenerate(String content,String filename) {
         
        BufferedWriter bufferedWriter = null;
        String FilePath = Send_Email.getPath()+filename;
        try {
            String strContent = content;
            File myFile = new File(FilePath);
            // check if file exist, otherwise create the file before writing
            if (!myFile.exists()) {
                myFile.createNewFile();
            }
            Writer writer = new FileWriter(myFile);
            bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(strContent);
        } catch (IOException e) {
        } finally{
            try{
                if(bufferedWriter != null) bufferedWriter.close();
            } catch(IOException ex){
                 
            }
        }
        return FilePath;
    }
}
